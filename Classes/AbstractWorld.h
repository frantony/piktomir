//
//  AbstractWorld.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__AbstractWorld__
#define __pictomir_cocos2d_x__AbstractWorld__

#include <iostream>
#include <vector>
#include <algorithm>
#include "RobotManager.h"
#include "AbstractRobot.h"
#include "Hints.h"
#include "Task.h"
#include "AbstractMap.h"

enum WorldType
{
    pmAbstractWorld = -1,
    pmWorld4 = 0
};

class AbstractWorld
{
private:
    std::vector<AbstractRobot *> robotList;
    std::vector<RobotManager *> robotManagers;
    int baseRobot;
protected:
    SYNTHESIZE(AbstractMap *, map,  Map);
public:
    
    void sortRobotList(bool (*sortPredicate)(const void*,const void*))
    {
        std::sort(robotList.begin(), robotList.end(), sortPredicate);
    }
    
    AbstractWorld() :
        robotList(std::vector<AbstractRobot *>()),
        robotManagers(std::vector<RobotManager *>()),
        baseRobot(-1),
        map(nullptr)
    {}
    
    AbstractWorld(AbstractMap *map) :
        robotList(std::vector<AbstractRobot *>()),
        robotManagers(std::vector<RobotManager *>()),
        baseRobot(-1),
        map(map)
    {}
    
    virtual ~AbstractWorld()
    {
        for(int i = 0; i < robotCount(); ++i)
        {
            CC_SAFE_RELEASE_NULL(robotList[i]);
            CC_SAFE_DELETE(robotManagers[i]);
        }
        CC_SAFE_RELEASE_NULL(map);
    }
    
    static AbstractWorld* create(AbstractMap *map)
    {
        return new AbstractWorld(map);
    }
    
    static AbstractWorld* create()
    {
        return new AbstractWorld;
    }
    
    int robotCount() const
    {
        return robotList.size();
    }
    
    AbstractRobot *robotAt(int index)
    {
        return robotList[index];
    }
        
    RobotManager *robotManagerAt(int index)
    {
        return robotManagers[index];
    }
    
    void addRobot(AbstractRobot *robot)
    {
        robotList.push_back(robot);
        robotManagers.push_back(new RobotManager(robot));
    }

    void setBaseRobot(int robot);
    
    AbstractRobot *getBaseRobot()
    {
        if(baseRobot < 0 || baseRobot >= robotList.size())
            return nullptr;
        
        return robotList[baseRobot];
    }
    
    int getBaseRobotIndex()
    {
        return baseRobot;
    }
    
    RobotManager *getBaseRobotManager()
    {
        if(baseRobot < 0 || baseRobot >= robotList.size())
            return nullptr;
        
        return robotManagers[baseRobot];
    }
    
    virtual WorldType type()
    {
        return pmAbstractWorld;
    }
    //ЗАТЫЧКА!!!
    bool robotsWin()
    {
        bool flag = true;
        
        for(int  i = 0; i < robotCount(); ++i)
        {
            if(!robotAt(i)->getTaskList()->empty() && !(robotAt(i)->isWin() && robotAt(i)->isEndedWork()))
                flag = false;
        }
        
        return flag;
    }
    
    bool robotsLoose()
    {
        bool flag = false;
        
        for(int  i = 0; i < robotCount(); ++i)
        {
            if(!robotAt(i)->getTaskList()->empty() && robotAt(i)->isBroken())
                flag = true;
        }
        
        return flag;
    }
    
    bool robotsEndedWork()
    {
        bool flag = true;
        
        for(int  i = 0; i < robotCount(); ++i)
        {
            if(!robotAt(i)->isEndedWork())
                flag = false;
        }
        
        return flag;
    }
    
    bool robotsPlayingAnimation(AbstractRobot *exceptRobot)
    {
        bool flag = false;
        
        for(int  i = 0; i < robotCount(); ++i)
        {
            if(robotAt(i) != exceptRobot && robotAt(i)->isPlayingAnimation())
                flag = true;
        }
        
        return flag;
    }
};

#endif /* defined(__pictomir_cocos2d_x__AbstractWorld__) */

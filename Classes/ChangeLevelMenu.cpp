//
//  ChangeLevelMenu.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/17/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "ChangeLevelMenu.h"
#include "PMTextureCache.h"
#include "GameLayer.h"

USING_NS_CC;

bool ChangeLevelMenu::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    

    MenuItemSprite *nextLevel = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("nextLevel", NORMAL_STATE),
                                                       PMTextureCache::instanse()->getIconSprite("nextLevel", SELECTED_STATE),
                                                       PMTextureCache::instanse()->getIconSprite("nextLevel", DISABLED_STATE),
                                                       CC_CALLBACK_1(ChangeLevelMenu::nextLevel, this));
    
    MenuItemSprite *prevLevel = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("prevLevel", NORMAL_STATE),
                                                       PMTextureCache::instanse()->getIconSprite("prevLevel", SELECTED_STATE),
                                                       PMTextureCache::instanse()->getIconSprite("prevLevel", DISABLED_STATE),
                                                       CC_CALLBACK_1(ChangeLevelMenu::prevLevel, this));
    
    prevLevel->setPosition( Point(20, 20) );
    prevLevel->setAnchorPoint(Point::ZERO );
    nextLevel->setPosition( Point(PMSettings::instance()->mapSize().width -  nextLevel->getContentSize().width - 20, 20) );
    nextLevel->setAnchorPoint(Point::ZERO );
    
    menu = Menu::create(nextLevel,prevLevel, nullptr);
    menu->setAnchorPoint( Point::ZERO );
    menu->setContentSize(Size(PMSettings::instance()->mapSize().width, nextLevel->getContentSize().height + 20) );
    
    menu->setPosition( Point::ZERO );
    
    setPosition( Point(0,0) );
    setAnchorPoint( Point::ZERO );
    
    addChild(menu,10);
    
    return true;
}

void ChangeLevelMenu::saveStates()
{
    state = ((MenuItem *)menu->getChildren().at(0))->isEnabled();
}

void ChangeLevelMenu::restoreStates()
{
    for(int i = 0; i < menu->getChildrenCount(); ++i)
    {
        ((MenuItem *)menu->getChildren().at(i))->setEnabled(state);
    }
}

void ChangeLevelMenu::setEnabled(bool flag)
{
    for(int i = 0; i < menu->getChildrenCount(); ++i)
    {
         ((MenuItem *)menu->getChildren().at(i))->setEnabled(flag);
    }
}

void ChangeLevelMenu::nextLevel(cocos2d::Ref *sender)
{
    PMSettings::instance()->incLevel();
    if (PMSettings::instance()->getLevel() == PMSettings::instance()->getMapsCount(parent->getWorldPart()) + 1)
    {
        PMSettings::instance()->incWorldPart();
        PMSettings::instance()->setLevel(1);
    }
    
    if (PMSettings::instance()->getWorldPart() >= PMSettings::instance()->getWorldsCount())
    {
        PMSettings::instance()->setWorldPart(0);
    }
    
    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, GameLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void ChangeLevelMenu::prevLevel(cocos2d::Ref *sender)
{
    PMSettings::instance()->decLevel();
    if (PMSettings::instance()->getLevel() == 0)
    {
        PMSettings::instance()->decWorldPart();
        
        if (PMSettings::instance()->getWorldPart() < 0)
        {
            PMSettings::instance()->setLevel(1);
            PMSettings::instance()->setWorldPart(0);
        }
		else
			PMSettings::instance()->setLevel( PMSettings::instance()->getMapsCount(parent->getWorldPart()));
    }
    
    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, GameLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

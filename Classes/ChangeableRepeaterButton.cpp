//
//  ChangeableRepeaterButton.cpp
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 13.11.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "ChangeableRepeaterButton.h"

USING_NS_CC;

using namespace std;

ChangeableRepeaterButton::ChangeableRepeaterButton(const cocos2d::ccMenuCallback& callback):
SelectButton(SelectButton::Repeater, 7, PMTextureCache::instanse()->getRepeaterSprite(7, NORMAL_STATE),
             PMTextureCache::instanse()->getRepeaterSprite(7, SELECTED_STATE),
             PMTextureCache::instanse()->getRepeaterSprite(7, DISABLED_STATE),
             callback)
{
    

    MenuItemImage *up_arrow = MenuItemImage::create(PICT_PATH "System/up_arrow.png", PICT_PATH "System/up_arrow.png", PICT_PATH "System/up_arrow.png", CC_CALLBACK_1(ChangeableRepeaterButton::incValue, this));
    up_arrow->setPosition(Point(getContentSize().width/2, getContentSize().height + 7));
    
    MenuItemImage *down_arrow = MenuItemImage::create(PICT_PATH "System/down_arrow.png", PICT_PATH "System/down_arrow.png", PICT_PATH "System/down_arrow.png", CC_CALLBACK_1(ChangeableRepeaterButton::decValue, this));
    down_arrow->setPosition(Point(getContentSize().width/2, -7));
    
    Menu *menu = Menu::create(up_arrow, down_arrow, nullptr);
    menu->setPosition( Point::ZERO );
    
    addChild(menu);
    
    label = Label::create("7", "Arial", 20, Size::ZERO,TextHAlignment::LEFT);
    label->setPosition( Point(getContentSize().width/2, getContentSize().height/2) );
    
    addChild(label,10);
}

cocos2d::Sprite *ChangeableRepeaterButton::cloneSprite()
{
    Sprite *newSpr = Sprite::createWithSpriteFrame(((Sprite *)this->getNormalImage())->getSpriteFrame());
    newSpr->setPosition(getPosition());
    
    char string[3];
    sprintf(string, "%d", repeater);
    
    Label *sprLabel = Label::create(string, "Arial", 20, Size::ZERO,TextHAlignment::LEFT);
    sprLabel->setPosition( Point(getContentSize().width/2, getContentSize().height/2) );
    
    newSpr->addChild(sprLabel,10);
    
    return newSpr;
}

void ChangeableRepeaterButton::incValue(cocos2d::Ref *pObject)
{
    if(repeater < 99)
    {
        ++repeater;
        
        char string[3];
        sprintf(string, "%d", repeater);
        
        label->setString(string);
    }
}

void ChangeableRepeaterButton::decValue(cocos2d::Ref *pObject)
{
    if(repeater > 7)
    {
        --repeater;
        
        char string[3];
        sprintf(string, "%d", repeater);
        
        label->setString(string);
    }
}
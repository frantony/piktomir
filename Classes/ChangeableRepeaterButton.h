//
//  ChangeableRepeaterButton.h
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 13.11.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__ChangeableRepeaterButton__
#define __pictomir_cocos2d_x__ChangeableRepeaterButton__

#include <iostream>

#include "cocos2d.h"
#include "SelectButton.h"
#include "PMTextureCache.h"

USING_NS_CC;

class ChangeableRepeaterButton : public SelectButton
{
public:
    
    ChangeableRepeaterButton():
        SelectButton()
    {}
    
    static ChangeableRepeaterButton* create()
    {
        return new ChangeableRepeaterButton();
    }
    
    ChangeableRepeaterButton(const cocos2d::ccMenuCallback& callback);
    
    static ChangeableRepeaterButton* create(const cocos2d::ccMenuCallback& callback)
    {
        return new ChangeableRepeaterButton(callback);
    }
    
    virtual cocos2d::Sprite *cloneSprite();
    
private:
    
    cocos2d::Label *label;
    
    void incValue(cocos2d::Ref *pObject);
    void decValue(cocos2d::Ref *pObject);
};


#endif /* defined(__pictomir_cocos2d_x__ChangeableRepeaterButton__) */

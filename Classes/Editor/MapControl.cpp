//
//  MapControl.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 12.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "MapControl.h"
#include "PMSettings.h"
#include "cocos2d.h"
#include "MapEditorGameLayer.h"
#include "Robot4Edit.h"
#include "XmlWorldSaver.h"
#include "MainMenuLayer.h"
#include "ScrollLayer.h"
#include "SelectMapMenuLayer.h"
#include "SaveRobotCountNetGame.h"

USING_NS_CC;

using namespace std;

MapControlLayer * MapControlLayer::instanse = nullptr;

MapControlLayer* MapControlLayer::getInstanse () {
    return instanse;
}

MapControlLayer* MapControlLayer::create() {
    if(instanse){
        instanse->removeAllChildrenWithCleanup(true);
        instanse->release();
    }
    instanse = new MapControlLayer;
    instanse->init();
    return instanse;
}


void MapControlLayer::finishMapLoading() {
    layersCnt_ = ((Map4*)(MapEditorGameLayer::getInstanse()->getWorld()->getMap()))->getLayerCount();
    
    
    tilesetCnt_ = ((Map4*)(MapEditorGameLayer::getInstanse()->getWorld()->getMap()))->getTileSet()+1;
    
    char tmp[10] = "";
    
    sprintf(tmp,"%d", layersCnt_);
    layersCnt->setString(tmp);
    
    sprintf(tmp,"%d", tilesetCnt_);
    tilesetCnt->setString(tmp);
    
    RobotPrefsScrolLayer::getInstanse()->finishLoading();
}

void MapControlLayer::updateArrow ()
{
    if(!arrow)return;
    arrow->setNormalImage(Sprite::create("System/down.png"));
    arrow->setSelectedImage(Sprite::create("System/down.png"));
    Size screenSize = PMSettings::instance()->getScreenSize();
    arrow->setPosition(Point((screenSize.width-PMSettings::instance()->getProgramLayerWidth())/2,0));
    showed = false;
}

void MapControlLayer::showLayer(cocos2d::Ref * sender) {
    showed = !showed;
    if(showed) {
        arrow->setNormalImage(Sprite::create("System/up.png"));
        arrow->setSelectedImage(Sprite::create("System/up.png"));
        arrow->setPosition((((MenuItemImage*)sender)->getPosition()+Point(0,18)));
        
        FiniteTimeAction* moveLayer = CCMoveBy::create(0.4f, Point(0,-sizeHeight));
        
        runAction( moveLayer );
        
        MapEditorGameLayer::getInstanse()->setEnabled(false);
    }
    else {
        arrow->setNormalImage(Sprite::create("System/down.png"));
        arrow->setSelectedImage(Sprite::create("System/down.png"));
        arrow->setPosition((((MenuItemImage*)sender)->getPosition()+Point(0,-18)));
        FiniteTimeAction* moveLayer = CCMoveBy::create(0.4f, Point(0,sizeHeight));
        runAction( moveLayer );
        MapEditorGameLayer::getInstanse()->setEnabled(true);
    }
    
    MapEditorGameLayer::getInstanse()->getSpeedBarLayer()->setEnabled(!showed);
}

void MapControlLayer::plusLayers (cocos2d::Ref * sender) {
    layersCnt_ ++;
    char tmp[10] = "";
    sprintf(tmp,"%d", layersCnt_);
    layersCnt->setString(tmp);
    
    MapEditorGameLayer::getInstanse()->changeMapLayers(layersCnt_);
}

void MapControlLayer::minusLayers (cocos2d::Ref * sender) {
    layersCnt_ --;
    if(layersCnt_ == 0) {layersCnt_++;return;}
    
    MapEditorGameLayer::getInstanse()->changeMapLayers(layersCnt_);
    
    char tmp[10] = "";
    sprintf(tmp,"%d", layersCnt_);
    layersCnt->setString(tmp);
}

void MapControlLayer::plusTileset (cocos2d::Ref * sender) {
    tilesetCnt_ ++;
    if(tilesetCnt_ == 2) {tilesetCnt_--;return;}
    char tmp[10] = "";
    sprintf(tmp,"%d", tilesetCnt_);
    tilesetCnt->setString(tmp);
    
    //MapEditorGameLayer::getInstanse()->changeTileset(tilesetCnt_-1);
}

void MapControlLayer::minusTileset (cocos2d::Ref * sender) {
    tilesetCnt_ --;
    if(tilesetCnt_ == 0) {tilesetCnt_++;return;}
    
    //MapEditorGameLayer::getInstanse()->changeTileset(tilesetCnt_-1);
    
    char tmp[10] = "";
    sprintf(tmp,"%d", tilesetCnt_);
    tilesetCnt->setString(tmp);
}

bool MapControlLayer::init()
{
    if ( !LayerColor::init() )
    {
        return false;
    }
    
    removeAllChildrenWithCleanup(true);
    
    sizeHeight = 200;
    Size screenSize = PMSettings::instance()->getScreenSize();
    
    initWithColor(Color4B(144,144,144,220), screenSize.width-PMSettings::instance()->getProgramLayerWidth(),sizeHeight);
    
    showed = false;
    layersCnt_ = 0;
    
    Label * mapLabel = Label::create(LocalizedString("Map"), PMSettings::instance()->getFont(), 22);
    mapLabel->setAnchorPoint(Point(0,1));
    mapLabel->setPosition(Point(25,sizeHeight-5));
    
    Label * layers = Label::create(LocalizedString("Layers"), PMSettings::instance()->getFont(), 18);

    layers->setAnchorPoint(Point(0,1));
    layers->setPosition(Point(25,sizeHeight-35));
    
    MenuItemLabel* layersPlus = MenuItemLabel::create(Label::create("+", PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(MapControlLayer::plusLayers,this));
    layersPlus->setAnchorPoint(Point(0,1));
    layersPlus->setPosition(Point(135,sizeHeight-35));
    
    MenuItemLabel* layersMinus = MenuItemLabel::create(Label::create("-", PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(MapControlLayer::minusLayers,this));
    layersMinus->setAnchorPoint(Point(0,1));
    layersMinus->setPosition(Point(95,sizeHeight-35));
    
    layersCnt = Label::create("1", PMSettings::instance()->getFont(), 18);
    layersCnt->setAnchorPoint(Point(0,1));
    layersCnt->setPosition(Point(110,sizeHeight-35));
    
    
    Label * tileset = Label::create(LocalizedString("TileSet"), PMSettings::instance()->getFont(), 18);

    tileset->setAnchorPoint(Point(0,1));
    tileset->setPosition(Point(25,sizeHeight-65));
    
    MenuItemLabel* tilesetPlus = MenuItemLabel::create(Label::create("+", PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(MapControlLayer::plusTileset,this));
    tilesetPlus->setAnchorPoint(Point(0,1));
    tilesetPlus->setPosition(Point(135,sizeHeight-65));
    
    MenuItemLabel* tilesetMinus = MenuItemLabel::create(Label::create("-", PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(MapControlLayer::minusTileset,this));
    tilesetMinus->setAnchorPoint(Point(0,1));
    tilesetMinus->setPosition(Point(95,sizeHeight-65));
    
    tilesetCnt = Label::create("1", PMSettings::instance()->getFont(), 18);
    tilesetCnt->setAnchorPoint(Point(0,1));
    tilesetCnt->setPosition(Point(110,sizeHeight-65));
    
    
    
MenuItemLabel* SaveMap = MenuItemLabel::create(Label::create(LocalizedString("Save"), PMSettings::instance()->getFont(), 22),CC_CALLBACK_1(MapControlLayer::SaveMap,this));

    SaveMap->setColor(Color3B(20, 200, 20));
    SaveMap->setAnchorPoint(Point(0,1));
    SaveMap->setPosition(Point(35,sizeHeight-95));
    
MenuItemLabel* closeMap = MenuItemLabel::create(Label::create(LocalizedString("Close"), PMSettings::instance()->getFont(), 22),CC_CALLBACK_1(MapControlLayer::CloseMap,this));

    closeMap->setColor(Color3B(20, 20, 200));
    closeMap->setAnchorPoint(Point(0,1));
    closeMap->setPosition(Point(35,sizeHeight-125));
    
MenuItemLabel* deleteMap = MenuItemLabel::create(Label::create(LocalizedString("DeleteMap"), PMSettings::instance()->getFont(), 22),CC_CALLBACK_1(MapControlLayer::DeleteMap,this));

    deleteMap->setColor(Color3B(200, 20, 20));
    deleteMap->setAnchorPoint(Point(0,1));
    deleteMap->setPosition(Point(35,sizeHeight-155));
    
    arrow = MenuItemImage::create("System/down.png", "System/down.png",CC_CALLBACK_1(MapControlLayer::showLayer,this));
    Menu * menu = Menu::create(arrow, SaveMap, closeMap, deleteMap, layersPlus, layersMinus, tilesetPlus, tilesetMinus, nullptr);
    
    arrow->setAnchorPoint(Point(0.5,1));
    arrow->setPosition(Point((screenSize.width-PMSettings::instance()->getProgramLayerWidth())/2,0));
    arrow->setScale(1.5);
    
    menu->setPosition( Point::ZERO);
    menu->retain();
    addChild(menu,2);
    
    addChild(mapLabel,2);
    addChild(layers,2);
    addChild(tileset,2);
    
    addChild(layersCnt,2);
    addChild(tilesetCnt,2);
    
    // robots
    
    robotsPrefs = RobotPrefsScrolLayer::getInstanse();
    robotsPrefs->setContentSize(Size(screenSize.width-PMSettings::instance()->getProgramLayerWidth() - 200,sizeHeight));
    robotsPrefs->setPosition(Point(200,0));
    robotsPrefs->frame = Rect(200, 0, screenSize.width-PMSettings::instance()->getProgramLayerWidth() - 200, sizeHeight);
    
//    Robot4Edit * robot4Edit = Robot4Edit::create();
//    robot4Edit->setAnchorPoint(Point(0,0));
//    robot4Edit->setPosition(0,sizeHeight);
    
//    Label * scroll = Label::create("Скрол типо", PMSettings::instance()->getFont(), 18);
//    scroll->setAnchorPoint(Point(0,1));
//    scroll->setPosition(Point(50,50));
//    robotsPrefs->addChild(robot4Edit);
//
    auto shape = DrawNode::create();
    static Point rect[4];
    
    rect[0] = Point(200, 0);
    rect[1] = Point(200, sizeHeight);
    rect[2] = Point(screenSize.width-PMSettings::instance()->getProgramLayerWidth(), sizeHeight);
    rect[3] = Point(screenSize.width-PMSettings::instance()->getProgramLayerWidth(), 0);
    
    static Color4F green(0, 1, 0, 1);
    shape->drawPolygon(rect, 4, green, 0, green);
    shape->setPosition( Point(0, 0) );
    
    auto clipper = ClippingNode::create();
    clipper->setAnchorPoint(Point(0, 0));
    clipper->setPosition( Point::ZERO );
    clipper->setStencil(shape);
    addChild(clipper);

    
    clipper->addChild(robotsPrefs,0);
//    addChild(robot4Edit,0);
    
    return true;
}

void MapControlLayer::SaveMap (cocos2d::Ref * sender) {
std::string mapPath = PMSettings::instance()->pathToLocalMap();
    
    XMLWorldSaver(MapEditorGameLayer::getInstanse()->getWorld(), MapEditorGameLayer::getInstanse(), mapPath);
    
    CloseMap(nullptr);
    
    
    SaveRobotCountNetGame saver;
}

void MapControlLayer::CloseMap (cocos2d::Ref * sender)
{
    PointObject *object = (PointObject *)MapEditorGameLayer::getInstanse()->getWorld()->getMap()->getScroll()->getParallax()->getParallaxArray()->arr[0];
    
    Sprite *background = (Sprite *)object->getChild();
    
    object->setRatio(Point(1.0,1.0));
    object->setOffset( Point(background->getContentSize().width / 2, background->getContentSize().height / 2) );
    
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, SelectMapMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);

}

void MapControlLayer::DeleteMap (cocos2d::Ref * sender)
{
    PMSettings::instance()->deleteMap();
    
    MapEditorGameLayer::getInstanse()->saveWorldsetLocal();
    
    CloseMap(nullptr);
}

void MapControlLayer::draw(cocos2d::Renderer* renderer, const kmMat4 &transform, bool transformUpdated)
{
    lineDrawCommand.init(_globalZOrder);
    lineDrawCommand.func = CC_CALLBACK_0(MapControlLayer::onDraw, this, transform, transformUpdated);
    Director::getInstance()->getRenderer()->addCommand(&lineDrawCommand);
    
    LayerColor::draw(renderer, transform, transformUpdated);
}

void MapControlLayer::onDraw(const kmMat4 &transform, bool transformUpdated)
{
    LayerColor::onDraw(transform, transformUpdated);
    kmMat4 oldMat;
    kmGLGetMatrix(KM_GL_MODELVIEW, &oldMat);
    kmGLLoadMatrix(&_modelViewTransform);
    
    DrawPrimitives::init();
    DrawPrimitives::setDrawColor4F(255,255,255,255);
    glLineWidth(2.0f);
    //draw lines
    
    DrawPrimitives::drawLine(Point(200,0), Point(200,sizeHeight));
    kmGLLoadMatrix(&oldMat);
}

//
//  MapControl.h
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 12.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__MapControl__
#define __pictomir_cocos2d_x__MapControl__

#include <iostream>
#include "RobotPrefs.h"

class MapControlLayer : public cocos2d::LayerColor
{
    int sizeHeight;
    bool showed;
    
public:
    
    virtual bool init();
    void showLayer(cocos2d::Ref * sender);
    void plusLayers (cocos2d::Ref * sender);
    void minusLayers (cocos2d::Ref * sender);
    
    void plusTileset (cocos2d::Ref * sender);
    void minusTileset (cocos2d::Ref * sender);
    
    void SaveMap (cocos2d::Ref * sender);
    void CloseMap (cocos2d::Ref * sender);
    void DeleteMap (cocos2d::Ref * sender);
    
    static MapControlLayer* create();
    
    static MapControlLayer * instanse;
    static MapControlLayer * getInstanse();
    
    void finishMapLoading();
    
    virtual void draw(cocos2d::Renderer* renderer, const kmMat4 &transform, bool transformUpdated);
    void onDraw(const kmMat4 &transform, bool transformUpdated);
    
    void updateArrow ();
    
private:
    cocos2d::CustomCommand lineDrawCommand;
    cocos2d::MenuItemImage* arrow;
    
    cocos2d::Label* layersCnt;
    cocos2d::Label* tilesetCnt;
    
    RobotPrefsScrolLayer * robotsPrefs;

    int layersCnt_;
    int tilesetCnt_;
};

#endif /* defined(__pictomir_cocos2d_x__MapControl__) */

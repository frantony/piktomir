//
//  MapEditorGameLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 10.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "MapEditorGameLayer.h"
#include "MapControl.h"
#include "SaveRobotCountNetGame.h"

MapEditorGameLayer *MapEditorGameLayer::_instanse = nullptr;

MapEditorGameLayer *MapEditorGameLayer::getInstanse()
{
    if(_instanse == nullptr)
    {
        _instanse = new MapEditorGameLayer;
        _instanse->init();
    }
    
    return _instanse;
}

void MapEditorGameLayer::reloadWorld() {
    removeAllChildrenWithCleanup(true);
    MapControlLayer* mapControlLayer = MapControlLayer::create();
    mapControlLayer->updateArrow();
    mapControlLayer->setPosition(Point(0,cocos2d::Director::getInstance()->getWinSize().height));
    addChild(mapControlLayer,50);
    gameLayerInit();
    loadWorld(PMSettings::instance()->getWorldPart(), PMSettings::instance()->getLevel());
    mapControlLayer->finishMapLoading();
}

cocos2d::Scene* MapEditorGameLayer::scene_()
{
    // 'scene' is an autorelease object
    cocos2d::Scene *scene = cocos2d::Scene::create();
    
    // 'layer' is an autorelease object
    MapEditorGameLayer *layer = MapEditorGameLayer::getInstanse();
    
    
    // add layer as a child to scene
    scene->addChild(layer,0,0);
    
    // return the scene
    return scene;
}

bool MapEditorGameLayer::init()
{
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    world = nullptr;
    
    mapVer = PMSettings::instance()->getMapVersion();
    
    editing = false;
    FTPUser = nullptr;
    
    FTPLogin = cocos2d::UserDefault::getInstance()->getStringForKey("Login");
    FTPPass = cocos2d::UserDefault::getInstance()->getStringForKey("Pass");
    
    bool internet = false;
    
    if (internet)
    if (FTPPass != "") {
            FTPUser = new FTPuser(writablePath,FTPLogin,FTPPass);
            if(FTPUser->checkFTP())
            {
        
            }
            else
            {
            
            }
        }
    
    PMSettings::instance()->setScreenSize (cocos2d::Director::getInstance()->getWinSize());
    
    //baseRobotManager->setControlledMode(true);
    //schedule(schedule_selector(GameLayer::makeStep), 0.01f);
    
    return true;
}

bool MapEditorGameLayer::changeMapSize (int width, int height) {
    if (getWorld()->type() == pmWorld4) {
        for (int i = 0; i < world->robotCount(); ++i) {
            AbstractRobot * robot = world->robotAt(i);
            if (robot->type() == pmRobot4) {
                PlayerRobot2D* robot_ = (PlayerRobot2D*)robot;
                if (robot_->getCurPosition().x >= width || robot_->getCurPosition().y >= height) {
                    return false;
                }
            }
        }
        ((Map4*)(getWorld()->getMap()))->resize (width,height);
    }
    return true;
}

bool MapEditorGameLayer::changeMapLayers (int layers) {
    if (getWorld()->type() == pmWorld4) {
        for (int i = 0; i < world->robotCount(); ++i) {
            AbstractRobot * robot = world->robotAt(i);
            if (robot->type() == pmRobot4) {
                PlayerRobot2D* robot_ = (PlayerRobot2D*)robot;
                if (robot_->getCurLayer() >= layers) {
                    return false;
                }
            }
        }
        ((Map4*)(getWorld()->getMap()))->changeLayers (layers);
    }
    
    return true;
}

void zipFolder(std::string src, std::string dst)
{
    system(("cd "+src+"\nzip -r -q "+dst+" .").c_str());
}

void MapEditorGameLayer::saveWorldsetLocal ()
{
    tinyxml2::XMLDocument * worldSet = new tinyxml2::XMLDocument;
	
    tinyxml2::XMLDeclaration * decl = worldSet->NewDeclaration();
    worldSet->LinkEndChild( decl );
    
    tinyxml2::XMLElement * worldsNode = worldSet->NewElement("worlds");
    worldsNode->SetAttribute("count", (int)PMSettings::instance()->getWorldsCount());
    
    if(mapVer != PMSettings::instance()->getMapVersion()) {
        worldsNode->SetAttribute("version", PMSettings::instance()->getMapVersion()+1);
    } else {
        worldsNode->SetAttribute("version", PMSettings::instance()->getMapVersion());
    }
    
    worldSet->LinkEndChild( worldsNode );
    
    int worldCount = PMSettings::instance()->getWorldsCount();
    
    for (int i = 0; i < worldCount; ++i) {
        tinyxml2::XMLElement * worldNode = worldSet->NewElement("world");
        worldNode->SetAttribute("count", PMSettings::instance()->getMapsCount(i));
        worldNode->SetAttribute("worldtype", 0);
        worldsNode->LinkEndChild( worldNode );
    }

    char tmp[100] = "";
    sprintf(tmp, "%s",PMSettings::instance()->pathToWorlds(PMSettings::instance()->getLocalGame()).c_str());

    worldSet->SaveFile( tmp );
    
    delete worldSet;
    
    SaveRobotCountNetGame saver;
}

void MapEditorGameLayer::uploadMapsToServer()
{
    saveWorldsetLocal();
    
    zipFolder(writablePath+"Maps", writablePath+"Maps.zip");
    std::string mapPathStr = writablePath+"Maps.zip";
    FILE* ver = fopen((writablePath+"ver.txt").c_str(), "w");
    fprintf(ver, "%d",PMSettings::instance()->getMapVersion());
    fclose(ver);
    FTPUser->uploadMap(writablePath+"ver.txt",mapPathStr);
}


void MapEditorGameLayer::clearMap (cocos2d::Ref * sender) {
    Map4* map_ = ((Map4*)(getWorld()->getMap()));
    for (auto it = map_->mapElements.begin(); it != map_->mapElements.end(); ++it) {
        for (auto j = it->begin(); j !=it->end(); ++j) {
            for (auto k = j->begin(); k !=j->end(); ++k) {
                (*k)->setElement(0);
            }
        }
    }
    
    for (int layer = 0; layer < map_->mapElements.size(); ++layer) {
        for (int y = 0; y < map_->mapElements[layer].size(); ++y) {
            for (int x = 0; x < map_->mapElements[layer][y].size(); ++x) {
                map_->deleteLeftWall (Point (x,y), layer);
                map_->deleteUpWall (Point (x,y), layer);
            }
        }
    }
    
    map_->update();
}


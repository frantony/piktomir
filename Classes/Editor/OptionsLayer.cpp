//
//  OptionsLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 19.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "OptionsLayer.h"
#include "MainMenuLayer.h"
#include "utils.h"
#include "cocos-ext.h"
#include "FTPutils.h"
#include "FTPuser.h"
#include "MapEditorGameLayer.h"

USING_NS_CC;

Scene* OptionsLayer::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    OptionsLayer *layer = OptionsLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer,0,0);
    
    // return the scene
    return scene;
}

bool OptionsLayer::init()
{
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    Size screenSize = PMSettings::instance()->getScreenSize();
    
    setPosition( Point (0,0) );
    
    Sprite *backGround  = Sprite::create(PICT_PATH "Background/background1.png" );
    backGround->setPosition( Point(screenSize.width/2, screenSize.height/2) );
    resizeSprite(backGround, screenSize.width, screenSize.height);
    addChild(backGround, -1);
    
    
    MenuItemFont *goToMain = MenuItemFont::create(LocalizedString("MainMenu"), CC_CALLBACK_1(OptionsLayer::mainMenu,this));
    goToMain->setPosition( Point (170, screenSize.height - 25) );
    Menu *navigationMenu = Menu::create(goToMain, nullptr);
    navigationMenu->setPosition(  Point::ZERO );
    addChild(navigationMenu, 1);
    
    Label * FTPLable = Label::create(LocalizedString("FTPOptions"), PMSettings::instance()->getFont(), 28);
    FTPLable->setAnchorPoint(Point(0.5,0.5));
    FTPLable->setPosition(Point(screenSize.width / 2 + 40, screenSize.height / 2 + 50));
    addChild(FTPLable,10);
    
    cocos2d::extension::Scale9Sprite* EditBoxSprite = cocos2d::extension::Scale9Sprite::create("System/scalesprite.png");
    
    Label * LoginLabel = Label::create(LocalizedString("Login"), PMSettings::instance()->getFont(), 24);

    LoginLabel->setAnchorPoint(Point(1,0.5));
    LoginLabel->setPosition(Point(screenSize.width / 2 - 10, screenSize.height / 2));
    addChild(LoginLabel,10);
    
    login = cocos2d::extension::EditBox::create(Size(200, 40), EditBoxSprite);
    login->setText(MapEditorGameLayer::getInstanse()->FTPLogin.c_str());
    login->setFontColor(Color3B(0, 0, 0));
    login->setAnchorPoint(Point(0,0.5));
    login->setPosition(Point(screenSize.width / 2 + 10, screenSize.height / 2));
    addChild(login,10);
    
    EditBoxSprite = cocos2d::extension::Scale9Sprite::create("System/scalesprite.png");
    
    Label * PassLabel = Label::create(LocalizedString("Password"), PMSettings::instance()->getFont(), 24);

    PassLabel->setAnchorPoint(Point(1,0.5));
    PassLabel->setPosition(Point(screenSize.width / 2 - 10, screenSize.height / 2 - 50));
    addChild(PassLabel,10);
    
    pass = cocos2d::extension::EditBox::create(Size(200, 40), EditBoxSprite);
    pass->setFontColor(Color3B(0, 0, 0));
    pass->setAnchorPoint(Point(0,0.5));
    pass->setPosition(Point(screenSize.width / 2 + 10, screenSize.height / 2 - 50));
    addChild(pass,10);
    
    MenuItemFont *OK = MenuItemFont::create(LocalizedString("OkButton"), CC_CALLBACK_1(OptionsLayer::OK, this));
    OK->setPosition( Point(screenSize.width / 2 + 40, screenSize.height / 2 - 100) );
    Menu *OKMenu = Menu::create(OK, nullptr);
    OKMenu->setPosition(  Point::ZERO );
    addChild(OKMenu, 1);
    
    return true;
}

void OptionsLayer::mainMenu(cocos2d::Ref *object)
{
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, MainMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void OptionsLayer::OK(cocos2d::Ref *object)
{
    printf("%s", login->getText());
    std::string tmpLogin = login->getText();
    std::string tmpPass = pass->getText();
    MD5 md5;
    std::string tmpPassHashStr = md5.digestString(tmpPass.c_str());
    FTPuser * tmpFTPUser = new FTPuser(writablePath, tmpLogin, tmpPassHashStr);
    if(tmpFTPUser->checkFTP()) {
        if(MapEditorGameLayer::getInstanse()->FTPUser) delete MapEditorGameLayer::getInstanse()->FTPUser;
        MapEditorGameLayer::getInstanse()->FTPUser = tmpFTPUser;
        MapEditorGameLayer::getInstanse()->FTPLogin = tmpLogin;
        MapEditorGameLayer::getInstanse()->FTPPass = tmpPassHashStr;
        
        // настройки
        
        UserDefault::getInstance()->setStringForKey("Login", tmpLogin);
        UserDefault::getInstance()->setStringForKey("Pass", tmpPassHashStr);
    }
    else
    {
        MessageBox ("Неверный логин/пароль", "Неверный логин/пароль");
    }
    
    mainMenu(object);
}
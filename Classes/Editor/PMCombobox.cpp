//
//  PMCombobox.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 31.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "PMCombobox.h"


USING_NS_CC;

PMComboBox* PMComboBox::create(std::vector<std::string> values_, cocos2d::Ref* target, SEL_MenuHandler onShow, SEL_MenuHandler onEnd, SEL_MenuHandler onClose, float fontSize, const char * font)
{
    PMComboBox *pRet = new PMComboBox();
    pRet->initWithValuesAndSelector(values_, target, onShow, onEnd,onClose, fontSize, font);
    pRet->autorelease();
    
    
    return pRet;
}

bool PMComboBox::initWithValuesAndSelector(std::vector<std::string> values_, cocos2d::Ref* target_, SEL_MenuHandler onShow_, SEL_MenuHandler onEnd_, SEL_MenuHandler onClose_, float fontSize, const char * font)
{
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    onShow = onShow_;
    onEnd = onEnd_;
    onClose = onClose_;
    target = target_;
    

    float maxWidth = 0;
    float maxHeight = 0;
    
    for (int i = 0; i < values_.size(); ++i)
    {
        values.push_back(Label::create(values_[i].c_str(), font, fontSize));
        if (values[i]->getContentSize().width > maxWidth) {
            maxWidth = values[i]->getContentSize().width;
        }
        if (values[i]->getContentSize().height > maxHeight) {
            maxHeight = values[i]->getContentSize().height;
        }
    }

    maxWidth+=40;
    
    width = maxWidth;
    height = maxHeight;
    
    setContentSize(Size(maxWidth, maxHeight));
    
    button = CCLayerColor::create(Color4B(144, 144, 144, 200), maxWidth, maxHeight);
    button->setPosition(Point::ZERO);
    addChild(button);
    
    showedLabel =  Label::create(values_[0].c_str(), font, fontSize);
    showedLabel->setColor(Color3B::BLACK);
    selectedItem = 0;
    showedLabel->setPosition(Point(10,1));
    showedLabel->setAnchorPoint(Point::ZERO);
    button->addChild(showedLabel,1);
    
    Sprite * left = Sprite::create("System/ComboboxLeft.png");
    Sprite * center = Sprite::create("System/ComboboxCenter.png");
    Sprite * right = Sprite::create("System/ComboboxRight.png");
    
    left->setAnchorPoint(Point::ZERO);
    center->setAnchorPoint(Point::ZERO);
    right->setAnchorPoint(Point(1,0));
    
    left->setPosition(Point::ZERO);
    center->setPosition(Point(10,0));
    right->setPosition(Point(maxWidth,0));
    
    left->setScaleY(maxHeight/30);
    center->setScaleY(maxHeight/30);
    center->setScaleX((maxWidth-40)/20);
    right->setScaleY(maxHeight/30);
    
    button->addChild(left,0);
    button->addChild(center,0);
    button->addChild(right,0);
    
    scroll = ScrollView::create(Size(maxWidth, maxHeight * values.size() + 16));
    scroll->setPosition(Point(0, - maxHeight * values.size() - 16));
    
    scroll->setViewSize( Size(maxWidth, maxHeight * values.size() + 16) );
    scroll->setContentSize( Size(maxWidth, maxHeight * values.size() + 16) );
    scroll->setAnchorPoint( Point(0,0) );
    scroll->setContentOffset( Point(0, 0), true);
    scroll->setClippingToBounds(true);
    scroll->setDirection(ScrollView::Direction::VERTICAL);
    scroll->setZoomScale(0.5);
    scroll->setBounceable(true);
    
    scroll->setVisible(false);
    addChild(scroll,100);
    
    center = Sprite::create("System/ComboboxCenter.png");
    center->setAnchorPoint(Point::ZERO);
    center->setPosition(Point::ZERO);
    center->setScaleY((maxHeight * values.size()+16)/30);
    center->setScaleX(maxWidth/20);
    scroll->addChild(center,0);
    
    
    for (int i = 0; i < values.size(); ++i)
    {
        values[values.size()-i-1]->setPosition(Point(5,(i+1)*maxHeight + 8));
        values[values.size()-i-1]->setAnchorPoint(Point(0,1));
        values[values.size()-i-1]->setColor(Color3B::BLACK);
        scroll->addChild(values[values.size()-i-1], 100);
    }
    
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(PMComboBox::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(PMComboBox::touchEnded, this);
    listener->onTouchMoved = CC_CALLBACK_2(PMComboBox::touchMoved, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

void PMComboBox::selectItemAtIndex (int index) {
    selectedItem = index;
    scroll->setVisible(false);
    showed = false;
    showedLabel->setString(values[selectedItem]->getString());
}

bool PMComboBox::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(!enabled)
        return false;
    
    if (showed)
    {
        scroll->ccTouchBegan(touch, pEvent);
        return true;
    }
    else
    {
        Point touchPoint = this->convertTouchToNodeSpace(touch);
        
        if (Rect(0, 0, width, height).containsPoint(touchPoint)) {
            return true;
        }
        else return false;
    }
}

void PMComboBox::touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    scroll->ccTouchMoved(touch, pEvent);
}

void PMComboBox::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if (showed)
    {
        scroll->ccTouchEnded(touch, pEvent);
        Point touchPoint = this->convertToNodeSpace(touch->getStartLocation());
        touchPoint.y = -touchPoint.y;
        if (!Rect(0, 0, width, ((float)values.size()) * height).containsPoint(touchPoint)) {
            if(onShow && target)(target->*onClose)(this);
            scroll->setVisible(false);
            showed = false;
        }
        else
        {
            Point dif = touch->getStartLocation() - touch->getLocation();
            dif.x = fabs(dif.x);
            dif.y = fabs(dif.y);
            if (dif.x < 5 && dif.y < 5)
            {
                selectedItem = (touchPoint.y-15)/height;
                scroll->setVisible(false);
                showed = false;
                showedLabel->setString(values[selectedItem]->getString());
                if(onEnd && target)(target->*onEnd)(this);
            }
        }
    }
    else
    {
        Point touchPoint = this->convertTouchToNodeSpace(touch);
        if (Rect(0, 0, width, height).containsPoint(touchPoint)) {
            if(onShow && target)(target->*onShow)(this);
            scroll->setVisible(true);
            showed = true;
        }
    }
}

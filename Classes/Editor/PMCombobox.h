//
//  PMCombobox.h
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 31.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PMCombobox__
#define __pictomir_cocos2d_x__PMCombobox__

#include <iostream>
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class PMComboBox : public Layer {
public:
    virtual bool initWithValuesAndSelector(std::vector<std::string>, cocos2d::Ref*, SEL_MenuHandler, SEL_MenuHandler,SEL_MenuHandler, float, const char*);
    static PMComboBox* create (std::vector<std::string>, cocos2d::Ref*, SEL_MenuHandler, SEL_MenuHandler,SEL_MenuHandler, float, const char*);
    
    int getSelectedItemIndex() {
        return selectedItem;
    }
    
    void selectItemAtIndex (int index);
    
    PMComboBox () :
        values(),
        button(nullptr),
        scroll(nullptr),
        onShow(nullptr),
        onEnd(nullptr),
        selectedItem(-1),
        showedLabel(nullptr),
        showed(false),
        width(0),
        height(0),
        enabled(true)
    {
        
    }
    
private:
    std::vector<Label*> values;
    LayerColor * button;
    ScrollView * scroll;
    SEL_MenuHandler onShow;
    SEL_MenuHandler onEnd;
    SEL_MenuHandler onClose;
    cocos2d::Ref* target;
    int selectedItem;
    
    Label* showedLabel;
    
    bool showed;
    
    float width;
    float height;

    CC_SYNTHESIZE(bool, enabled, Enabled)
    
    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
};


#endif /* defined(__pictomir_cocos2d_x__PMCombobox__) */

//
//  PMPopoverMethodButton.h
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 23.10.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PMPopoverMethodButton__
#define __pictomir_cocos2d_x__PMPopoverMethodButton__

#include <iostream>
#include "cocos-ext.h"
#include "MethodLayer.h"

USING_NS_CC;
USING_NS_CC_EXT;

class PMPopoverMethodButton : public LayerColor {
public:
    static PMPopoverMethodButton * instanse;
    static PMPopoverMethodButton * getInstanse ();
    static PMPopoverMethodButton* create(int button, int width, int realHeight, Node* parent);
    virtual bool init();
    void setup (int button, int width, int realHeight);

    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    
    CC_SYNTHESIZE(bool, showed, Showed);
    CC_SYNTHESIZE(int, methodIndex, MethodIndex);
    
    int index_;
    MethodLayer * parent_;
};

#endif /* defined(__pictomir_cocos2d_x__PMPopoverMethodButton__) */

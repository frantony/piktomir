//
//  PMPopoverRepeaterButton.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 17.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "PMPopoverRepeaterButton.h"
#include "MethodLayer.h"
#include "PMTextureCache.h"
#include "RobotMethod.h"
#include "ProgramLayer.h"

const int PopoverWidth = 320;
const int PopoverHeight = 40;

PMPopoverRepeaterButton * PMPopoverRepeaterButton::instanse = nullptr;

PMPopoverRepeaterButton* PMPopoverRepeaterButton::create(int width, int realHeight, Node* parent) {
    
    if(!instanse) {
        instanse = new PMPopoverRepeaterButton;
        
        instanse->init();
        instanse->setup(width, realHeight);
        
        parent->addChild(instanse, 20);
    }
    
    instanse->parent_ = (MethodLayer*)parent;
    
    return instanse;
}

PMPopoverRepeaterButton* PMPopoverRepeaterButton::getInstanse () {
    return instanse;
}

bool PMPopoverRepeaterButton::init() {
    if ( !CCLayerColor::init() )
    {
        return false;
    }
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(PMPopoverRepeaterButton::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(PMPopoverRepeaterButton::touchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    setContentSize(Size(PopoverWidth, PopoverHeight));
    setColor(Color3B(144, 144, 144));
    setOpacity(230);
    setAnchorPoint(Point(0.5, 0));
    
    Sprite *Off = PMTextureCache::instanse()->getRepeaterSprite(1, DISABLED_STATE);
    Off->setPosition(Point(5,5));
    Off->setAnchorPoint( Point::ZERO);
    Off->setScaleY(0.7);
    Off->setScaleX(0.7);
    addChild(Off);
    
    for (int i = 1; i <= 7 ; ++i)
    {
        Sprite *r1 = PMTextureCache::instanse()->getRepeaterSprite(i, NORMAL_STATE);

        r1->setPosition(Point(5+35*i,5));
        r1->setAnchorPoint( Point::ZERO);
        r1->setScaleY(0.7);
        r1->setScaleX(0.7);
        addChild(r1);
    }
    
    Sprite *r8 = PMTextureCache::instanse()->getRepeaterSprite(0, NORMAL_STATE);
    
    r8->setPosition(Point(5+35*8,5));
    r8->setAnchorPoint( Point::ZERO);
    r8->setScaleY(0.7);
    r8->setScaleX(0.7);
    addChild(r8);
    
    return true;
}

void PMPopoverRepeaterButton::setup(int width, int realHeight) {
    showed = true;
    setVisible(true);
    int Y = realHeight - 5;
    setPosition( Point(-20, Y ) );
}

bool PMPopoverRepeaterButton::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent) {
    return true;
}

void PMPopoverRepeaterButton::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent) {
    showed = false;
    Point touchPoint = convertTouchToNodeSpace(touch);
    
    if (touchPoint.x < PopoverWidth && touchPoint. x > 0 &&
        touchPoint.y < PopoverHeight && touchPoint.y > 0) {
        int ans = touchPoint.x/35;
        printf("%d\n",ans);
        
        if(ans > 0 && ans < 7)parent_->setRepeater (ans);
        if(ans == 7)parent_->setRepeater (-1);
        if(ans == 0)parent_->setRepeater (-2);
    } else {
        
    }
    parent_->getParentLayer()->setEnabled(true);
    removeFromParent();
    instanse = nullptr;
    setVisible(false);
    
    parent_->removeAllChildrenWithCleanup(true);
    parent_->drawFunctions();
}


//
//  Robot4Edit.h
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 15.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__Robot4Edit__
#define __pictomir_cocos2d_x__Robot4Edit__

#include "Robot4.h"
#include "RobotPrefs.h"
#include "PMCombobox.h"



class Robot4Edit : public cocos2d::LayerColor {
    cocos2d::CustomCommand lineDrawCommand;
    Robot4* robot_;
    
    int sizeHeight;
    virtual bool init();
    
    void plusEndX (cocos2d::Ref * sender);
    void minusEndX (cocos2d::Ref * sender);
    void plusEndY (cocos2d::Ref * sender);
    void minusEndY (cocos2d::Ref * sender);
    
    void showRobotStates (cocos2d::Ref * sender);
    
    void setDown (cocos2d::Ref * sender);
    void setUp (cocos2d::Ref * sender);
    void setLeft (cocos2d::Ref * sender);
    void setRight (cocos2d::Ref * sender);
    
    cocos2d::MenuItemLabel *stateLable;
    
public:
    int index;
    RobotPrefsScrolLayer* parent_;
    virtual void draw(cocos2d::Renderer* renderer, const kmMat4 &transform, bool transformUpdated);
    void onDraw(const kmMat4 &transform, bool transformUpdated);
    Robot4Edit() : cocos2d::LayerColor(),
    robot_(nullptr),
    state(0),
    endXCnt_ (0),
    endYCnt_ (0),
    endXCnt (nullptr),
    endYCnt (nullptr),
    combobox(false),
    endGrassCnt_(0)
    {
    }
    
    CREATE_FUNC(Robot4Edit);
    
    cocos2d::Label* endXCnt;
    cocos2d::Label* endYCnt;
    
    cocos2d::Label* paintGrassCnt;
    cocos2d::extension::ControlSlider *paintSlider;
    
    cocos2d::LayerColor * types;
    
    void setRobot(Robot4* robot);
    
    int endXCnt_;
    int endYCnt_;
    int endGrassCnt_;
    
    int state;
    
    bool combobox;
    
    void onShowCombobox(cocos2d::Ref* sender);
    void onEndCombobox(cocos2d::Ref* sender);
    void onCloseCombobox(cocos2d::Ref* sender);
    
    void showFunctions (cocos2d::Ref*);
    void setPaint (cocos2d::Ref *sender, cocos2d::extension::Control::EventType controlEvent);
    
    cocos2d::Menu * menu;
    
    PMComboBox * combobox_;
};

#endif /* defined(__pictomir_cocos2d_x__Robot4Edit__) */

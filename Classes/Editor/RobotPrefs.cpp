//
//  RobotPrefs.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 15.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "RobotPrefs.h"
#include "MapEditorGameLayer.h"
#include "Robot4Edit.h"
#include "ProgramLayer.h"
#include "RobotMethod.h"

USING_NS_CC;

RobotPrefsScrolLayer* RobotPrefsScrolLayer::instanse = nullptr;

RobotPrefsScrolLayer* RobotPrefsScrolLayer::getInstanse() {
    if(!instanse) {
        instanse = new RobotPrefsScrolLayer;
        instanse->setActiveRobot(0);
    }
    return instanse;
}

bool RobotPrefsScrolLayer::init() {
    
    if ( !Layer::init() )
    {
        return false;
    }
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(RobotPrefsScrolLayer::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(RobotPrefsScrolLayer::touchEnded, this);
    listener->onTouchMoved = CC_CALLBACK_2(RobotPrefsScrolLayer::touchMoved, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

bool RobotPrefsScrolLayer::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(!active)
        return false;
    if(!frame.containsPoint((this->convertTouchToNodeSpace(touch)+ getPosition()))) return false;
    
    for (auto it = robots_.begin(); it != robots_.end(); it++) {
        Rect rect = (*it)->paintSlider->getBoundingBox();
        rect.origin.x = 0;
        rect.origin.y = 0;
        Point point = (*it)->paintSlider->getBackgroundSprite()->convertTouchToNodeSpace(touch);
        if (rect.containsPoint(point)) {
            return false;
        }
    }
    
    Point touchPoint = touch->getStartLocation();

    
    startPoint = touchPoint;
    
    return true;
}


void RobotPrefsScrolLayer::touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    printf("%f\n",touch->getDelta().x);
    
    setPosition((getPosition()+Point(touch->getDelta().x,0)));
}

void RobotPrefsScrolLayer::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    int robotCount = MapEditorGameLayer::getInstanse()->getWorld()->robotCount();
    
    if ( getPosition().x > frame.origin.x ) {
        setPosition(Point(frame.origin.x,0));
    } else if ( 200 * (robotCount+1) < frame.size.width ) {
        setPosition(Point(frame.origin.x,0));
    } else {
        if(getPosition().x + 200 *  (robotCount+1) < frame.origin.x + frame.size.width) {
            
            setPosition(Point(frame.origin.x + frame.size.width - 200 * (robotCount + 1),0));
            
        } else {
            
        }
    }
}

void RobotPrefsScrolLayer::finishLoading() {
    removeAllChildrenWithCleanup(true);
    for (int i = 0; i < MapEditorGameLayer::getInstanse()->getWorld()->robotCount(); ++i) {
        AbstractRobot * robot = MapEditorGameLayer::getInstanse()->getWorld()->robotAt(i);
        
        if (robot->type() == pmRobot4) {
                Robot4Edit * robot4Edit = Robot4Edit::create();
                robot4Edit->setAnchorPoint(Point(0,0));
                robot4Edit->setPosition(Point(200*i,frame.size.height));
                robot4Edit->setRobot((Robot4*)robot);
                robot4Edit->parent_ = this;
                robot4Edit->index = i;
                if(i==0)robot4Edit->showFunctions(nullptr);
                addChild(robot4Edit);
                robots_.push_back(robot4Edit);
        }
    }
    
    MenuItemLabel * newRobot = MenuItemLabel::create(
                                                     Label::create(
                                                                        LocalizedString("AddRobot"),
                                                                        PMSettings::instance()->getFont(),
                                                                        20),
                                                     CC_CALLBACK_1(RobotPrefsScrolLayer::addRobot,this)
                                                     );
    newRobot->setAnchorPoint( Point::ZERO);
    newRobot->setPosition(Point(10,0));
    
    Menu * menu = Menu::create(newRobot, nullptr);
    menu->setAnchorPoint( Point::ZERO);
    menu->setPosition(Point(MapEditorGameLayer::getInstanse()->getWorld()->robotCount()*200, frame.size.height / 2 ) );

    addChild(menu);
}

void RobotPrefsScrolLayer::addRobot (cocos2d::Ref*) {
    AbstractRobot *robot;
    
    RobotType robotType = pmRobot4;
    
    AbstractWorld * world = MapEditorGameLayer::getInstanse()->getWorld();
    
    switch (robotType)
    {
        case pmRobot4:
            robot = Robot4::create(world);
            break;
            
        case pmLamp4:
            robot = Lamp4::create(world);
            break;
            
        case pmTrasportRobot4:
            robot = TransportRobot4::create(world);
            break;
            
        default:
            robot = nullptr;
            break;
    }
    
    if (!robot) return;
    
    switch (robotType)
    {
        case pmRobot4:
            ((Robot4*)robot)->setPosition( Point(1, 1), 0, 0);
            ((Robot4*)robot)->setStartPosition( Point(1,1) );
            ((Robot4*)robot)->setStartDirection(0);
            ((Robot4*)robot)->setStartLayer(0);
            break;
            
        case pmLamp4:
            break;
            
        case pmTrasportRobot4:
            break;
            
        default:
            break;
    }
    
    if(robot->parentType() == pmPlayerRobot )
        ((PlayerRobot *)robot)->initTexture();
    
    world->addRobot(robot);
    
    // Tasks
    robot->setTaskList(new TaskList(pmCompleteAllTasks));
    
    ((Map4*)world->getMap())->drawNewRobot((PlayerRobot2D*)robot);
    
    // Methods
    
    ProgramLayer * programLayer = ProgramLayer::create(MapEditorGameLayer::getInstanse());
    MapEditorGameLayer::getInstanse()->addChild(programLayer, 15, 1);
    programLayer->setVisible(false);
    MethodLayer *methodLayer = MethodLayer::create(programLayer);
    methodLayer->setIndex(0);
    programLayer->layers.push_back(methodLayer);
    
    methodLayer->setMethodID(CMDMAIN);
    
    methodLayer->setName( "main" );
    methodLayer->setRepeater (1);
    methodLayer->setCondition (0);
    methodLayer->setUseRepeater (false);
    methodLayer->setUseCondition ( false);
    methodLayer->setWidth ( 1);
    methodLayer->setHeight ( 1);

    std::vector<_method> methods;
    methods.push_back(_method((_method::ExecuteType)(0), _method::pmNormal, _method::pmExecuteOnTrue));
    methodLayer->setMethods(methods);
    
    methodLayer->setHidden ( false);
    
    methodLayer->setResizable ( false);
    methodLayer->setMaxHeight (1);
    methodLayer->setMinHeight (1);
    methodLayer->drawFunctions();
    
    ((Robot4*)robot)->program = programLayer;
    
    programLayer->drawLayers(world);
    
    finishLoading();
}


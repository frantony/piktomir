//
//  RobotPrefs.h
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 15.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__RobotPrefs__
#define __pictomir_cocos2d_x__RobotPrefs__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "PMSettings.h"

class Robot4Edit;

class RobotPrefsScrolLayer : public cocos2d::Layer
{
public:
    virtual bool init();
    
    
    RobotPrefsScrolLayer() :
    cocos2d::Layer()
    {
        activeRobot = 0;
        active = true;
        init();
    }
    
    void finishLoading ();
    
    cocos2d::Rect frame;
    
    static RobotPrefsScrolLayer * instanse;
    static RobotPrefsScrolLayer * getInstanse();
    
    void addRobot(cocos2d::Ref*);
    
protected:
    SYNTHESIZE(bool, active, Active);
    SYNTHESIZE(int, activeRobot, ActiveRobot);
    std::vector<Robot4Edit*> robots_;
private:
    cocos2d::Point startPoint;
    
    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    
    void touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
};

#endif /* defined(__pictomir_cocos2d_x__RobotPrefs__) */

//
//  SaveRobotCountNetGame.cpp
//  Piktomir
//
//  Created by Danila Eremin on 04.04.14.
//
//

#include "SaveRobotCountNetGame.h"
#include "PMSettings.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "tinyxml2.h"

USING_NS_CC;
using namespace tinyxml2;

SaveRobotCountNetGame::SaveRobotCountNetGame() {
    auto settings = PMSettings::instance();
    
    std::string worlds = settings->pathToWorlds(false);
    
    ssize_t size;
    char *buf = (char *)FileUtils::getInstance()->getFileData(worlds.c_str(),"r", &size);
    
    tinyxml2::XMLDocument doc;
    doc.Parse(buf,size);
    
    if(doc.Error())
    {
        CCLOG("error:%s\n",doc.GetErrorStr2());
        
        delete buf;
    }
    
    XMLElement *worldsNode = doc.FirstChildElement("worlds");
    int world = 0;
    
    
    
    for (auto curNode = worldsNode->FirstChildElement(); curNode; curNode = curNode->NextSiblingElement())
    {
        if(!strcmp(curNode->Name(), "world")) {
            int cnt = curNode->UnsignedAttribute("count");
            
			tinyxml2::XMLDocument* save = new  tinyxml2::XMLDocument;
            
            XMLDeclaration * decl = save->NewDeclaration();
            save->LinkEndChild( decl );
            
            XMLElement * worldNode = save->NewElement("world");
            save->LinkEndChild( worldNode );
            
            for (int i = 1; i <= cnt; ++i) {
                int robots = getRobotCountInMap(world, i);
                
                XMLElement * mapNode = save->NewElement("map");
                worldNode->LinkEndChild( mapNode );
                mapNode->SetAttribute("count", robots);
            }
            
            std:: string path = settings->pathToWorld(world, false)+"/world.xml";
            world++;
            
            save->SaveFile( path.c_str() );
            
            delete save;

        }
    }
}

int SaveRobotCountNetGame::getRobotCountInMap(int world, int map) {
    auto settings = PMSettings::instance();
    
    std::string mapPath = settings->pathToMap(world, map, false);
    
	ssize_t size;
    char *buf = (char *)FileUtils::getInstance()->getFileData(mapPath.c_str(),"r", &size);
    
	tinyxml2::XMLDocument doc;
    doc.Parse(buf,size);
    
    
    if(doc.Error())
    {
        CCLOG("error:%s\n",doc.GetErrorStr2());
        
        delete buf;
    }
    
    XMLElement *mapNode = doc.FirstChildElement("map");
    
    for (auto curNode = mapNode->FirstChildElement(); curNode; curNode = curNode->NextSiblingElement())
    {
        if(!strcmp(curNode->Name(), "robots"))
        {
            int cnt = 0;
            for (auto robot = curNode->FirstChildElement(); robot; robot = robot->NextSiblingElement()) {
                if(!strcmp(robot->Name(), "robot")) {
                    cnt++;
                }
            }
            return cnt;
        }
    }
    
    return 0;
}
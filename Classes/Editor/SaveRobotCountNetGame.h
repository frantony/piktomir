//
//  SaveRobotCountNetGame.h
//  Piktomir
//
//  Created by Danila Eremin on 04.04.14.
//
//

#ifndef __Piktomir__SaveRobotCountNetGame__
#define __Piktomir__SaveRobotCountNetGame__

#include <iostream>

class SaveRobotCountNetGame {
    
public:
    SaveRobotCountNetGame();
    
    int getRobotCountInMap(int world, int map);
};

#endif /* defined(__Piktomir__SaveRobotCountNetGame__) */

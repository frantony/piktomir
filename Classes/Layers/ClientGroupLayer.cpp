//
//  ClientGroup.cpp
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 19.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "ClientGroupLayer.h"

USING_NS_CC;
using namespace std;

bool ClientGroupLayer::initWithClients(int index, const Map<int,ClientLayer*> &newClients ,float width)
{
    if(!init())
        return false;
    
    clients = newClients;

    headerLabel = Label::create(StringUtils::format("Группа %d",index) , "Arial", 18);
    addChild(headerLabel);
    headerLabel->setAnchorPoint( Point::ZERO );
    
    setContentSize( Size(width - 2 * CLIENTLAYER_BOUND, getContentSize().height) );
    
    drawClients();
    
    return true;
}

bool ClientGroupLayer::init()
{
    if ( !LayerColor::init() )
    {
        return false;
    }
    
    
    
    
    Sprite *uncheckedSprite = Sprite::create(PICT_PATH "System/unchecked1.png");
    
    checkBox = MenuItemSprite::create(uncheckedSprite, uncheckedSprite, CC_CALLBACK_1(ClientGroupLayer::onCheckClicked, this));
    checkBox->setPosition( Point::ZERO );
    checkBox->setAnchorPoint(Point::ZERO);
    checked = false;
    
    checkBoxMenu= Menu::create(checkBox, nullptr);
    checkBoxMenu->setAnchorPoint( Point::ZERO );
    checkBoxMenu->setContentSize( uncheckedSprite->getContentSize() );
    addChild(checkBoxMenu);

    gameLabel = Label::create("", "Arial", 18);
    gameLabel->setAnchorPoint( Point::ZERO );
    addChild(gameLabel);
    
    return true;
}

void ClientGroupLayer::setGame(int world, int level)
{
    gameLabel->setString(StringUtils::format("Игра: %d.%d",world + 1,level + 1));
}

void ClientGroupLayer::clearGame()
{
    gameLabel->setString("");
}

void ClientGroupLayer::removeClient(PMClient *client)
{
    if(clients.find(client->peer_->connectID) != clients.end())
    {
        clients.erase(client->peer_->connectID);
        
        drawClients();
    }
}

cocos2d::Map<int,ClientLayer*> ClientGroupLayer::eraseClients(float width)
{
    Map<int,ClientLayer*> erased;
    
    for(auto i = clients.begin(); i != clients.end();)
    {
        if(i->second->getChecked())
        {
            i->second->setWidth(width);
            erased.insert(i->first, i->second);
            removeChild(i->second);
            i = clients.erase(i);
        }
        else
        {
            ++i;
        }
    }
    
    drawClients();
    
    return erased;
}


void ClientGroupLayer::drawClients()
{
    checkBoxMenu->retain();
    headerLabel->retain();
    gameLabel->retain();
    
    float height = 0.0;
    
    height += headerLabel->getContentSize().height;
    
    for(auto &i : clients)
    {
        height += i.second->getContentSize().height + CLIENTLIST_SEPARATOR;
        //i.second->retain();
    }
    
    removeAllChildrenWithCleanup(false);
    setContentSize( Size(getContentSize().width, height) );
    
    float lastY = height - headerLabel->getContentSize().height;
    
    addChild(headerLabel);
    headerLabel->setPosition( Point(checkBoxMenu->getContentSize().width + 10, lastY) );
    headerLabel->release();
    
    addChild(checkBoxMenu);
    checkBoxMenu->setPosition( Point(0, lastY) );
    checkBoxMenu->release();
    
    addChild(gameLabel);
    gameLabel->setPosition( Point(headerLabel->getPositionX() + headerLabel->getContentSize().width + 10.0, lastY) );
    gameLabel->release();
    
    for(auto &i : clients)
    {
        lastY -= i.second->getContentSize().height + CLIENTLIST_SEPARATOR;
        i.second->setAnchorPoint( Point::ZERO );
        i.second->setPosition( Point(CLIENTLAYER_BOUND, lastY) );
        i.second->setContentSize( Size(getContentSize().width - 2 * CLIENTLAYER_BOUND, i.second->getContentSize().height) );
        //i->second->release();
        addChild(i.second);
    }
}

void ClientGroupLayer::forceSetChecked(bool flag)
{
    checked = flag;
    
    if(checked)
    {
        checkBox->setNormalImage(Sprite::create(PICT_PATH "System/checked1.png"));
        checkBox->setSelectedImage(Sprite::create(PICT_PATH "System/checked1.png"));
    }
    else
    {
        checkBox->setNormalImage(Sprite::create(PICT_PATH "System/unchecked1.png"));
        checkBox->setSelectedImage(Sprite::create(PICT_PATH "System/unchecked1.png"));
    }
    
    for(auto &i : clients)
    {
        i.second->forceSetChecked(flag);
    }
}

void ClientGroupLayer::onCheckClicked(cocos2d::Ref *sender)
{
    checked = !checked;
    
    if(checked)
    {
        checkBox->setNormalImage(Sprite::create(PICT_PATH "System/checked1.png"));
        checkBox->setSelectedImage(Sprite::create(PICT_PATH "System/checked1.png"));
    }
    else
    {
        checkBox->setNormalImage(Sprite::create(PICT_PATH "System/unchecked1.png"));
        checkBox->setSelectedImage(Sprite::create(PICT_PATH "System/unchecked1.png"));
    }
}
//
//  ClientGroup.h
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 19.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__ClientGroup__
#define __pictomir_cocos2d_x__ClientGroup__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "PMSettings.h"

#include "ClientLayer.h"

#include "PMClient.h"

class ClientGroupLayer :public cocos2d::LayerColor
{
    SYNTHESIZE(bool, checked, Checked);
    
public:
    ClientGroupLayer()
    {
        
    }
    
    virtual bool init();

    bool initWithClients(int index, const cocos2d::Map<int,ClientLayer*> &clients ,float width);
    
    CREATE_FUNC(ClientGroupLayer);

    static ClientGroupLayer* create(int index, const cocos2d::Map<int,ClientLayer*> &clients, float width)
    {
        ClientGroupLayer *pRet = new ClientGroupLayer();
        if (pRet && pRet->initWithClients(index, clients, width))
        {
            pRet->autorelease();
            return pRet;
        }
        else
        {
            delete pRet;
            pRet = nullptr;
            return nullptr;
        }
    }
    
    
    ~ClientGroupLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    void removeClient(PMClient *client);
    
    cocos2d::Map<int,ClientLayer*> &getClients()
    {
        return clients;
    }
    
    void addClients(const cocos2d::Map<int,ClientLayer*> &clientList)
    {
        for(auto &i : clientList)
            clients.insert(i.first, i.second);
        
        drawClients();
    }
    
    int size()
    {
        return clients.size();
    }
    
    cocos2d::Map<int,ClientLayer*> eraseClients(float width);
    
    void forceSetChecked(bool flag);
    
    void setGame(int world, int level);
    void clearGame();
    
private:
    cocos2d::Label *headerLabel;
    cocos2d::Label *gameLabel;
    cocos2d::Map<int,ClientLayer*> clients;
    
    void drawClients();
    
    cocos2d::MenuItemSprite *checkBox;
    cocos2d::Menu *checkBoxMenu;
    
    void onCheckClicked(cocos2d::Ref *sender);
};
#endif /* defined(__pictomir_cocos2d_x__ClientGroup__) */

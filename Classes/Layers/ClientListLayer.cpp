//
//  ClientListLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 07.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "ClientListLayer.h"
#include "PMSettings.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

#include "PMClient.h"

bool ClientListLayer::init()
{
    if ( !ScrollView::init() )
    {
        return false;
    }
    
    setDirection(ScrollView::Direction::VERTICAL);
    setZoomScale(0.5);
    setClippingToBounds(true);
    setBounceable(false);
    
    return true;
}

bool ClientListLayer::initWithHeight(float height)
{
    if ( !init() )
    {
        return false;
    }
    
    setViewSize( Size(400 - 2 * CLIENTLIST_BOUND,height) );
    
    return true;
}

void ClientListLayer::refreshClientList(float dt)
{
//    getContainer()->removeAllChildrenWithCleanup(true);
//    
//    Label *label = Label::create("Text1", "Arial", 18);
//    label->setPosition( Point(0, 0) );
//    
//    setContentSize(Size(500, label->getContentSize().height));
//    setContentOffset(Point(0,PMSettings::instance()->getScreenSize().height - label->getContentSize().height),false);
//    
//    addChild(label);

//    clientList.clear();
//    
//    for(int i = 0; i < server->clients_.size(); ++i)
//        clientList.push_back(ClientLayer::create(server->clients_[i].name_.c_str(), getViewSize().width,server->clients_[i].peer_->connectID));
//    
//    drawClients();
}

void ClientListLayer::addClient(PMClient *client)
{
    clientList.insert(client->getId(),ClientLayer::create(client->name_.c_str(), getViewSize().width));
    
    drawClients();
}

void ClientListLayer::removeClient(PMClient *client)
{
    if(clientList.find(client->getId()) != clientList.end())
    {
        clientList.erase(client->getId());
    }
    else
    {
        for(int i = 0; i < groupList.size(); ++i)
        {
            groupList.at(i)->removeClient(client);
        }
    }
    
    removeEmptyGroups();
    drawClients();
}

void ClientListLayer::createGroup()
{
    Map<int,ClientLayer *> groupClients;
    
    for(auto i = clientList.begin(); i != clientList.end();)
    {
        if(i->second->getChecked())
        {
            groupClients.insert(i->first, i->second);
            _container->removeChild(i->second);
            i = clientList.erase(i);
        }
        else
        {
            ++i;
        }
    }
    
    groupList.pushBack(ClientGroupLayer::create(groupList.size() + 1 ,groupClients, getViewSize().width));
    
    drawClients();
}

void ClientListLayer::deleteGroups()
{
    for(auto i = groupList.begin(); i != groupList.end();)
    {
        if((*i)->getChecked())
        {
            addClients((*i)->getClients());
            
            removeChild(*i,true);
             i = groupList.erase(i);
        }
        else
        {
            ++i;
        }
    }
    
    drawClients();
}

void ClientListLayer::removeEmptyGroups()
{
    for(auto i = groupList.begin(); i != groupList.end();)
    {
        if((*i)->size() == 0)
        {
            removeChild(*i,true);
            i = groupList.erase(i);
        }
        else
        {
            ++i;
        }
    }
    
    drawClients();
}

void ClientListLayer::addToGroup()
{
    Map<int,ClientLayer *> groupClients;
    
    for(auto i = clientList.begin(); i != clientList.end();)
    {
        if(i->second->getChecked())
        {
            groupClients.insert(i->first, i->second);
            _container->removeChild(i->second);
             i = clientList.erase(i);
        }
        else
        {
            ++i;
        }
    }
    
    for(int i = 0; i < groupList.size(); ++i)
    {
        if(groupList.at(i)->getChecked())
        {
            groupList.at(i)->addClients(groupClients);
            break;
        }
    }
    
    drawClients();
}

void ClientListLayer::deleteFromGroup()
{
    for(int i = 0; i < groupList.size(); ++i)
    {
        if(groupList.at(i)->getChecked())
        {
            addClients(groupList.at(i)->eraseClients(getViewSize().width));
        }
    }
    
    removeEmptyGroups();
}

void ClientListLayer::loadClientList()
{
//    clientList.insert(pair<int, ClientLayer*>(255,ClientLayer::create("Test1", getViewSize().width)));
//    clientList.insert(pair<int, ClientLayer*>(256,ClientLayer::create("Test2", getViewSize().width)));
//    clientList.insert(pair<int, ClientLayer*>(257,ClientLayer::create("Test3", getViewSize().width)));
//    
//    std::map<int, ClientLayer *> list;
//    list.insert(pair<int, ClientLayer*>(258,ClientLayer::create("Test4", getViewSize().width)));
//    list.insert(pair<int, ClientLayer*>(259,ClientLayer::create("Test5", getViewSize().width)));
//    
//    groupList.push_back(ClientGroupLayer::create(groupList.size() +1, list, getViewSize().width));
//    
//    drawClients();
}

void ClientListLayer::drawClients()
{
    float height = 0.0;
    
    
    for(auto &i : groupList)
    {
        height += i->getContentSize().height + CLIENTLIST_SEPARATOR;
        //groupList.at(i)->retain();
    }
    
    for(auto &i : clientList)
    {
        height += i.second->getContentSize().height + CLIENTLIST_SEPARATOR;
        //i.second->retain();
    }
    
    _container->removeAllChildrenWithCleanup(false);
    
    setContentSize(Size(getViewSize().width, height));
    setContentOffset(Point(0,getContentSize().height - height),false);
                   
    float lastY = height;
    
    for(auto &i : groupList)
    {
        lastY -= i->getContentSize().height + CLIENTLIST_SEPARATOR;
        addChild(i, 1);
        //groupList.at(i)->release();
        i->setPosition( Point(CLIENTLAYER_BOUND,lastY) );
        i->setAnchorPoint( Point(0, 0) );
    }
    
    for(auto &i : clientList)
    {
        lastY -= i.second->getContentSize().height + CLIENTLIST_SEPARATOR;
        addChild(i.second, 1);
        //i->second->release();
        i.second->setPosition( Point(CLIENTLAYER_BOUND, lastY) );
        i.second->setAnchorPoint( Point(0, 0) );
    }
}
//
//  ClientListLayer.h
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 07.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__ClientListLayer__
#define __pictomir_cocos2d_x__ClientListLayer__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "GameLayer.h"

#include "PMServerHost.h"
#include "ClientGroupLayer.h"

const float CLIENTLIST_BOUND = 20.0;

class ClientListLayer :public cocos2d::extension::ScrollView
{
public:

    virtual bool init();
    bool initWithHeight(float height);
    
    CREATE_FUNC(ClientListLayer);
    
    static ClientListLayer* create(float height)
    {
        ClientListLayer *pRet = new ClientListLayer();
        if (pRet && pRet->initWithHeight(height))
        {
            pRet->autorelease();
            return pRet;
        }
        else
        {
            delete pRet;
            pRet = nullptr;
            return nullptr;
        }
    }
    
    ~ClientListLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    void setServer(PMServerHost *srv)
    {
        server = srv;
    }
    
    cocos2d::Map<int,ClientLayer*>& getClientList()
    {
        return clientList;
    }
    
    cocos2d::Vector<ClientGroupLayer*>& getGroups()
    {
        return groupList;
    }
    
    void createGroup();
    void addToGroup();
    void deleteFromGroup();
    void deleteGroups();
    
    void addClient(PMClient *client);
    void removeClient(PMClient *client);
    
    void addClient(int id, ClientLayer *layer)
    {
        clientList.insert(id,layer);
    }
    
    void removeClient(int id)
    {
        clientList.erase(id);
    }
    
    void addGroup(ClientGroupLayer *layer)
    {
        groupList.pushBack(layer);
    }
    void removeGroup(int index)
    {
        cocos2d::Vector<ClientGroupLayer *>::iterator i = groupList.begin() + index;
        groupList.erase(i);
    }
    
    void drawClients();
private:
    void addClients(const cocos2d::Map<int,ClientLayer*> &clients, bool refreshUI = false)
    {
        for(auto &i : clients)
            clientList.insert(i.first, i.second);
        
        if(refreshUI)
            drawClients();
    }
    
    
    void refreshClientList(float dt = 0);
    void loadClientList();
    
    PMServerHost *server;
    
    cocos2d::Map<int,ClientLayer*> clientList;
    
    cocos2d::Vector<ClientGroupLayer *> groupList;
    //std::vector<ClientLayer *>clientList;
    void removeEmptyGroups();    
};

#endif /* defined(__pictomir_cocos2d_x__ClientListLayer__) */

//
//  GameLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//
#include "GameLayer.h"
#include "ProgramLayer.h"
#include "SimpleAudioEngine.h"
#include "PMManager.h"
#include "PMGameDatabase.h"
#include "GlobalHintLayer.h"
#include "MethodHintLayer.h"
#include "XmlWorldReader.h"
#include "XmlWorldSaver.h"

#include "VariableList.h"

USING_NS_CC;

using namespace CocosDenshion;

Scene* GameLayer::scene(bool continueGame, int world, int level, int robotIndex)
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    GameLayer *layer = GameLayer::create(continueGame, world, level, robotIndex);
    
    // add layer as a child to scene
    scene->addChild(layer,0,0);
    
    // return the scene
    return scene;
}

GameLayer *GameLayer::create(bool continueGame, int world,int level, int robotIndex)
{
    GameLayer *pRet = new GameLayer();
    
    if(pRet && pRet->initWithWorldAndLevel(continueGame, world, level, robotIndex)) {
        pRet->autorelease();
        return pRet;
    } else {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

// on "init" you need to initialize your instance
bool GameLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

    
    PMSettings::instance()->setGameLayer(this);
    
    return true;
}

bool GameLayer::initWithWorldAndLevel(bool bContinueGame,int world,int level, int robotIndex)
{
    if ( !init() )
    {
        return false;
    }
    
    netRobotIndex = robotIndex;
    
    gameLayerInit();
    if(world == -1 || level == -1)
    {
        if(bContinueGame)
            continueGame();
        else
            loadWorld(PMSettings::instance()->getWorldPart(), PMSettings::instance()->getLevel());
    }
    else
    {
        loadWorld(world, level);
    }
    return true;
}

void GameLayer::setStartGameStates()
{
    programLayer->setEnabled(false);
    
    if(methodStackLayer != nullptr)
        methodStackLayer->setEnabled(false);
    
    if(changeLevelMenu)
        changeLevelMenu->setEnabled(false);
    
    programControlLayer->getStartProgramButton()->setEnabled(false);
}

void GameLayer::setEndGameStates()
{
    programControlLayer->getStartProgramButton()->setEnabled(false);
    programControlLayer->getMakeStepButton()->setEnabled(false);
    
    if(changeLevelMenu)
        changeLevelMenu->setEnabled(true);
}

void GameLayer::setEnabledForNetwork(bool state)
{
    //programControlLayer->setEnabled(false);
    pauseButton->setEnabled(state);
    methodStackLayer->setEnabled(state);
}

void GameLayer::setEnabled(bool state)
{
    programLayer->setEnabled(state);
    speedBarLayer->setEnabled(state);
    world->getMap()->setEnabled(state);
    
    if(methodStackLayer != nullptr)
        methodStackLayer->setEnabled(state);
    
    programControlLayer->setEnabled(state);
    
    if(changeLevelMenu)
        changeLevelMenu->setEnabled(state);
}

void GameLayer::saveStates()
{
    programLayerState = programLayer->isEnabled();
    speedBarState = speedBarLayer->isEnabled();
    mapState = world->getMap()->isEnabled();
    
    if(methodStackLayer)
        methodStackLayer->saveStates();
    
    programControlLayer->saveStates();
    
    if(changeLevelMenu)
        changeLevelMenu->saveStates();
}

void GameLayer::restoreStates()
{
    programLayer->setEnabled(programLayerState);
    speedBarLayer->setEnabled(speedBarState);
    world->getMap()->setEnabled(mapState);

    if(methodStackLayer)methodStackLayer->restoreStates();
    programControlLayer->restoreStates();
    
    if(changeLevelMenu)
        changeLevelMenu->restoreStates();
}

void GameLayer::continueLevel(cocos2d::Ref *object)
{
    if(pauseLayer != nullptr)
    {
        if(pauseButton)pauseButton->setEnabled(true);
        pauseLayer->hide();
        pauseLayer = nullptr;
    }
    
    restoreStates();
    
    if(world->getBaseRobotManager()->getState() == RobotManager::Working)
        programControlLayer->getStartProgramButton()->setEnabled(false);
    
    resume();
}

void GameLayer::restartLevel(cocos2d::Ref *object)
{
    setEnabled(true);
    
    programLayer->reset();
    
    programStarted = false;
    
    world->getMap()->clean();
    world->getMap()->update();
    
    
    for (int i = 0; i < world->robotCount(); ++i)
    {
        world->robotManagerAt(i)->clear();
        world->robotAt(i)->cleanTempMethods();
        world->robotAt(i)->clearAllStateFlags();
        //world->robotAt(i)->setStateFlag(AbstractRobot::Ready);
        world->robotAt(i)->setStepCount(0);
        world->robotManagerAt(i)->setState(RobotManager::Paused);
    }
    
    clearHighlitedButton();
    
    unscheduleAllSelectors();
    
    
    if(methodStackLayer)
        methodStackLayer->clear();
    
    if(winLayer != nullptr)
    {
        winLayer->hide();
        winLayer = nullptr;
    }
    
    if(pauseLayer != nullptr)
    {
        pauseLayer->hide();
        pauseLayer = nullptr;
    }
    
    if(looseLayer != nullptr)
    {
        looseLayer->hide();
        looseLayer = nullptr;
    }
    
    highlightedLayer = -1;

    schedule(schedule_selector(GameLayer::makeStep), 0.01f);
}


void GameLayer::continueGame()
{
    worldPart = PMSettings::instance()->getWorldPart();
    level = PMSettings::instance()->getLevel();
    
    loadWorld(writablePath + "save.map");
}


void GameLayer::saveMap()
{
    XMLWorldSaver xml(world, this ,writablePath + "save.map");
    PMSettings::instance()->setGameStarted(true);
}

void GameLayer::showHint(Ref *sender)
{
    if(getChildByTag(HINT_LAYER_TAG) == nullptr)
    {
        GlobalHintLayer *hintLayer = GlobalHintLayer::create(hint->hintParts[0].hintText, (Node *)sender);
        addChild(hintLayer, 20, HINT_LAYER_TAG);
    }
}

void GameLayer::drawHint()
{
#ifndef MAP_EDITOR
//    if(hint->hintParts.size() == 0)
//        return;
//    
//    GlobalHintLayer *hintLayer = GlobalHintLayer::create(hint->hintParts[0].hintText);
//    addChild(hintLayer,20);
    
    MenuItemSprite *showHintButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("info", NORMAL_STATE),
                                            PMTextureCache::instanse()->getIconSprite("info", SELECTED_STATE),
                                            PMTextureCache::instanse()->getIconSprite("info", DISABLED_STATE),
                                            CC_CALLBACK_1(GameLayer::showHint, this));
    
    showHintButton->setAnchorPoint( Point::ZERO );
    
    Menu *menu = Menu::create(showHintButton, nullptr);
    menu->setAnchorPoint( Point::ZERO );
    menu->setContentSize(showHintButton->getContentSize());
    menu->setPosition( Point(speedBarLayer->getPositionX(),
                             speedBarLayer->getPositionY() - menu->getContentSize().height - 10.0) );
    
    addChild(menu, 10);
#else
    if(hint->hintParts.size() == 0)
    {
        HintPart part;
        part.type = MethodHint;
        part.hintText = "";
        hint->hintParts.push_back(part);
    }
    //hintLayer = MethodHintLayer::create(hint->hintParts[0].hintText, "Подсказка");
    //addChild(hintLayer,20);
#endif
}

void GameLayer::loadWorld(std::string mapPath)
{
    
    XmlWorldReader *xml= new XmlWorldReader;

    world = xml->readWorld(this,mapPath,netRobotIndex);
    
    if(world != nullptr && programLayer != nullptr)
    {
        world->getBaseRobotManager()->setGameLayer(this);
        world->getBaseRobotManager()->setState(RobotManager::Paused);
        schedule(schedule_selector(GameLayer::makeStep), 0.01f);
        
        programLayer->setWorldAndLevel();
        
        //parallax->addChild(world->getMap(), -1, Point(1.0f,1.0f), Point::ZERO);
        addChild( world->getMap(), 0);

        programStarted = false;
        
        setEnabled(true);
        
#ifdef MAP_EDITOR
        if (hint == nullptr) {
            hint = Hint::create();
        }
#endif
        if(hint != nullptr)
            drawHint();
        
#ifndef MAP_EDITOR
        methodStackLayer = MethodStackLayer::create(this, world->getBaseRobotManager());
        addChild(methodStackLayer , 10);
#else
        methodStackLayer = nullptr;
#endif
        
        if(isNetGame())
        {
            saveStates();
            setEnabledForNetwork(false);
            schedule(schedule_selector(GameLayer::checkAllClientConnecion), 0.1f, kRepeatForever, 1.5f * SYSTEM_ANIMATION_DELAY);
        }
        
    }
    else
    {
        char errorString[255];
        
        sprintf(errorString, "Load map error!\n%s",xml->getError().c_str());
        MessageBox(errorString, "Error");
    }
    
    delete xml;
}

void GameLayer::loadWorld(int newWorldPart,int newLevel)
{
    
    setWorldPart(newWorldPart);
    setLevel(newLevel);
    
//    if(client == nullptr)
//    {
//        worldPart = PMSettings::instance()->getWorldPart();
//        level = PMSettings::instance()->getLevel();
//    }
//    else
//    {
//#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
//        //worldPart = client->selected_server_->world_id_;
//        //level = client->selected_server_->map_id_;
//#endif
//    }
    
    if(isNetGame())
        PMSettings::instance()->setLocalGame(false);
    
    std::string mapPathString = PMSettings::instance()->pathToMap(worldPart, level).c_str();
    std::string mapPath = FileUtils::getInstance()->fullPathForFilename(mapPathString);


    loadWorld(mapPath);
#ifndef MAP_EDITOR
    if(world != nullptr && !isNetGame())
        saveMap();
#endif
    
    //CCLOG("%s",Director::getInstance()->getTextureCache()->getCachedTextureInfo().c_str());
}


void GameLayer::gameLayerInit()
{
    setPosition(Point::ZERO);

	Size mapSize = PMSettings::instance()->mapSize();

    speedBarLayer = SpeedBarLayer::create();
    
    addChild(speedBarLayer,2);    
    
    
    speedBarLayer->setPosition( Point(10.0,
                                    mapSize.height - speedBarLayer->getContentSize().height - 20) );
    
    programControlLayer = ProgramControlLayer::create(this, isNetGame());
    
    programControlLayer->setPosition( Point(mapSize.width - programControlLayer->getContentSize().width - 25,
                                          mapSize.height - programControlLayer->getContentSize().height - 10) );
    
    
    addChild(programControlLayer, 10);
    
    if(!isNetGame())
    {
        changeLevelMenu = ChangeLevelMenu::create(this);
        addChild(changeLevelMenu, 10);
    }
#ifndef MAP_EDITOR
    pauseButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("pause", NORMAL_STATE),
                                           PMTextureCache::instanse()->getIconSprite("pause", SELECTED_STATE),
                                           PMTextureCache::instanse()->getIconSprite("pause", DISABLED_STATE),
                                           CC_CALLBACK_1(GameLayer::pause, this));
    
    soundOffButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("soundOff", NORMAL_STATE),
                                           PMTextureCache::instanse()->getIconSprite("soundOff", SELECTED_STATE),
                                           PMTextureCache::instanse()->getIconSprite("soundOff", DISABLED_STATE),
                                           CC_CALLBACK_1(GameLayer::toggleSound, this));
    
    soundOnButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("soundOn", NORMAL_STATE),
                                              PMTextureCache::instanse()->getIconSprite("soundOn", SELECTED_STATE),
                                              PMTextureCache::instanse()->getIconSprite("soundOn", DISABLED_STATE),
                                              CC_CALLBACK_1(GameLayer::toggleSound, this));
    
    pauseButton->setPosition( Point::ZERO );
    soundOffButton->setPosition( Point(pauseButton->getContentSize().width + 10,0) );
    soundOnButton->setPosition( Point(pauseButton->getContentSize().width + 10,0) );
    
    soundOffButton->setVisible(!PMSettings::instance()->getSoundEnabled());
    soundOnButton->setVisible(PMSettings::instance()->getSoundEnabled());
    
    Menu *topMenu = Menu::create(pauseButton,soundOffButton, soundOnButton, nullptr);
    
    topMenu->setContentSize( Size(pauseButton->getContentSize().width, pauseButton->getContentSize().height) );
    topMenu->setAnchorPoint( Point::ZERO );
    topMenu->setPosition( Point(mapSize.width / 2, mapSize.height - topMenu->getContentSize().height / 2 - 10) );

    addChild(topMenu, 10);
#endif
}

void GameLayer::checkAllClientConnecion(float t)
{
#ifndef MAP_EDITOR
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
//    if(client->ready())
//    {
//        setEnabledForNetwork(true);
//        restoreStates();
//    }
#endif
#endif
}

void GameLayer::sendSolution()
{
    _pt_robot_instr_list instrList;
    
    instrList.robotIndex = world->getBaseRobotIndex();
    instrList.programNum = programLayer->layers.size();
    
    for(int i= 0; i < instrList.programNum; ++i)
    {
        instrList.programs[i].ID = programLayer->layers[i]->getMethodID();
        instrList.programs[i].useRepeater = programLayer->layers[i]->getUseRepeater();
        instrList.programs[i].useCondition = programLayer->layers[i]->getUseCondition();
        instrList.programs[i].repeater = programLayer->layers[i]->getRepeater();
        instrList.programs[i].condition = programLayer->layers[i]->getCondition();
        
        instrList.programs[i].methodNum = programLayer->layers[i]->getMethods().size();
        
        for(int j = 0; j < instrList.programs[i].methodNum; ++j)
        {
            instrList.programs[i].methods[j] = programLayer->layers[i]->getMethods()[j].methodID;
            instrList.programs[i].execType[j] = programLayer->layers[i]->getMethods()[j].executeType;
        }
    }
    
    PMManager::instance()->sendSolution(&instrList);
}

void GameLayer::getSolutions()
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
//    for(set<char *>::iterator i = client->solutions_.begin() ; i != client->solutions_.end(); ++i)
//    {
//        pt_robot_instr_list *instrList = (pt_robot_instr_list *)(*i);
//    
//        world->robotAt(instrList->robotIndex)->setMethodListFromNet(instrList);
//    }
#endif
}

void GameLayer::loadRobotMethods()
{
    if(!programStarted)
    {
        world->getBaseRobot()->cleanTempMethods();
        
        for(int i = 0; i < programLayer->layers.size(); ++i)
        {
            //std::pair<int,RobotMethod> execMethod(programLayer->layers[i]->getMethodID_Exec(), RobotMethod(world->getBaseRobot(), programLayer->layers[i]->makeExecMethod(),  RobotMethod::TempMethod));
            std::pair<int,RobotMethod> method(programLayer->layers[i]->getMethodID(), RobotMethod(world->getBaseRobot(), programLayer->layers[i]->makeMethod(),  RobotMethod::TempMethod));
            
            world->getBaseRobot()->methodList.insert(method);
            //world->getBaseRobot()->methodList.insert(execMethod);
        }
        
    }
}

void GameLayer::start(cocos2d::Ref *object)
{
    if(!programStarted)
    {
        VariableList::instanse()->clearNotReservedList();
        
        loadRobotMethods();
        
        if(isNetGame())
        {
            sendSolution();
        }

        startGame();
    }
    else
    {
        for(int j = 0 ; j < world->robotCount(); ++j)
        {
            world->robotManagerAt(j)->setState(RobotManager::Working);
        }
    }
}

void GameLayer::startNetGame(cocos2d::Ref *object)
{
    restartLevel(nullptr);
    VariableList::instanse()->clearNotReservedList();
    
    loadRobotMethods();
    
    sendSolution();
    PMManager::instance()->sendReadyToStart();
}

void GameLayer::startGame()
{
    for(int j = 0 ; j < world->robotCount(); ++j)
    {
        if(!world->robotAt(j)->getMethodList().empty() && !programStarted)
            world->robotManagerAt(j)->linkProgram();
        
        world->robotManagerAt(j)->setState(RobotManager::Working);        
    }
    
    if(!programStarted)
        programStarted = true;
    
    setStartGameStates();
}

void GameLayer::makeProgramStep(cocos2d::Ref *object)
{
    if(!programStarted)
        start(nullptr);
    
    programControlLayer->getStartProgramButton()->setEnabled(true);
    
    for(int j = 0 ; j < world->robotCount(); ++j)
    {
        world->robotManagerAt(j)->setState(RobotManager::Controlled);
    }
}

void GameLayer::makeStep(float s)
{
    for (int i = 0; i < world->robotCount(); ++i)
    {
        world->robotManagerAt(i)->loop();
    }
}

void GameLayer::winLayerInit()
{
    winLayer = RobotWinLayer::create(this,world->getBaseRobot()->getStepCount());
    addChild(winLayer, 20);
    winLayer->show();
}

void GameLayer::looseLayerInit()
{
    looseLayer = RobotLooseLayer::create(this);
    addChild(looseLayer, 20);
    looseLayer->show();
}

void GameLayer::pauseLayerInit(bool isRobotLoose)
{
    pauseLayer = PauseLayer::create(this, isRobotLoose);
    addChild(pauseLayer, 20);
    
    if(pauseButton)
        pauseButton->setEnabled(false);
    
    pauseLayer->show();
}

void GameLayer::robotWin()
{
    setEndGameStates();
    
    unscheduleAllSelectors();
    
    winLayerInit();

    if(PMSettings::instance()->getSoundEnabled())
        SimpleAudioEngine::getInstance()->playEffect("Sounds/win_sound.wav");
    
    if(!isNetGame())
        PMGameDatabase::instance()->setGameScore(worldPart, level, world->getBaseRobot()->getStepCount());
}

void GameLayer::robotLoose()
{
    setEndGameStates();
    
    unscheduleAllSelectors();
    
    highLightBreakButon();
    
    looseLayerInit();
    
    if(PMSettings::instance()->getSoundEnabled())
        SimpleAudioEngine::getInstance()->playEffect("Sounds/loose_sound.wav");
}

void GameLayer::robotFailure()
{
    setEndGameStates();
    
    unscheduleAllSelectors();
}

void GameLayer::pause(cocos2d::Ref *object)
{
    saveStates();
    
    setEnabled(false);
    
    if(pauseButton)
        pauseButton->setEnabled(false);
   
    CCLayer::pause();
    
    pauseLayerInit(world->getBaseRobot()->isBroken());
}

void GameLayer::toggleSound(cocos2d::Ref *object)
{
    PMSettings::instance()->setSoundEnabled(!PMSettings::instance()->getSoundEnabled());
    
    soundOffButton->setVisible(!PMSettings::instance()->getSoundEnabled());
    soundOnButton->setVisible(PMSettings::instance()->getSoundEnabled());
    
    if(!PMSettings::instance()->getSoundEnabled())
        SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    else
        SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}

void GameLayer::highLightBreakButon()
{
    if(highlightedButton != -1 && highlightedLayer != -1)
    {
        MethodLayer *layer = programLayer->layers[highlightedLayer];
        layer->highlightMethodBreak(highlightedButton);
    }
}

void GameLayer::highLightButon(int layerIndex,int button)
{
    if(highlightedLayer != -1)
        clearHighlitedButton();
    
    highlightedLayer = layerIndex;
    highlightedButton = button;
    
    MethodLayer *layer = programLayer->layers[highlightedLayer];
    layer->highlightMethod(highlightedButton);
}


void GameLayer::clearHighlitedButton()
{
    if(highlightedLayer == -1)
        return;
    
    MethodLayer *layer = programLayer->layers[highlightedLayer];
    layer->clearHighlightMethod(highlightedButton);
}

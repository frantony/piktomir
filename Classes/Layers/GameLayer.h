//
//  GameLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__GameLayer__
#define __pictomir_cocos2d_x__GameLayer__

#include <iostream>
#include "cocos2d.h"
#include "PMSettings.h"
#include "World4.h"
#include "ProgramLayer.h"
#include "Hint.h"
#include "PMTextureCache.h"
#include "PMClient.h"

#include "RobotLooseLayer.h"
#include "RobotWinLayer.h"
#include "PauseLayer.h"
#include "SpeedBarLayer.h"
#include "ProgramControlLayer.h"
#include "ChangeLevelMenu.h"
#include "MethodStackLayer.h"

#include "GlobalHintLayer.h"

class GameLayer:public cocos2d::Layer
{
    SYNTHESIZE(ProgramLayer *, programLayer, ProgramLayer);
    SYNTHESIZE(SpeedBarLayer *, speedBarLayer, SpeedBarLayer);
    SYNTHESIZE(ProgramControlLayer *, programControlLayer, ProgramControlLayer);
    SYNTHESIZE(ChangeLevelMenu *, changeLevelMenu, ChangeLevelMenu);
    
    SYNTHESIZE(RobotWinLayer *, winLayer, WinLayer);
    SYNTHESIZE(RobotLooseLayer *, looseLayer, LooseLayer);
    SYNTHESIZE(PauseLayer *, pauseLayer, PauseLayer);
    SYNTHESIZE(MethodStackLayer *, methodStackLayer, MethodStackLayer);
    
    
    SYNTHESIZE(Hint*, hint, Hint);
    
    SYNTHESIZE(int,worldPart,WorldPart);
    SYNTHESIZE(int,level,Level);
    SYNTHESIZE(AbstractWorld *,world,World);
public:
    virtual bool init();
    bool initWithWorldAndLevel(bool continueGame, int world,int level,int robotIndex);
    
    static cocos2d::Scene* scene(bool continueGame = false, int world = -1,int level = -1,int robotIndex = -1);
    
    CREATE_FUNC(GameLayer);
    
    static GameLayer *create(bool continueGame, int world,int level,int robotIndex);
    
    GameLayer():
        cocos2d::Layer(),
        programLayer(nullptr),
        speedBarLayer(nullptr),
        programControlLayer(nullptr),
        changeLevelMenu(nullptr),
        winLayer(nullptr),
        looseLayer(nullptr),
        pauseLayer(nullptr),
        methodStackLayer(nullptr),
        hint(nullptr),
        world(nullptr),
        highlightedLayer(-1),
        highlightedButton(-1),
        programStarted(false),
        pauseButton(nullptr),
        netRobotIndex(-1)
    {
        cocos2d::Director::getInstance()->getTextureCache()->removeAllTextures();
    }
    
//    GameLayer(PMClient *client):
//        cocos2d::Layer(),
//        programLayer(nullptr),
//        speedBarLayer(nullptr),
//        programControlLayer(nullptr),
//        changeLevelMenu(nullptr),
//        winLayer(nullptr),
//        looseLayer(nullptr),
//        pauseLayer(nullptr),
//        methodStackLayer(nullptr),
//        hint(nullptr),
//        world(nullptr),
//        highlightedLayer(-1),
//        highlightedButton(-1),
//        programStarted(false),
//        pauseButton(nullptr),
//        client(client)
//    {
//        cocos2d::Director::getInstance()->getTextureCache()->removeAllTextures();
//    }
    
    ~GameLayer()
    {
        CC_SAFE_DELETE(hint);
        CC_SAFE_DELETE(world);
        
        removeAllChildrenWithCleanup(true);
        
        VariableList::instanse()->clearAllList();
    
        cocos2d::Director::getInstance()->getTextureCache()->removeUnusedTextures();
        
        //cocos2d::SpriteFrameCache::getInstance()->removeUnusedSpriteFrames();
    }
    
    void saveMap();
       
protected:
    
    int highlightedLayer;
    int highlightedButton;
    
    bool programStarted;
    
    cocos2d::MenuItemSprite *pauseButton;
    
    int netRobotIndex;
    
    cocos2d::MenuItemSprite *soundOffButton;
    cocos2d::MenuItemSprite *soundOnButton;
    
    bool programLayerState;
    bool speedBarState;
    bool mapState;    
    
    void gameLayerInit();
    void loadWorld(int world,int level);
    void loadWorld(std::string mapPath);
    //Handlers
    
    void makeStep(float s);
    void checkAllClientConnecion(float t);
    
    void winLayerInit();
    void looseLayerInit();
    void pauseLayerInit(bool isRobotLoose);
    
    void drawHint();
    
    void saveStates();
    void restoreStates();
    
    void sendSolution();
    void getSolutions();
    
    bool isNetGame()
    {
        return netRobotIndex != -1;
    }
    
    void loadRobotMethods();
    
    void showHint(cocos2d::Ref *sender);
    
public:    //methods
    void continueGame();
    void continueLevel(cocos2d::Ref *object);
    void restartLevel(cocos2d::Ref *object);
    
    void start(cocos2d::Ref *object);
    void startNetGame(cocos2d::Ref *object);
    void pause(cocos2d::Ref *object);
    void toggleSound(cocos2d::Ref *object);
    void makeProgramStep(cocos2d::Ref *object);
    
    void robotWin();
    void robotLoose();
    void robotFailure();
    
    void setEnabled(bool state);
    void setStartGameStates();
    void setEndGameStates();
    void setEnabledForNetwork(bool state);
    
    void highLightBreakButon();
    void highLightButon(int layerIndex,int button);
    void clearHighlitedButton();
    
#ifdef MAP_EDITOR
    MethodHintLayer *hintLayer;
    void readyForSave () {
        std::string text = "";//hintLayer->hint->getText();
        if(text != "")
        {
            hint->hintParts[0].type = MethodHint;
            hint->hintParts[0].hintText = text;
        } else hint = nullptr;
    }
#endif
    
    void startGame();
};

#endif /* defined(__pictomir_cocos2d_x__GameLayer__) */

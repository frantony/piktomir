//
//  HintLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/10/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__GlobalHintLayer__
#define __pictomir_cocos2d_x__GlobalHintLayer__

#include <iostream>
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

const float GLOBAL_HINT_BORDER = 10.0f;
const float GLOBAL_HINT_SELFREMOVE_DELAY = 4.0f;


class GlobalHintLayer : public cocos2d::LayerColor
{
public:
    virtual bool initWithText(std::string text, cocos2d::Node* callButton);
    
    GlobalHintLayer(std::string text):
        cocos2d::LayerColor()
    {}
    
    static GlobalHintLayer *create(std::string text, cocos2d::Node* callButton);
    
    ~GlobalHintLayer();
private:
    cocos2d::LayerColor *layerColor;
    
    cocos2d::EventListenerTouchOneByOne *listener;
    
    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    
    void startSelfRemove();
    void selfRemove();
};
#ifdef MAP_EDITOR
class HintEditLayer : public cocos2d::LayerColor
{
public:
    EditBox* hint;
    
    virtual bool init();
    
    HintEditLayer(std::string text, std::string placeHolder):
    cocos2d::LayerColor(),
    text(text),
    placeHolder(placeHolder)
    {}
    
    static HintEditLayer *create(std::string text,std::string placeHolder);
    static int hintCount;
    
    ~HintEditLayer();
private:
    std::string text;
    std::string placeHolder;
};
#endif

#endif /* defined(__pictomir_cocos2d_x__GlobalHintLayer__) */

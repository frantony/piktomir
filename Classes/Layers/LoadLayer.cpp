//
//  LoadLayer.cpp
//  Piktomir
//
//  Created by [scaR] on 04.03.14.
//
//

#include "LoadLayer.h"
#include "FTPuser.h"
#include "PMSettings.h"
#include "utils.h"

#include <thread>

USING_NS_CC;
using namespace std;

Scene* LoadLayer::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    LoadLayer *layer = LoadLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer,0,0);
    
    // return the scene
    return scene;
}


bool LoadLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    LayerColor *color = LayerColor::create(Color4B(255,255,255,255), PMSettings::instance()->getScreenSize().width, PMSettings::instance()->getScreenSize().height);
    color->setAnchorPoint( Point::ZERO );
    color->setPosition( Point::ZERO );
    addChild(color,-1);
    
    
    Sprite *backGround  = Sprite::create(PICT_PATH "Background/LoadBackground.png");
    backGround->setPosition( Point(PMSettings::instance()->getScreenSize().width/2, PMSettings::instance()->getScreenSize().height/2) );
    //resizeSprite(backGround, PMSettings::instance()->getScreenSize().width, PMSettings::instance()->getScreenSize().height);
    addChild(backGround, 0);
    
//    Label *label = Label::create("Загрузка", "Arial", 20);
//    label->setPosition(Point(PMSettings::instance()->getScreenSize().width / 2, PMSettings::instance()->getScreenSize().height / 2));
//    addChild(label);
    
    SpriteFrameCache *sfCache = SpriteFrameCache::getInstance();
    sfCache->addSpriteFramesWithFile(PICT_PATH "System/load-sprites.plist");
    
    Sprite *loadSprite = Sprite::createWithSpriteFrame(sfCache->getSpriteFrameByName("load-1.png"));
    loadSprite->setAnchorPoint( Point(0.5, 0) );
    
    loadSprite->setPosition( Point(PMSettings::instance()->getScreenSize().width/2, 50) );
    
    addChild(loadSprite,1);
    
    AnimationCache *cache = AnimationCache::getInstance();
    cache->addAnimationsWithFile(PICT_PATH "System/load-animation.plist");
    Animation *animation = cache->getAnimation("start");
    animation->setDelayPerUnit(SYSTEM_ANIMATION_DELAY );
    Animate *animate = Animate::create(animation);
    
    FiniteTimeAction *repeat = RepeatForever::create(animate);
    
    loadSprite->runAction(repeat);
    
#ifndef MAP_EDITOR
    thread = new FTPThread(bind(&LoadLayer::endDownLoadMaps, this));
    thread->start();
#endif
    //_thread->detach();
    
    return true;
}

void LoadLayer::endDownLoadMaps()
{
    CCLOG("Ftp download result:%s", thread->getResultString().c_str());
    
    delete thread;
    
    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, MainMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}
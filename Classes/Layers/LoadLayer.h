//
//  LoadLayer.h
//  Piktomir
//
//  Created by [scaR] on 04.03.14.
//
//

#ifndef __Piktomir__LoadLayer__
#define __Piktomir__LoadLayer__

#include <iostream>
#include <thread>

#include "cocos2d.h"
#include "MainMenuLayer.h"
#include "FTPThread.h"

class LoadLayer : public cocos2d::Layer
{
public:
    virtual bool init();
    
    CREATE_FUNC(LoadLayer);
    
    static cocos2d::Scene* scene();
    
    ~LoadLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    FTPThread *thread;
    
private:
    void endDownLoadMaps();
};

#endif /* defined(__Piktomir__LoadLayer__) */

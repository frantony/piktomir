//
//  MenuLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "MainMenuLayer.h"
#include "SelectMenuLayer.h"
#include "SettingsMenuLayer.h"
#include "GameLayer.h"
#include "TeacherLayer.h"
#include "utils.h"
#include "PMManager.h"
#include "SelectLocalNetGames.h"

#include "LocalizedString.h"

USING_NS_CC;

Scene* MainMenuLayer::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    MainMenuLayer *layer = MainMenuLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer,0,0);
    
    // return the scene
    return scene;
}


bool MainMenuLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    PMSettings::instance()->setLocalGame(true);
    
    setPosition( Point(0,0) );

    Sprite *backGround  = Sprite::create(PICT_PATH "Background/background1.png");
    backGround->setPosition( Point(PMSettings::instance()->getScreenSize().width/2, PMSettings::instance()->getScreenSize().height/2) );
    resizeSprite(backGround, PMSettings::instance()->getScreenSize().width, PMSettings::instance()->getScreenSize().height);
    addChild(backGround, -1);
    
#ifndef MAP_EDITOR
    bool continue_ = PMSettings::instance()->getGameStarted();
    
    MenuItemFont *item0 = nullptr;
    
    if (continue_)
    {
        item0 = MenuItemFont::create(LocalizedString("ContinueGame"),
                                     CC_CALLBACK_1(MainMenuLayer::continueGame, this));
        item0->setPosition( Point(PMSettings::instance()->getScreenSize().width / 2, PMSettings::instance()->getScreenSize().height / 2 + 100) );
    }
    
    MenuItemFont *item1 = MenuItemFont::create(LocalizedString("NewGame"),
                                               CC_CALLBACK_1(MainMenuLayer::newGame, this));
    MenuItemFont *item2 = MenuItemFont::create(LocalizedString("SelectLevel"),
                                               CC_CALLBACK_1(MainMenuLayer::selectLevel, this));
    MenuItemFont *item3 = MenuItemFont::create(LocalizedString("Settings"),
                                               CC_CALLBACK_1(MainMenuLayer::settings, this));
    MenuItemFont *item4 = nullptr;
    
#if(CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 )
    item4 = MenuItemFont::create(LocalizedString("Exit"),
                                 CC_CALLBACK_1(MainMenuLayer::exitGame, this));
    item4->setPosition( Point(PMSettings::instance()->getScreenSize().width / 2, PMSettings::instance()->getScreenSize().height / 2 - 100) );
#endif
    
    item1->setPosition( Point(PMSettings::instance()->getScreenSize().width / 2, PMSettings::instance()->getScreenSize().height / 2 + 50) );
    item2->setPosition( Point(PMSettings::instance()->getScreenSize().width / 2, PMSettings::instance()->getScreenSize().height / 2 ) );
    item3->setPosition( Point(PMSettings::instance()->getScreenSize().width / 2, PMSettings::instance()->getScreenSize().height / 2 - 50) );
    
    Menu *menu = Menu::create(item1, item2, item3, item4, nullptr);
    
    if(item0 != nullptr)
        menu->addChild(item0);
#else
    MenuItemFont *item1 = MenuItemFont::create(LocalizedString("EditLocalCopy"),
                                               CC_CALLBACK_1(MainMenuLayer::openLocal, this));
    MenuItemFont *item2 = MenuItemFont::create(LocalizedString("DownloadFromFtp"),
                                               CC_CALLBACK_1(MainMenuLayer::downloadFromFTP, this));
    MenuItemFont *item3 = MenuItemFont::create(LocalizedString("Options"),
                                               CC_CALLBACK_1(MainMenuLayer::showOptionsLayer, this));
    MenuItemFont *item4 = nullptr;
    
#if(CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 )
    item4 = MenuItemFont::create(LocalizedString("Exit"), CC_CALLBACK_1(MainMenuLayer::exitGame, this));
    item4->setPosition( Point(PMSettings::instance()->getScreenSize().width / 2, PMSettings::instance()->getScreenSize().height / 2 - 100) );
#endif
    
    item1->setPosition( Point(PMSettings::instance()->getScreenSize().width / 2, PMSettings::instance()->getScreenSize().height / 2 + 50) );
    item2->setPosition( Point(PMSettings::instance()->getScreenSize().width / 2, PMSettings::instance()->getScreenSize().height / 2) );
    item3->setPosition( Point(PMSettings::instance()->getScreenSize().width / 2, PMSettings::instance()->getScreenSize().height / 2 - 50) );
    
    
    Menu *menu = Menu::create(item1, item2, item3, item4, nullptr);
#endif
    
    menu->setPosition(Point::ZERO);
    
    addChild(menu, 1);
    
#ifndef MAP_EDITOR
    MenuItemFont *teacherLayerButton = MenuItemFont::create(LocalizedString("TeacherMode"),
                                                            CC_CALLBACK_1(MainMenuLayer::teacherMode, this));
    teacherLayerButton->setAnchorPoint( Point::ZERO );
    teacherLayerButton->setPosition( Point::ZERO );
    
    Menu *teacherMenu = Menu::create(teacherLayerButton, nullptr);
    teacherMenu->setPosition( Point(getContentSize().width - teacherLayerButton->getContentSize().width - 20.0,
                                        teacherLayerButton->getContentSize().height + 20.0) );
    
    
    addChild(teacherMenu, 1);

    MenuItemFont *downloadMaps = MenuItemFont::create(LocalizedString("DownloadMaps"),
                                                            CC_CALLBACK_1(MainMenuLayer::downloadMaps, this));
    downloadMaps->setAnchorPoint( Point::ZERO );
    downloadMaps->setPosition( Point::ZERO );
    
    Menu *downloadMenu = Menu::create(downloadMaps, nullptr);
    downloadMenu->setPosition( Point(20.0, teacherLayerButton->getContentSize().height + 20.0) );
    
    
    addChild(downloadMenu, 1);
#endif
    
    return true; 
}


void MainMenuLayer::newGame(cocos2d::Ref *sender)
{
    PMSettings::instance()->loadWorlds();
    PMSettings::instance()->setWorldPart(0);
    PMSettings::instance()->setLevel(1);
    PMSettings::instance()->setGameStarted(false);

    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, GameLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void MainMenuLayer::teacherMode(cocos2d::Ref *object)
{
    PMSettings::instance()->loadWorlds();
    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, TeacherLayer::scene());
    Director::getInstance()->replaceScene(trans);

}

void MainMenuLayer::downloadMaps(cocos2d::Ref *object)
{
    thread = new FTPThread(std::bind(&MainMenuLayer::endDownLoadMaps, this));
    thread->start();
}

void MainMenuLayer::endDownLoadMaps()
{
    MessageBox(thread->getResultString().c_str(), "Ftp result");
    
    delete thread;
}

void MainMenuLayer::continueGame(cocos2d::Ref *sender)
{
    PMSettings::instance()->loadWorlds();
    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, GameLayer::scene(true));
    Director::getInstance()->replaceScene(trans);
}

void MainMenuLayer::selectLevel(cocos2d::Ref *sender)
{
    PMSettings::instance()->loadWorlds();
    TransitionSlideInR* trans = TransitionSlideInR::create( 1.5f * SYSTEM_ANIMATION_DELAY, SelectMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void MainMenuLayer::settings(cocos2d::Ref *sender)
{
    TransitionSlideInR* trans = TransitionSlideInR::create( 1.5f * SYSTEM_ANIMATION_DELAY, SettingsMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void MainMenuLayer::exitGame(cocos2d::Ref *sender)
{
    removeAllChildrenWithCleanup(true);
    exit(0);
}

#ifdef MAP_EDITOR
void MainMenuLayer::showOptionsLayer(cocos2d::Ref *sender)
{
    TransitionSlideInR* trans = TransitionSlideInR::create( 1.5f * SYSTEM_ANIMATION_DELAY, OptionsLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void MainMenuLayer::downloadFromFTP(cocos2d::Ref *sender) {
    FTPuser * tmpFTPUser = new FTPuser(writablePath, cocos2d::UserDefault::getInstance()->getStringForKey("Login"), cocos2d::UserDefault::getInstance()->getStringForKey("Pass"));
    if(tmpFTPUser->checkFTP()) {
        if(MapEditorGameLayer::getInstanse()->FTPUser) delete MapEditorGameLayer::getInstanse()->FTPUser;
        MapEditorGameLayer::getInstanse()->FTPUser = tmpFTPUser;
        MapEditorGameLayer::getInstanse()->FTPLogin = cocos2d::UserDefault::getInstance()->getStringForKey("Login");
        MapEditorGameLayer::getInstanse()->FTPPass = cocos2d::UserDefault::getInstance()->getStringForKey("Pass");
        
    }
    else
    {
        MessageBox (LocalizedString("WrongLoginPassword").c_str(), LocalizedString("WrongLoginPassword").c_str());
    }

    
    if (MapEditorGameLayer::getInstanse()->FTPUser)
    {
        if (MapEditorGameLayer::getInstanse()->FTPUser->downloadMap())
        {
            
            system(("rm -rf "+writablePath + "Maps/").c_str());
            
            
            rmdir((writablePath + "Maps/").c_str());
            printf("%s",(writablePath + "Maps").c_str());
            platform_mkdir((writablePath + "Maps").c_str());
            if(unzipFile((writablePath + "Maps.zip").c_str(),writablePath + "Maps/" ) == 0)
            {
                openLocal (nullptr);
            }
            else
            {
                MessageBox (LocalizedString("ErrorWhileUnpacking").c_str(), LocalizedString("ErrorWhileUnpacking").c_str());
            }
        }
        else
        {
            MessageBox (LocalizedString("ErrorWhileDownloading").c_str(), LocalizedString("CheckInternetConnection").c_str());
        }
    }
    else
    {
        MessageBox (LocalizedString("EnterLoginPassword").c_str(), LocalizedString("EnterLoginPassword").c_str());
        showOptionsLayer(nullptr);
    }
}

void MainMenuLayer::openLocal (cocos2d::Ref *sender)
{
    
    TransitionSlideInR* trans = TransitionSlideInR::create( 1.5f * SYSTEM_ANIMATION_DELAY, SelectLocalNetGames::scene());
        Director::getInstance()->replaceScene(trans);
}
#endif


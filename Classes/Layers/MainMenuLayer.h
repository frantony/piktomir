//
//  MenuLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__MenuLayer__
#define __pictomir_cocos2d_x__MenuLayer__

#include <iostream>
#include "cocos2d.h"
#include "FTPThread.h"

#ifdef MAP_EDITOR
#include "mapEditorsMenu.h"
#include "OptionsLayer.h"
#include "MapEditorGameLayer.h"
#endif

USING_NS_CC;

class MainMenuLayer : public cocos2d::Layer
{
public:
    virtual bool init();
    
    CREATE_FUNC(MainMenuLayer);
    
    static cocos2d::Scene* scene();
    
    ~MainMenuLayer()
    {
        removeAllChildrenWithCleanup(true);
        cocos2d::Director::getInstance()->getTextureCache()->removeUnusedTextures();
    }
    
private:
    
    FTPThread *thread;
    
    void newGame(cocos2d::Ref *sender);
    void continueGame(cocos2d::Ref *sender);
    void selectLevel(cocos2d::Ref *sender);
    void settings(cocos2d::Ref *sender);
    void exitGame(cocos2d::Ref *sender);
    void teacherMode(cocos2d::Ref *object);
    void downloadMaps(cocos2d::Ref *object);
    void endDownLoadMaps();
    
#ifdef MAP_EDITOR
    void showOptionsLayer (cocos2d::Ref *sender);
    void downloadFromFTP (cocos2d::Ref * sender);
    void openLocal (cocos2d::Ref *sender);
#endif
};


#endif /* defined(__pictomir_cocos2d_x__MenuLayer__) */

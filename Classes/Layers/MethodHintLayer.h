//
//  MethodHintLayer.h
//  Piktomir
//
//  Created by [scaR] on 20.03.14.
//
//

#ifndef __Piktomir__MethodHintLayer__
#define __Piktomir__MethodHintLayer__

#include <iostream>
#include <string>
#include "cocos2d.h"


const float METHOD_HINT_BORDER = 10.0f;
const float METHOD_HINT_SELFREMOVE_DELAY = 2.0f;

class MethodHintLayer : public cocos2d::Layer
{
public:
    virtual bool initWithText(std::string text, cocos2d::Node *callButton);
    
    MethodHintLayer():
        cocos2d::Layer()
    {}
    
    ~MethodHintLayer()
    {
        _eventDispatcher->removeEventListener(listener);
        removeAllChildrenWithCleanup(true);
    }
    
    static MethodHintLayer *create(std::string text, cocos2d::Node *callButton);
private:
    cocos2d::EventListenerTouchOneByOne *listener;
    cocos2d::LayerColor *layerColor;
    
    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    
    void startSelfRemove();
    void selfRemove();
};

#endif /* defined(__Piktomir__MethodHintLayer__) */

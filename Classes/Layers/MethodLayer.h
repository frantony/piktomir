//
//  ProgramLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 30.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__MethodLayer__
#define __pictomir_cocos2d_x__MethodLayer__

#include <iostream>
#include <vector>
#include "cocos2d.h"
#include "Robot4.h"
#include "CCGL.h"
#include "Hint.h"
#include "MethodHintLayer.h"
#include "SelectButton.h"
#include "SelectMethodExecuteTypeMenu.h"

class ProgramLayer;

const float TOUCH_CRITICAL_DISTANCE = 10.0;

const int NAME_LABEL_TAG = 0;
const int NAME_SPRITE_TAG = 1;
const int INFO_BUTTON_TAG = 2;
const int REPEATER_BUTTON_TAG = 3;
const int CONDITION_BUTTON_TAG = 4;
const int REPEATER_SPRITE_TAG = 5;
const int CONDITION_SPRITE_TAG = 6;

const int HINT_LAYER_TAG = 100;

const int ML_BACKGROUND_ZORDER = 0;
const int ML_CONTENT_ZORDER = 1;
const int ML_BUTTONS_ZORDER = 2;
const int ML_HINT_ZORDER = 3;


const float BUTTON_TILE_SIZE = 40.0f;
const double METHODLAYER_TILE_OFFSET = 10.0;
const double BORDER_RADIUS = 10.0;

class MethodLayer : public cocos2d::LayerColor
{

    SYNTHESIZE(int, index, Index);
    SYNTHESIZE(ProgramLayer *, parentLayer, ParentLayer);
    
    SYNTHESIZE(int, methodID,MethodID);
    
    SYNTHESIZE(int, height, Height);
    SYNTHESIZE(int, width, Width);
    
    SYNTHESIZE(int, condition,Condition);
    SYNTHESIZE(int, repeater, Repeater);
    SYNTHESIZE(bool, useCondition, UseCondition);
    SYNTHESIZE(bool, useRepeater, UseRepeater);

    SYNTHESIZE(Hint *,hint,Hint);
    
    SYNTHESIZE(std::string, name, Name);
    
    
    SYNTHESIZE(bool, hidden, Hidden);
    SYNTHESIZE(std::string, givenText, GivenText);
    SYNTHESIZE(std::string, doText, DoText);
    
    
    SYNTHESIZE(bool, resizable, Resizable);
    SYNTHESIZE(int, maxHeight, MaxHeight);
    SYNTHESIZE(int, minHeight, MinHeight);
    
    SYNTHESIZE(std::vector<_method>, methods, Methods);
    //SYNTHESIZE(std::vector<int>, backGround, BackGround);
    
private:
    enum TouchType
    {
        mlDragMethod,
        mlDragRepeater,
        mlDragCondition,
        mlResize,
        mlSwitchRepeater,
        mlSwitchCondition,
        mlSwitchLock,
        mlSwitchLockHeight
    };
    
    bool enabled;
    
    float realWidth;
    float realHeight;
    
    cocos2d::Layer *backGroundLayer;
    cocos2d::Layer *methodLayer;
    
    float nameLength;
#ifdef MAP_EDITOR
    //HintEditLayer * hintLayer;
#endif
    
    cocos2d::MenuItemSprite *pushProgramButton;
    //for drag'n'drop
    cocos2d::Sprite *draggedSprite;
    int draggedMethod;
    
    TouchType touchType;
    
    cocos2d::Point startPoint;
    
    cocos2d::MenuItemSprite *showHintButton;
    
    value_tag checkConditionTag;
    
public:
    
    MethodLayer():
        cocos2d::LayerColor(),
        index(-1),
        parentLayer(nullptr),
        methodID(EMPTY_METHOD),
        height(0),
        width(0),
        condition(EMPTY_CONDITION),
        repeater(1),
        useCondition(false),
        useRepeater(false),
        hint(nullptr),
        enabled(true),
        methods(std::vector<_method> ()),
        //backGround(std::vector<int> ()),
        resizable(false),
        maxHeight(0),
        minHeight(0),
        realWidth(0),
        realHeight(0),
        backGroundLayer(nullptr),
        methodLayer(nullptr),
        name(std::string()),
        nameLength(0.0f),
        hidden(false),
        givenText(std::string()),
        doText(std::string()),
        pushProgramButton(nullptr),
        draggedSprite(nullptr),
        draggedMethod(-1),
        touchType(mlDragMethod),
        startPoint(cocos2d::Point::ZERO),
        showHintButton(nullptr)
#ifdef MAP_EDITOR
    ,
        deleteMenu(nullptr),
        lockHeight(0)
#endif
    {}
    
    CREATE_FUNC(MethodLayer);
    
    MethodLayer(ProgramLayer *parent):
        cocos2d::LayerColor(),
        index(-1),
        parentLayer(parent),
        methodID(EMPTY_METHOD),
        height(0),
        width(0),
        condition(EMPTY_CONDITION),
        repeater(1),
        useCondition(false),
        useRepeater(false),
        hint(nullptr),
        enabled(true),
        methods(std::vector<_method> ()),
        //backGround(std::vector<int> ()),
        resizable(false),
        maxHeight(0),
        minHeight(0),
        realWidth(0),
        realHeight(0),
        backGroundLayer(nullptr),
        methodLayer(nullptr),
        name(std::string()),
        nameLength(0.0f),
        hidden(false),
        givenText(std::string()),
        doText(std::string()),
        pushProgramButton(nullptr),
        draggedSprite(nullptr),
        draggedMethod(-1),
        touchType(mlDragMethod),
        startPoint(cocos2d::Point::ZERO),
        showHintButton(nullptr)
#ifdef MAP_EDITOR
        ,
        deleteMenu(nullptr),
        lockHeight(0)
#endif
    {}
    
    ~MethodLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    virtual bool init();
    
    friend class XmlWorldReader;
    friend class XMLWorldSaver;
    
    static MethodLayer* create(ProgramLayer *parent)
    {
        MethodLayer *layer = new MethodLayer(parent);
        
        layer->init();
        layer->autorelease();
        
        return layer;
    }
    
    cocos2d::Sprite* resizeSprite;
    
    std::vector<_instruction> makeMethod();
    
    virtual void draw(cocos2d::Renderer* renderer, const kmMat4 &transform, bool transformUpdated);
    
    void setButton(SelectButton *actionButton,cocos2d::Touch *touch);
    
    void highlightMethodBreak(int method);
    void highlightMethod(int method);
    void clearHighlightMethod(int method);
    
    void setControlledMode(bool flag);
    
    void addProgram(cocos2d::Ref *sender);
    
    void drawFunctions();
    
    void setMethod (int type, int index);
    
    bool containsTouch(cocos2d::Touch *touch)
    {
        return backGroundLayer->boundingBox().containsPoint(this->convertTouchToNodeSpace(touch));
    }
    
    bool getEnabled()
    {
        return enabled;
    }
    
    void setEnabled(bool flag, bool button = true)
    {
        enabled = flag;
        
        if(button)
            pushProgramButton->setEnabled(flag);
        
        if(showHintButton != nullptr)
            showHintButton->setEnabled(flag);
        
#ifdef MAP_EDITOR
        if(deleteMenu != nullptr)
        {
            deleteMenu->setEnabled(flag);
            for (int i = 0; i < deleteMenu->getChildren().size(); ++i) {
                ((cocos2d::MenuItem*)(deleteMenu->getChildren().at(i)))->setEnabled(flag);
            }
        }
#endif
    }
    
    int methodAtTouch(cocos2d::Touch *touch);
    int lockAtTouch(cocos2d::Touch *touch);
    bool conditionAtTouch(cocos2d::Touch *touch);
    bool repeaterAtTouch(cocos2d::Touch *touch);
    
    cocos2d::Sprite *cloneMethodSprite(int method);
    cocos2d::Sprite *cloneConditionSprite();
    cocos2d::Sprite *cloneRepeaterSprite();
    
    bool isMethodDraggable(int method)
    {
        return methods[method].methodID != EMPTY_METHOD && methods[method].backgroundType != _method::pmBlocked;
    }
    
    bool isConditionDraggable()
    {
        return condition != EMPTY_CONDITION;
    }
    
    bool isRepeaterDraggable()
    {
        return repeater != 1;
    }
    
    void addAction(int methodIndex,int methodID, bool byProgramLayer = false);
    void addCondition(int newCondition);
    void addRepeater(int repeater);
    
private:    

    cocos2d::CustomCommand lineDrawCommand;
    
    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    
    void resize(int sgn);
    void resizeWidth(int sgn);
    
    void showHint(cocos2d::Ref *sender);
    void drawHint();
    
    //void showSelectExecuteTypeMenu(cocos2d::Ref *sender);
    
    void setNextExecuteType(int method);
    
    void onDraw(const kmMat4 &transform, bool transformUpdated);
    
#ifdef MAP_EDITOR
    int LockAtTouch;
    cocos2d::Menu * deleteMenu;
    cocos2d::Sprite * lockHeightSprite;
    void deleteMethod(cocos2d::Ref* sender);
    void moveUp();
    void makeHint(cocos2d::Ref* sender);
    
    std::vector<int> lastBackground;
    std::vector<int> newHint;
    
    void readyForSave();
    
    int lockHeight;
public:
    void showMethodTypeEditor(int index);
    void showRepeaterTypeEditor();
    void showConditionTypeEditor();
#endif
};

#endif /* defined(__pictomir_cocos2d_x__ProgramLayer__) */

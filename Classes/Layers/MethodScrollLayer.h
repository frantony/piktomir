//
//  MethodScrollLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/19/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__MethodScrollLayer__
#define __pictomir_cocos2d_x__MethodScrollLayer__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "PMSettings.h"

class MethodStackLayer;

class MethodScrollLayer : public cocos2d::extension::ScrollView
{
public:
    
    MethodScrollLayer():
        cocos2d::extension::ScrollView(),
        active(false),
        parent(nullptr),
        dragStarted(false)
    {}
    
    MethodScrollLayer(MethodStackLayer *parent):
        cocos2d::extension::ScrollView(),
        active(false),
        parent(parent),
        dragStarted(false)
    {}
        
    virtual bool init();
    
    CREATE_FUNC(MethodScrollLayer);
    
    static MethodScrollLayer *create(MethodStackLayer *parent);
    
    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    
    ~MethodScrollLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    void setOpacity(GLubyte opacity);
private:
    SYNTHESIZE(bool, active, Active);
    
    MethodStackLayer *parent;
    bool dragStarted;
    cocos2d::Point startPoint;
    bool removed;
    

};

#endif /* defined(__pictomir_cocos2d_x__MethodScrollLayer__) */

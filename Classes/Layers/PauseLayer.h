//
//  PauseLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PauseLayer__
#define __pictomir_cocos2d_x__PauseLayer__

#include <iostream>
#include "cocos2d.h"

class GameLayer;

class PauseLayer : public cocos2d::LayerColor
{
public:
    
    PauseLayer():
        cocos2d::LayerColor(),
        parent(nullptr),
        isRobotLoose(false)
    {}
    
    PauseLayer(GameLayer *parent,bool isRobotLoose):
        cocos2d::LayerColor(),
        parent(parent),
        isRobotLoose(isRobotLoose)
    {}
    
    virtual bool init();
    
    CREATE_FUNC(PauseLayer);
    
    static PauseLayer *create(GameLayer *parent,bool isRobotLoose);
    
    void show();
    void hide();
    
    void selfRemove();
    
    ~PauseLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
private:
    GameLayer *parent;
    bool isRobotLoose;
    
    void mainMenu(cocos2d::Ref *sender);
    void nextLevel(cocos2d::Ref *sender);
    void exitGame(cocos2d::Ref *sender);
};



#endif /* defined(__pictomir_cocos2d_x__PauseLayer__) */

//
//  ProgramControlLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/17/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "ProgramControlLayer.h"
#include "PMSettings.h"
#include "PMTextureCache.h"
#include "GameLayer.h"

#ifdef MAP_EDITOR
#include "MapEditorGameLayer.h"
#endif

USING_NS_CC;

bool ProgramControlLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    
    netGameStartButton = nullptr;
    
    startProgramButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("start", NORMAL_STATE),
                                                  PMTextureCache::instanse()->getIconSprite("start", SELECTED_STATE),
                                                  PMTextureCache::instanse()->getIconSprite("start", DISABLED_STATE),
                                                  CC_CALLBACK_1(GameLayer::start, parent));
    
    stopProgramButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("stop", NORMAL_STATE),
                                                 PMTextureCache::instanse()->getIconSprite("stop", SELECTED_STATE),
                                                 PMTextureCache::instanse()->getIconSprite("stop", DISABLED_STATE),
                                                 CC_CALLBACK_1(GameLayer::restartLevel, parent));
    
    makeStepButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("makeStep", NORMAL_STATE),
                                            PMTextureCache::instanse()->getIconSprite("makeStep", SELECTED_STATE),
                                            PMTextureCache::instanse()->getIconSprite("makeStep", DISABLED_STATE),
                                              CC_CALLBACK_1(GameLayer::makeProgramStep, parent));
    
    if(isNetGame)
    {
        netGameStartButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("start", NORMAL_STATE),
                                                    PMTextureCache::instanse()->getIconSprite("start", SELECTED_STATE),
                                                    PMTextureCache::instanse()->getIconSprite("start", DISABLED_STATE),
                                                      CC_CALLBACK_1(GameLayer::startNetGame, parent));
        
        netGameStartButton->setPosition( Point(0, 
                                             -PROGRAM_CONTROL_BUTTON_SIZE - PROGRAM_CONTROL_BUTTON_SEPARATOR) );
        netGameStartButton->setAnchorPoint( Point::ZERO );
    }
    
    stopProgramButton->setPosition( Point( 2 * (PROGRAM_CONTROL_BUTTON_SIZE + PROGRAM_CONTROL_BUTTON_SEPARATOR), 0) );
    stopProgramButton->setAnchorPoint( Point::ZERO );
    startProgramButton->setPosition( Point(0, 0) );
    startProgramButton->setAnchorPoint( Point::ZERO );
    makeStepButton->setPosition( Point((PROGRAM_CONTROL_BUTTON_SIZE + PROGRAM_CONTROL_BUTTON_SEPARATOR),0) );
    makeStepButton->setAnchorPoint( Point::ZERO );
    
    programMenu = Menu::create(startProgramButton, makeStepButton, stopProgramButton, netGameStartButton, nullptr);
    programMenu->setAnchorPoint( Point::ZERO );
    programMenu->setContentSize(Size(3 * (PROGRAM_CONTROL_BUTTON_SIZE + PROGRAM_CONTROL_BUTTON_SEPARATOR), PROGRAM_CONTROL_BUTTON_SIZE) );
    
    programMenu->setPosition( Point::ZERO );
    
    
    addChild(programMenu,10);
    
    
#ifdef MAP_EDITOR
    editButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("edit", NORMAL_STATE),
                                        PMTextureCache::instanse()->getIconSprite("edit", SELECTED_STATE),
                                        PMTextureCache::instanse()->getIconSprite("edit", DISABLED_STATE),
                                        CC_CALLBACK_1(MapEditorGameLayer::setEdit,MapEditorGameLayer::getInstanse()));
    editButton->setPosition( Point( -60, 0) );
    editButton->setAnchorPoint( Point::ZERO );
    programMenu->addChild(editButton,10);
    programMenu->setContentSize(Size(5 * (PROGRAM_CONTROL_BUTTON_SIZE + PROGRAM_CONTROL_BUTTON_SEPARATOR), PROGRAM_CONTROL_BUTTON_SIZE) );
    
    clearButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("clear", NORMAL_STATE),
                                        PMTextureCache::instanse()->getIconSprite("clear", SELECTED_STATE),
                                        PMTextureCache::instanse()->getIconSprite("clear", DISABLED_STATE),
                                        CC_CALLBACK_1(MapEditorGameLayer::clearMap,MapEditorGameLayer::getInstanse()));
    clearButton->setPosition( Point( -100, 0) );
    clearButton->setAnchorPoint( Point::ZERO );
    programMenu->addChild(clearButton,10);
    programMenu->setContentSize(Size(5 * (PROGRAM_CONTROL_BUTTON_SIZE + PROGRAM_CONTROL_BUTTON_SEPARATOR), PROGRAM_CONTROL_BUTTON_SIZE) );
#endif
    
    setContentSize(programMenu->getContentSize());
    
    return true;
}


void ProgramControlLayer::setEnabled(bool flag)
{
    startProgramButton->setEnabled(flag);
    stopProgramButton->setEnabled(flag);
    makeStepButton->setEnabled(flag);
#ifdef MAP_EDITOR
    editButton->setEnabled(flag);
#endif
}


void ProgramControlLayer::saveStates()
{
    startButtonState = startProgramButton->isEnabled();
    stopProgramButtonState = stopProgramButton->isEnabled();
    makeStepButtonState = makeStepButton->isEnabled();
}

void ProgramControlLayer::restoreStates()
{
    startProgramButton->setEnabled(startButtonState);
    stopProgramButton->setEnabled(stopProgramButtonState);
    makeStepButton->setEnabled(makeStepButtonState);
}
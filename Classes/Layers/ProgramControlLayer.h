//
//  ProgramControlLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/17/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__ProgramControlLayer__
#define __pictomir_cocos2d_x__ProgramControlLayer__

#include <iostream>
#include "PMSettings.h"

class GameLayer;

const float PROGRAM_CONTROL_BUTTON_SIZE = 50.0f;
const float PROGRAM_CONTROL_BUTTON_SEPARATOR = 5.0f;

class ProgramControlLayer : public cocos2d::Layer
{
public:
    
    virtual bool init();
    
    CREATE_FUNC(ProgramControlLayer);
    
    void setEnabled(bool flag);
    
    SYNTHESIZE(cocos2d::MenuItemSprite *, startProgramButton, StartProgramButton);
    SYNTHESIZE(cocos2d::MenuItemSprite *, stopProgramButton, StopProgramButton);
    SYNTHESIZE(cocos2d::MenuItemSprite *, makeStepButton, MakeStepButton);
    SYNTHESIZE(cocos2d::MenuItemSprite *, netGameStartButton, NetGameStartButton);
    
#ifdef MAP_EDITOR
    SYNTHESIZE(cocos2d::MenuItemSprite *, editButton, EditButton);
    SYNTHESIZE(cocos2d::MenuItemSprite *, clearButton, ClearButton);
#endif
    
    ProgramControlLayer() :
        cocos2d::Layer(),
        parent(nullptr)
    {}

    
    ProgramControlLayer(GameLayer *parent, bool isNetGame) :
        cocos2d::Layer(),
        parent(parent),
        isNetGame(isNetGame)
    {}
    
    static ProgramControlLayer* create(GameLayer *parent, bool isNetGame)
    {
        ProgramControlLayer *programLayer = new ProgramControlLayer(parent, isNetGame);
        
        programLayer->init();
        programLayer->autorelease();
        
        return programLayer;
    }
    
    ~ProgramControlLayer()
    {
        removeAllChildrenWithCleanup(true);
    }

    void saveStates();
    void restoreStates();
private:
    GameLayer *parent;
    cocos2d::Menu *programMenu;
    
    bool isNetGame;
    
    bool startButtonState;
    bool stopProgramButtonState;
    bool makeStepButtonState;
};

#endif /* defined(__pictomir_cocos2d_x__ProgramControlLayer__) */

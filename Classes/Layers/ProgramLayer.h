//
//  ProgramLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 30.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__ProgramLayer__
#define __pictomir_cocos2d_x__ProgramLayer__

#include <iostream>
#include "MethodLayer.h"
#include "SelectButton.h"
#include "ChangeableRepeaterButton.h"
#include "cocos-ext.h"

class GameLayer;

const float PROGAMLAYER_MENU_SEPARATOR = 20.0f;

const float PROGRAMLAYER_BORDER_X = 20.0f;
const float PROGRAMLAYER_BORDER_Y = 35.0f;

const float PROGRAMLAYER_PROGRAM_SEPARATOR = 10.0f;

const int PL_ACTIONMENU_ZORDER = 0;
const int PL_TOUCHOVERLAY_ZORDER = 1;
const int PL_SCROLL_ZORDER = 2;


class ProgramLayer : public cocos2d::LayerColor
{
    SYNTHESIZE(GameLayer *,parentLayer,ParentLayer);
    SYNTHESIZE(SelectButton *, activeButton, ActiveButton);
    SYNTHESIZE(cocos2d::Menu *, actionMenu, SelectMenu);
    SYNTHESIZE(cocos2d::extension::ScrollView *, scroll, Scroll);

protected:
    
    enum TouchType
    {
        plNone,
        plDragMenuItem,
        mlDragMethod,
        mlDragCondition,
        mlDragRepeater,
        plSelectMenuItem,
        mlSelectMethod,
        mlSelectCondition,
        mlSelectRepeater,
        plCancelTouch
    };
    
    bool isAnimationRunnnig;
    bool enabled;
    
    bool controlledMode;
    
    cocos2d::Sprite *draggedSprite;
    SelectButton *selectedButton;
    int selectedMethod;
    int selectedMethodLayerItem;
    int selectedLayer;
    
    TouchType touchType;
    
public:
    
    std::vector<MethodLayer *> layers;
    
public:
    ProgramLayer() :
        cocos2d::LayerColor(),
        parentLayer(nullptr),
        activeButton(nullptr),
        actionMenu(nullptr),
        scroll(nullptr),
        isAnimationRunnnig(false),
        enabled(true),
        controlledMode(false),
        draggedSprite(nullptr),
        selectedButton(nullptr),
        selectedMethodLayerItem(-1),
        selectedLayer(-1),
        touchType(plNone)
    {}
    
    static ProgramLayer* create()
    {
        return new ProgramLayer();
    }
    
    ProgramLayer(GameLayer *parent) :
        cocos2d::LayerColor(),
        parentLayer(parent),
        //activeButton(nullptr),
        actionMenu(nullptr),
        scroll(nullptr),
        isAnimationRunnnig(false),
        enabled(true),
        controlledMode(false),
        draggedSprite(nullptr),
        selectedButton(nullptr),
        selectedMethodLayerItem(-1),
        selectedLayer(-1),
        touchType(plNone)
    {}
    
    static ProgramLayer* create(GameLayer *parent)
    {
        ProgramLayer *programLayer = new ProgramLayer(parent);
        
        programLayer->init();
        programLayer->autorelease();
        
        return programLayer;
    }
    
    ~ProgramLayer()
    {
        _eventDispatcher->removeEventListener(listener);
        removeAllChildrenWithCleanup(true);
    }
    
    virtual bool init();
    
    void setEnabled(bool state);
    
    bool isEnabled()
    {
        return enabled;
    }
    
    void reset();
    
    std::vector<_instruction> getMainMethod();

    void setControlledMode(bool flag);
    
    void drawLayers(AbstractWorld *world);
    
    void update();
    
    void setWorldAndLevel();
private:
    
    cocos2d::EventListenerTouchOneByOne *listener;
    cocos2d::Label * labelWorld;
    int worldLabelHeight;
    
    void selectAction(cocos2d::Ref *sender);
    void addAction(cocos2d::Ref *sender);
    void addCondition(cocos2d::Ref *sender);
    void addRepeater(cocos2d::Ref *sender);
    
    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    
    SelectButton *findMenuButtonAtTouch(cocos2d::Touch *touch);
    int layerAtTouch(cocos2d::Touch *touch);

    
    bool isDragging();
    bool isSelecting();
    
#ifdef MAP_EDITOR
    cocos2d::Menu * addFunctionMenu;
    void addMethod (cocos2d::Ref * sender);
    
#endif
};

#endif /* defined(__pictomir_cocos2d_x__ProgramLayer__) */

//
//  RobotLooseLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__RobotLooseLayer__
#define __pictomir_cocos2d_x__RobotLooseLayer__

#include <iostream>
#include "cocos2d.h"

USING_NS_CC;

class GameLayer;

class RobotLooseLayer : public cocos2d::LayerColor
{
private:
    GameLayer *parent;
    
    void mainMenu(cocos2d::Ref *sender);
    void nextLevel(cocos2d::Ref *sender);
public:
    
    RobotLooseLayer():
        cocos2d::LayerColor(),
        parent(nullptr)
    {}
    
    RobotLooseLayer(GameLayer *parent):
        cocos2d::LayerColor(),
        parent(parent)
    {}
    
    virtual bool init();
    
    CREATE_FUNC(RobotLooseLayer);
    
    static RobotLooseLayer *create(GameLayer *parent);
    
    void show();
    void hide();
    
    void selfRemove();
    
    ~RobotLooseLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
};

#endif /* defined(__pictomir_cocos2d_x__RobotLooseLayer__) */

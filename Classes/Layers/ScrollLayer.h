//
//  ScrollLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 10.04.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__ScrollLayer__
#define __pictomir_cocos2d_x__ScrollLayer__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "AbstractMap.h"

const double SCROLLLAYER_TOUCH_OFFSET = 200.0;
const double SCROLLLAYER_ACTIVELAYER_RANGE = 50.0;
const double SCROLLLAYER_ACTIVELAYER_DELTA = 3.0;

class ScrollLayer : public cocos2d::Layer
{
    enum TouchType {
        DragLayer,
        ScrollLayers,
        EditLayer,
        DragRobot,
        ResizeMap
    };
    
public:
    
    ScrollLayer() :
        cocos2d::Layer(),
        previewDraw(false),
        layer(nullptr)
    {
        init();
    }
    ScrollLayer(AbstractMap *layer, cocos2d::Size size, bool previewDraw) :
        cocos2d::Layer(),
        previewDraw(previewDraw),
        layer(layer)
    {
        init();
        setContentSize(size);
        //setViewSize(size);
    }
    
    ~ScrollLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    static ScrollLayer* create()
    {
        ScrollLayer *layer = new ScrollLayer();
        layer->autorelease();
        return layer;
    }
    
    static ScrollLayer* create(AbstractMap *layer, cocos2d::Size size, bool previewDraw)
    {
        ScrollLayer *ret = new ScrollLayer(layer, size, previewDraw);
        ret->autorelease();
        return ret;
    }

    virtual bool init();
    
    void removeBackground()
    {
        parallax->removeChildByTag(0, true);
    }
    
protected:
    
    SYNTHESIZE(cocos2d::ParallaxNode *, parallax,Parallax);
    SYNTHESIZE(bool, active, Active);
private:
    bool previewDraw;
    AbstractMap *layer;
    cocos2d::Point startPoint;
    cocos2d::Point startLayerPosition;
    cocos2d::Size  startSize;
    TouchType touchType;
    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    
    void touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
   
    int dragedRobot;
};



#endif /* defined(__pictomir_cocos2d_x__ScrollLayer__) */

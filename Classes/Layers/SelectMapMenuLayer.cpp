//
//  SelectMapMenuLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "SelectMapMenuLayer.h"
#include "GameLayer.h"
#include "utils.h"
#include "MainMenuLayer.h"
#include "SelectMenuLayer.h"
#include "ScrollLayer.h"
#include "PMMapPreview.h"

Scene* SelectMapMenuLayer::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    SelectMapMenuLayer *layer = SelectMapMenuLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer,0,0);
    
    // return the scene
    return scene;
}

bool SelectMapMenuLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size screenSize = PMSettings::instance()->getScreenSize();
    
    setPosition( Point(0,0) );
    
    Sprite *backGround  = Sprite::create(PICT_PATH "Background/background1.png" );
    backGround->setPosition( Point(screenSize.width/2, screenSize.height/2) );
    resizeSprite(backGround, screenSize.width, screenSize.height);
    addChild(backGround, -1);
    
    Menu *navigationMenu = Menu::create(nullptr);
    
    int worldPart = PMSettings::instance()->getWorldPart();
    
    Label *worldNumber = Label::create(StringUtils::format(LocalizedString("WorldPattern").c_str(), worldPart+1), PMSettings::instance()->getFont(), 32);
    worldNumber->setPosition( Point(screenSize.width / 2, screenSize.height / 2 + 50 + PMMapPreviewHeight / 2) );
    addChild(worldNumber,1);
    
    scroll = ScrollView::create();

    drawScroll();
    
    addChild(scroll);
    
#ifdef MAP_EDITOR
    MenuItemFont *uploadToServer = MenuItemFont::create(LocalizedString("UploadToServer"), CC_CALLBACK_1(SelectMapMenuLayer::uploadMapsToServer, this));
    uploadToServer->setPosition( Point(170, screenSize.height - 125) );
    uploadToServer->setColor(Color3B(20, 200, 20));
    navigationMenu->addChild(uploadToServer);
    
    MenuItemFont *deleteWorld = MenuItemFont::create(LocalizedString("DeleteGame"), CC_CALLBACK_1(SelectMapMenuLayer::DeleteWorld, this));
    deleteWorld->setPosition( Point(170, screenSize.height - 175) );
    deleteWorld->setColor(Color3B(200, 20, 20));
    navigationMenu->addChild(deleteWorld);
#endif
    
    
    MenuItemFont *goToMain = MenuItemFont::create(LocalizedString("MainMenu"),
                                                  CC_CALLBACK_1(SelectMapMenuLayer::mainMenu, this));
    
    MenuItemFont *goToSelectWorld = MenuItemFont::create(LocalizedString("Back"),
                                                         CC_CALLBACK_1(SelectMapMenuLayer::worldsMenu, this));
    
    goToMain->setPosition( Point(170, screenSize.height - 25) );
    goToSelectWorld->setPosition( Point(170, screenSize.height - 75) );
    
    navigationMenu->addChild(goToMain);
    navigationMenu->addChild(goToSelectWorld);
    navigationMenu->setPosition( Point::ZERO );
    addChild(navigationMenu, 1);
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(SelectMapMenuLayer::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(SelectMapMenuLayer::touchEnded, this);
    listener->onTouchMoved = CC_CALLBACK_2(SelectMapMenuLayer::touchMoved, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
    
}

void SelectMapMenuLayer::drawScroll () {
    scroll->getContainer()->removeAllChildren();
    
    int worldPart = PMSettings::instance()->getWorldPart();
    Size screenSize = PMSettings::instance()->getScreenSize();
    
#ifdef MAP_EDITOR
    scroll->setViewSize( Size(MIN(screenSize.width,
                                  (PMSettings::instance()->getMapsCount(worldPart)+2)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset), PMMapPreviewHeight) );
    scroll->setPosition( Point(MAX(0, screenSize.width / 2 - ((PMSettings::instance()->getMapsCount(worldPart)+2)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset) / 2),screenSize.height / 2 - PMMapPreviewHeight / 2) );
    scroll->setContentSize( Size((PMSettings::instance()->getMapsCount(worldPart)+2)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, PMMapPreviewHeight) );
#else
    scroll->setViewSize( Size(MIN(screenSize.width,
                                  PMSettings::instance()->getMapsCount(worldPart)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset), PMMapPreviewHeight) );
    scroll->setPosition( Point(MAX(0, screenSize.width / 2 - (PMSettings::instance()->getMapsCount(worldPart)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset) / 2),screenSize.height / 2 - PMMapPreviewHeight / 2) );
    scroll->setContentSize( Size(PMSettings::instance()->getMapsCount(worldPart)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, PMMapPreviewHeight) );
#endif
    scroll->setContentOffset( Point(0, 0), true);
    scroll->setClippingToBounds(true);
    scroll->setDirection(ScrollView::Direction::HORIZONTAL);
    scroll->setZoomScale(0.5);
    scroll->setBounceable(true);
    
    for (int i = 0; i < PMSettings::instance()->getMapsCount(worldPart); ++i) {
        PMMapPreview * preview = PMMapPreview::create(worldPart, i+1);
        preview->setPosition(Point(i*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
        scroll->addChild(preview);
        
#ifdef MAP_EDITOR
        Sprite* copySprite = PMTextureCache::instanse()->getIconSprite("saveHint");
        copySprite->setAnchorPoint(Point(1,1));
        copySprite->setPosition(Point(PMMapPreviewWidth,PMMapPreviewHeight));
        preview->addChild(copySprite);
        
        Sprite* deleteSprite = PMTextureCache::instanse()->getIconSprite("clear");
        deleteSprite->setAnchorPoint(Point(1,1));
        deleteSprite->setPosition(Point(PMMapPreviewWidth - 30,PMMapPreviewHeight));
        preview->addChild(deleteSprite);
#endif
    }
    
#ifdef MAP_EDITOR
    dragPreview = nullptr;
    ISdrag = false;
    addMapPreview = PMMapPreview::create();
    addMapPreview->setPosition(Point(PMSettings::instance()->getMapsCount(worldPart)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
    scroll->addChild(addMapPreview);
    
    pasteMapPreview = PMMapPreview::createPastePreview();
    pasteMapPreview->setPosition(Point((PMSettings::instance()->getMapsCount(worldPart)+1)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
    scroll->addChild(pasteMapPreview);
#endif
}

void SelectMapMenuLayer::mainMenu(cocos2d::Ref *sender)
{
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, MainMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void SelectMapMenuLayer::worldsMenu(cocos2d::Ref *sender)
{
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, SelectMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

#ifdef MAP_EDITOR
void SelectMapMenuLayer::addMap (cocos2d::Ref *sender)
{
    addMapPreview->setPosition((addMapPreview->getPosition()+Point(PMMapPreviewWidth+PMMapPreviewOffset,0)));
    pasteMapPreview->setPosition((pasteMapPreview->getPosition()+Point(PMMapPreviewWidth+PMMapPreviewOffset,0)));
    if (MIN(PMSettings::instance()->getScreenSize().width, (PMSettings::instance()->getMapsCount()+2)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset) < (PMSettings::instance()->getMapsCount()+2)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset) {
        float newX = scroll->getContainer()->getPosition().x - PMMapPreviewWidth - PMMapPreviewOffset;
        scroll->setContentOffset(Point(newX, 0),true);
    }
    
    
    PMSettings::instance()->addMap();
    
    std::string mapPath = PMSettings::instance()->pathToMap(PMSettings::instance()->getWorldPart(), PMSettings::instance()->getMapsCount());
    
    char MAP[10000] = "";
    
    std::string newMapPath = FileUtils::getInstance()->fullPathForFilename("Maps/new.map");
    
    FILE* map = fopen(newMapPath.c_str(), "rb");
    fread(MAP,1,10000,map);
    fclose(map);

    
    FILE* newmap = fopen(mapPath.c_str(), "wb");
    
    fprintf(newmap,"%s",MAP);
    fclose(newmap);
    
    MapEditorGameLayer::getInstanse()->mapVer++;
    MapEditorGameLayer::getInstanse()->saveWorldsetLocal();
    
    drawScroll();
}

void SelectMapMenuLayer::pasteMap (cocos2d::Ref *sender) {
    if (PMSettings::instance()->getCopyMap() == "") return;
    addMapPreview->setPosition((addMapPreview->getPosition()+Point(PMMapPreviewWidth+PMMapPreviewOffset,0)));
    pasteMapPreview->setPosition((pasteMapPreview->getPosition()+Point(PMMapPreviewWidth+PMMapPreviewOffset,0)));
    
    float newX = scroll->getContainer()->getPosition().x - PMMapPreviewWidth - PMMapPreviewOffset;
    
    scroll->setContentOffset(Point(newX, 0),true);
    
    PMSettings::instance()->addMap();
    
    std::string mapPath = PMSettings::instance()->pathToMap(PMSettings::instance()->getWorldPart(), PMSettings::instance()->getMapsCount());
    
    char MAP[10000] = "";
    
    std::string newMapPath = PMSettings::instance()->getCopyMap();
    
    FILE* map = fopen(newMapPath.c_str(), "rb");
    fread(MAP,1,10000,map);
    //fscanf(map,"%[]",MAP);
    fclose(map);
    
    
    FILE* newmap = fopen(mapPath.c_str(), "wb");
    
    fprintf(newmap,"%s",MAP);
    fclose(newmap);
    
    MapEditorGameLayer::getInstanse()->mapVer++;
    MapEditorGameLayer::getInstanse()->saveWorldsetLocal();
    
    drawScroll();
}

void SelectMapMenuLayer::uploadMapsToServer (cocos2d::Ref *sender)
{
    MapEditorGameLayer::getInstanse()->uploadMapsToServer();
}


void SelectMapMenuLayer::DeleteWorld (cocos2d::Ref * sender)
{
    system(("rm -rf "+PMSettings::instance()->pathToWorld()).c_str());
 
    for (int i = PMSettings::instance()->getWorldPart()+1; i <= PMSettings::instance()->getWorldsCount(); ++i) {
        std:: string oldMapName = PMSettings::instance()->pathToWorld(i);
        std:: string newMapName = PMSettings::instance()->pathToWorld(i-1);
        rename(oldMapName.c_str(), newMapName.c_str());
    }
    PMSettings::instance()->deleteWorld();
    
    MapEditorGameLayer::getInstanse()->saveWorldsetLocal();
 
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, SelectMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}
 

#endif

bool SelectMapMenuLayer::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
#ifdef MAP_EDITOR
    time = getTimeMs();
#endif
    
    return scroll->onTouchBegan(touch, pEvent);
}

void SelectMapMenuLayer::touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
#ifdef MAP_EDITOR
    Point dif = (touch->getLocation()-touch->getStartLocation());
    
    dif.x = fabs(dif.x);
    dif.y = fabs(dif.y);
    
    if (dif.x < 20 && dif.y < 20 && !dragPreview)
    {
        if (time + 1000 < getTimeMs())
        {
            Point touchPoint = scroll->getContainer()->convertToNodeSpace(touch->getStartLocation());
            
            mapNum = (touchPoint.x/(PMMapPreviewOffset+PMMapPreviewWidth)) + 1;
            
            if ( mapNum < 1 ) return;
            if ( mapNum > PMSettings::instance()->getMapsCount()) return;
            
            ISdrag = true;
            
            dragPreview = PMMapPreview::create(PMSettings::instance()->getWorldPart(), mapNum);
            dragPreview->setScale(1.2);
            dragPreview->setContentSize(Size(PMMapPreviewWidth+20, PMMapPreviewHeight+20));
            if(dragPreview)addChild(dragPreview);
            //scroll->TouchCancelled(touch, pEvent);
            
            delta = ((Node*)scroll->getContainer()->getChildren().at(mapNum-1))->convertToNodeSpace(touch->getLocation());
            ((Node*)scroll->getContainer()->getChildren().at(mapNum-1))->setVisible(false);
        }
    }
    
    if (ISdrag) {
        if(dragPreview){
            dragPreview->setPosition((convertTouchToNodeSpace(touch)-delta));
            
            if(scroll->convertTouchToNodeSpace(touch).x < 200) {
                scroll->setContentOffset((scroll->getContentOffset()+ Point(20,0)), true);
            }
            
            if(scroll->convertTouchToNodeSpace(touch).x > scroll->getViewSize().width-200) {
                scroll->setContentOffset((scroll->getContentOffset()+ Point(-20,0)), true);
            }
            
            Point touchPoint = scroll->getContainer()->convertToNodeSpace(touch->getLocation());
            
            int mapNum_ = (touchPoint.x/(PMMapPreviewOffset+PMMapPreviewWidth));
            if (mapNum_ > PMSettings::instance()->getMapsCount()) {
                mapNum_ = PMSettings::instance()->getMapsCount();
            }
            if(mapNum_ < 0)mapNum_ = 0;
            
            if (mapNum_ > mapNum-1) {
                for (int i = 0; i < mapNum; ++i) {
                    ((Node*)scroll->getContainer()->getChildren().at(i))->setPosition(Point(i*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
                }
                for (int i = mapNum; i < mapNum_+1; ++i) {
                    ((Node*)scroll->getContainer()->getChildren().at(i))->setPosition(Point((i-1)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
                }
                for (int i = mapNum_+1; i < scroll->getContainer()->getChildren().size(); ++i) {
                    ((Node*)scroll->getContainer()->getChildren().at(i))->setPosition(Point(i*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
                }
            }
            else {
                for (int i = 0; i < mapNum_; ++i) {
                    ((Node*)scroll->getContainer()->getChildren().at(i))->setPosition(Point(i*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
                }
                for (int i = mapNum_; i < mapNum-1; ++i) {
                    ((Node*)scroll->getContainer()->getChildren().at(i))->setPosition(Point((i+1)*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
                }
                for (int i = mapNum; i < scroll->getContainer()->getChildren().size(); ++i) {
                    ((Node*)scroll->getContainer()->getChildren().at(i))->setPosition(Point(i*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
                }
            }
        }
    }
#endif
}

void SelectMapMenuLayer::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(scroll->getContentOffset().x > 0 )scroll->setContentOffset(Point::ZERO,true);
    if(scroll->getContentOffset().x <  - scroll->getContentSize().width + scroll->getViewSize().width )
        scroll->setContentOffset(Point(-scroll->getContentSize().width + scroll->getViewSize().width,0),true);
    
    scroll->onTouchEnded(touch, pEvent);
     #ifdef MAP_EDITOR
    if(dragPreview)
    {
        removeChild(dragPreview, true);
        dragPreview = nullptr;
    }
    

    if(ISdrag) {
        ISdrag = false;
        
        Point touchPoint = scroll->getContainer()->convertTouchToNodeSpace(touch);
        
        int newmapnum = (touchPoint.x/(PMMapPreviewOffset+PMMapPreviewWidth)) + 1;
        
        if (newmapnum > PMSettings::instance()->getMapsCount() )
            newmapnum = PMSettings::instance()->getMapsCount();
        
        if (newmapnum < 1) {
            newmapnum = 1;
        }
        
        printf("start map %d\n", mapNum);
        printf("end map %d\n", newmapnum);
        
        if(newmapnum == mapNum)
        {
            ((Node*)scroll->getContainer()->getChildren().at(mapNum-1))->setVisible(true);
            for (int i = 0; i < scroll->getContainer()->getChildren().size(); ++i) {
                ((Node*)scroll->getContainer()->getChildren().at(i))->setPosition(Point(i*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
            }
            return;
        }
        
        rename(PMSettings::instance()->pathToMap(PMSettings::instance()->getWorldPart(), mapNum).c_str(), (writablePath+"tmp.map").c_str());
        
        for (int i = mapNum; i != newmapnum; i+=sign(newmapnum-mapNum)) {
            rename(PMSettings::instance()->pathToMap(PMSettings::instance()->getWorldPart(), i+sign(newmapnum-mapNum)).c_str(), PMSettings::instance()->pathToMap(PMSettings::instance()->getWorldPart(), i).c_str());
        }
        
        rename((writablePath+"tmp.map").c_str(), PMSettings::instance()->pathToMap(PMSettings::instance()->getWorldPart(), newmapnum).c_str());
        
        scroll->getContainer()->removeAllChildrenWithCleanup(true);
        
        for (int i = 0; i < PMSettings::instance()->getMapsCount(); ++i) {
            PMMapPreview * preview = PMMapPreview::create(PMSettings::instance()->getWorldPart(), i+1);
            preview->setPosition(Point(i*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
            scroll->addChild(preview);
        }
        
        addMapPreview = PMMapPreview::create();
        addMapPreview->setPosition(Point(PMSettings::instance()->getMapsCount()*(PMMapPreviewWidth + PMMapPreviewOffset) + PMMapPreviewOffset, 0));
        scroll->addChild(addMapPreview);
    }
    
    else {
#endif

		Point dif = touch->getLocation() - touch->getStartLocation();
        
        dif.x = fabs(dif.x);
        dif.y = fabs(dif.y);
        
        if (dif.x < 5 && dif.y < 5) {
            Point touchPoint = scroll->getContainer()->convertTouchToNodeSpace(touch);
            
            int map = (touchPoint.x/(PMMapPreviewOffset+PMMapPreviewWidth)) + 1;
            
            if (map > 0 && map <= PMSettings::instance()->getMapsCount())
            {
#ifdef MAP_EDITOR
                PMSettings::instance()->setLevel(map);
                Rect copybox = Rect((map-1)*(PMMapPreviewOffset+PMMapPreviewWidth)+170, 170, 30, 30);
                Rect deletebox = Rect((map-1)*(PMMapPreviewOffset+PMMapPreviewWidth)+120, 150, 50, 50);
                if (copybox.containsPoint(touchPoint)) {
                    PMSettings::instance()->setCopyMap( PMSettings::instance()->pathToMap(PMSettings::instance()->getWorldPart(), map) );
                } else if (deletebox.containsPoint(touchPoint)) {
                    PMSettings::instance()->deleteMap( map );
                    MapEditorGameLayer::getInstanse()->saveWorldsetLocal();
                    drawScroll();
                }
                else {
                    MapEditorGameLayer::getInstanse()->reloadWorld();
                    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, MapEditorGameLayer::scene_());
                
                    Director::getInstance()->replaceScene(trans);
                }
#else
                PMSettings::instance()->setLevel(map);
                TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, GameLayer::scene());
                Director::getInstance()->replaceScene(trans);
#endif
                
            }
#ifdef MAP_EDITOR
            else if (map - 1 == PMSettings::instance()->getMapsCount() )
            {
                addMap(nullptr);
            } else if (map - 2 == PMSettings::instance()->getMapsCount()) {
                pasteMap(nullptr);
            }
#endif
            
        }
#ifdef MAP_EDITOR
    }
#endif
}


//
//  SelectMapMenuLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__SelectMapMenuLayer__
#define __pictomir_cocos2d_x__SelectMapMenuLayer__

#include <iostream>

#include "cocos-ext.h"
#include "PMMapPreview.h"

USING_NS_CC;
USING_NS_CC_EXT;

class SelectMapMenuLayer : public cocos2d::Layer
{
public:
    virtual bool init();
    
    CREATE_FUNC(SelectMapMenuLayer);
    
    static cocos2d::Scene* scene();
    
    ~SelectMapMenuLayer()
    {
        removeAllChildrenWithCleanup(true);
        cocos2d::Director::getInstance()->getTextureCache()->removeAllTextures();
    }
    
private:
    ScrollView * scroll;
    Menu *levelSelectMenu;
    
    void mainMenu(cocos2d::Ref *sender);
    void worldsMenu(cocos2d::Ref *sender);
    
    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void drawScroll ();
    
#ifdef MAP_EDITOR
    PMMapPreview * dragPreview;
    bool ISdrag;
    int mapNum;
    time_t time;
    Point delta;
    
    PMMapPreview * addMapPreview;
    PMMapPreview * pasteMapPreview;
    void addMap (cocos2d::Ref *sender);
    void pasteMap (cocos2d::Ref *sender);
    void uploadMapsToServer (cocos2d::Ref *sender);
    
    void DeleteWorld (cocos2d::Ref * sender);
#endif
};

#endif /* defined(__pictomir_cocos2d_x__SelectMapMenuLayer__) */

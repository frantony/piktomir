//
//  SelectMenuLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "SelectMenuLayer.h"
#include "SelectMapMenuLayer.h"
#include "utils.h"
#include "PMSettings.h"
#include "MainMenuLayer.h"

USING_NS_CC;
using namespace std;

Scene* SelectMenuLayer::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    SelectMenuLayer *layer = SelectMenuLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer,0,0);
    
    // return the scene
    return scene;
}

bool SelectMenuLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    
    
    
    Size screenSize = PMSettings::instance()->getScreenSize();
    
    setPosition( Point(0,0) );
    
    Sprite *backGround  = Sprite::create(PICT_PATH "Background/background1.png" );
    backGround->setPosition( Point(screenSize.width/2, screenSize.height/2) );
    resizeSprite(backGround, screenSize.width, screenSize.height);
    addChild(backGround, -1);
    
    Menu *navigationMenu = Menu::create(nullptr);
    worldSelectMenu = Menu::create();
    
    
    int MapsSize = PMSettings::instance()->getWorldsCount();

    worldSelectMenu->setPosition( Point(0, 50 * MapsSize / 2) );
    for (int i = 0; i < MapsSize; ++i) {
        MenuItemFont * world_ = MenuItemFont::create(StringUtils::format(LocalizedString("WorldPattern").c_str(), i+1),
                                                     CC_CALLBACK_1(SelectMenuLayer::setWorld, this));
        world_->setPosition( Point(screenSize.width / 2, screenSize.height / 2 - i*50) );
        worldSelectMenu->addChild(world_);
        
#ifdef MAP_EDITOR
        MenuItemSprite *deleteSprite = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("clear", NORMAL_STATE), PMTextureCache::instanse()->getIconSprite("clear", NORMAL_STATE), CC_CALLBACK_1(SelectMenuLayer::deleteWorld, this));
        deleteSprite->setPosition( Point(screenSize.width / 2 + 90, screenSize.height / 2 - i*50) );
        worldSelectMenu->addChild(deleteSprite);
#endif
    }

    
    
#ifdef MAP_EDITOR
    plusButton = MenuItemFont::create("+", CC_CALLBACK_1(SelectMenuLayer::addWorld, this));
    plusButton->setPosition( Point(screenSize.width / 2, screenSize.height / 2 - MapsSize*50) );
    worldSelectMenu->addChild(plusButton,1);
#endif
    
    addChild(worldSelectMenu);
    
#ifdef MAP_EDITOR
    MenuItemFont *uploadToServer = MenuItemFont::create(LocalizedString("UploadToServer"), CC_CALLBACK_1(SelectMenuLayer::uploadMapsToServer, this));
    uploadToServer->setPosition( Point(170, screenSize.height - 75) );
    uploadToServer->setColor(Color3B(20, 200, 20));
    navigationMenu->addChild(uploadToServer);
#endif
    
    MenuItemFont *goToMain = MenuItemFont::create(LocalizedString("MainMenu"),
                                                  CC_CALLBACK_1(SelectMenuLayer::mainMenu, this));
    goToMain->setPosition( Point(170, screenSize.height - 25) );
    
    navigationMenu->addChild(goToMain);
    navigationMenu->setPosition( Point::ZERO );
    addChild(navigationMenu, 1);
    
    
    return true;
}

void SelectMenuLayer::deleteWorld(cocos2d::Ref *sender) {
#ifdef MAP_EDITOR
    int MapsSize = PMSettings::instance()->getWorldsCount();

    int world = 0;
    
    for(int i = 0; i < worldSelectMenu->getChildrenCount(); ++i) {
        if(worldSelectMenu->getChildren().at(2*i+1) == sender) {
            world = i;
            break;
        }
    }
    
    system(("rm -rf "+PMSettings::instance()->pathToWorld()).c_str());
    
    for (int i = PMSettings::instance()->getWorldPart()+1; i <= MapsSize; ++i) {
        std:: string oldMapName = PMSettings::instance()->pathToWorld(i);
        std:: string newMapName = PMSettings::instance()->pathToWorld(i-1);
        rename(oldMapName.c_str(), newMapName.c_str());
    }
    
    PMSettings::instance()->deleteWorld(world);
    
#ifdef MAP_EDITOR
    MapEditorGameLayer::getInstanse()->saveWorldsetLocal();
#endif
    Director::getInstance()->replaceScene(SelectMenuLayer::scene());
#endif
}

void SelectMenuLayer::setWorld(cocos2d::Ref *sender)
{
#ifdef MAP_EDITOR
    for(int i = 0; i < worldSelectMenu->getChildrenCount(); ++i) {
        if(worldSelectMenu->getChildren().at(2*i) == sender) {
            PMSettings::instance()->setWorldPart(i);
            
            TransitionSlideInR* trans = TransitionSlideInR::create( 1.5f * SYSTEM_ANIMATION_DELAY, SelectMapMenuLayer::scene());
            Director::getInstance()->replaceScene(trans);
            break;
        }
    }
#else
    for(int i = 0; i < worldSelectMenu->getChildrenCount(); ++i) {
        if(worldSelectMenu->getChildren().at(i) == sender) {
            PMSettings::instance()->setWorldPart(i);
            
            TransitionSlideInR* trans = TransitionSlideInR::create( 1.5f * SYSTEM_ANIMATION_DELAY, SelectMapMenuLayer::scene());
            Director::getInstance()->replaceScene(trans);
            break;
        }
    }
#endif
}

void SelectMenuLayer::mainMenu(cocos2d::Ref *object)
{
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, MainMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

#ifdef MAP_EDITOR
void SelectMenuLayer::addWorld (cocos2d::Ref *object)
{
    plusButton->removeFromParent();
    int MapsSize = PMSettings::instance()->getWorldsCount();
    
    worldSelectMenu->setPosition( Point(0, 50 * MapsSize / 2) );
    
    Size screenSize = PMSettings::instance()->getScreenSize();
    MenuItemFont * world_ = MenuItemFont::create(StringUtils::format(LocalizedString("WorldPattern").c_str(), MapsSize+1), CC_CALLBACK_1(SelectMenuLayer::setWorld, this));
    world_->setPosition( Point(screenSize.width / 2, screenSize.height / 2 - MapsSize*50) );
    worldSelectMenu->addChild(world_);
    
    MenuItemSprite *deleteSprite = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("clear", NORMAL_STATE), PMTextureCache::instanse()->getIconSprite("clear", NORMAL_STATE), CC_CALLBACK_1(SelectMenuLayer::deleteWorld, this));
    deleteSprite->setPosition( Point(screenSize.width / 2 + 90, screenSize.height / 2 - MapsSize*50) );
    worldSelectMenu->addChild(deleteSprite);
    
    plusButton = MenuItemFont::create("+", CC_CALLBACK_1(SelectMenuLayer::addWorld, this));
    plusButton->setPosition( Point(screenSize.width / 2, screenSize.height / 2 - MapsSize*50) );
    worldSelectMenu->addChild(plusButton,1);
    
    PMSettings::instance()->addWorld();
    
    plusButton->setPosition( Point(screenSize.width / 2, screenSize.height / 2 - MapsSize*50) );
    
    char tmp[100] = "";
    sprintf(tmp, "%s",PMSettings::instance()->pathToWorld(MapsSize).c_str());
    
    platform_mkdir(tmp);
    
    MapEditorGameLayer::getInstanse()->mapVer++;
    MapEditorGameLayer::getInstanse()->saveWorldsetLocal();
    
    plusButton->setPosition( Point(screenSize.width / 2, screenSize.height / 2 - (MapsSize+1)*50) );
}

void SelectMenuLayer::uploadMapsToServer (cocos2d::Ref *sender)
{
    MapEditorGameLayer::getInstanse()->uploadMapsToServer();
}
#endif

//
//  SelectMenuLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__SelectMenuLayer__
#define __pictomir_cocos2d_x__SelectMenuLayer__

#include <iostream>
#include "cocos2d.h"

USING_NS_CC;

class SelectMenuLayer : public cocos2d::Layer
{
public:
    virtual bool init();
    
    CREATE_FUNC(SelectMenuLayer);
    
    static cocos2d::Scene* scene();
    
    ~SelectMenuLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
private:
    cocos2d::Menu *worldSelectMenu;
    
    void setWorld(cocos2d::Ref *sender);
    void mainMenu(cocos2d::Ref *sender);
    
    void deleteWorld(cocos2d::Ref *object);

#ifdef MAP_EDITOR
    void addWorld (cocos2d::Ref *object);
    void uploadMapsToServer (cocos2d::Ref *sender);
    
    cocos2d::MenuItemFont * plusButton;
#endif
};


#endif /* defined(__pictomir_cocos2d_x__SelectMenuLayer__) */

//
//  SelectMethodBackgroundMenu.cpp
//  Piktomir
//
//  Created by Бесшапошников Никита on 27.03.14.
//
//

#include "SelectMethodExecuteTypeMenu.h"
#include "MethodLayer.h"
#include "utils.h"
#include "PMTextureCache.h"

USING_NS_CC;

SelectMethodExecuteTypeMenu *SelectMethodExecuteTypeMenu::create(MethodLayer *methodLayer, int buttonIndex)
{
    SelectMethodExecuteTypeMenu *pRet = new SelectMethodExecuteTypeMenu(methodLayer, buttonIndex);
    
    if(pRet && pRet->init()) {
        pRet->autorelease();
        return pRet;
    } else {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
    
}

bool SelectMethodExecuteTypeMenu::init()
{
    if ( !Layer::init())
    {
        return false;
    }
    
    setScale(0.7);
    setContentSize( Size(3 * BUTTON_TILE_SIZE + 2 * SMB_BORDER + 2 * SMB_SEPARATOR, BUTTON_TILE_SIZE + 2 * SMB_BORDER) );
    
    layerColor= LayerColor::create(Color4B(145,145,145,255 * 0.8), getContentSize().width, getContentSize().height);
    
    int x = 0.0f;
    
    Menu *menu = Menu::create();
    menu->setPosition( Point(SMB_BORDER, SMB_BORDER) );
    menu->setAnchorPoint( Point::ZERO );
    
    for(int i = 0; i < 3; ++i)
    {
        Sprite *normalSprite = PMTextureCache::instanse()->getRobotMethodSprite(EMPTY_METHOD, (_method::ExecuteType)i, NORMAL_STATE);
        Sprite *selectedSprite = PMTextureCache::instanse()->getRobotMethodSprite(EMPTY_METHOD, (_method::ExecuteType)i, SELECTED_STATE);
        Sprite *disabledSprite = PMTextureCache::instanse()->getRobotMethodSprite(EMPTY_METHOD, (_method::ExecuteType)i, DISABLED_STATE);
        
        float diff = (normalSprite->getContentSize().width - selectedSprite->getContentSize().width)/2;
        
        selectedSprite->setPosition( Point(diff, diff) );
        
        MenuItemSprite *background = MenuItemSprite::create(normalSprite, selectedSprite, disabledSprite, CC_CALLBACK_1(SelectMethodExecuteTypeMenu::selectExecuteType, this));
        
        background->setAnchorPoint( Point::ZERO );
        background->setPosition( Point(x ,0) );
        
        menu->addChild(background, 0, i);
        x+= BUTTON_TILE_SIZE + SMB_SEPARATOR;
    }
    
    layerColor->addChild(menu);
    
    auto shape = DrawNode::create();
    
    Point *rect = new Point[20 * 4];
    
    float BORDER_RADIUS = SMB_BORDER;
    
    Point *arc0 = getArcPoints(BORDER_RADIUS, BORDER_RADIUS , BORDER_RADIUS ,CC_DEGREES_TO_RADIANS(180),CC_DEGREES_TO_RADIANS(90), 20);
    Point *arc1 = getArcPoints(getContentSize().width - BORDER_RADIUS, BORDER_RADIUS, BORDER_RADIUS, CC_DEGREES_TO_RADIANS(270), CC_DEGREES_TO_RADIANS(90), 20);
    Point *arc2 = getArcPoints(getContentSize().width - BORDER_RADIUS, getContentSize().height - BORDER_RADIUS, BORDER_RADIUS,CC_DEGREES_TO_RADIANS(0),CC_DEGREES_TO_RADIANS(90), 20);
    Point *arc3 = getArcPoints(BORDER_RADIUS, getContentSize().height - BORDER_RADIUS, BORDER_RADIUS,CC_DEGREES_TO_RADIANS(90),CC_DEGREES_TO_RADIANS(90), 20);
    
    memcpy(rect, arc0, 20 * sizeof(Point));
    memcpy(rect + 20, arc1, 20 * sizeof(Point));
    memcpy(rect + 40, arc2, 20 * sizeof(Point));
    memcpy(rect + 60, arc3, 20 * sizeof(Point));
    
    static Color4F green(0, 1, 0, 1);
    shape->drawPolygon(rect, 4 * 20, green, 0, green);
    shape->setPosition( Point(0, 0) );
    
    auto clipper = ClippingNode::create();
    clipper->setAnchorPoint(Point(0, 0));
    clipper->setPosition( Point::ZERO );
    clipper->setStencil(shape);
    clipper->addChild(layerColor);
    
    
    addChild(clipper, 0);
    
    delete rect;
    delete arc0;
    delete arc1;
    delete arc2;
    delete arc3;

    
    return true;
}

void SelectMethodExecuteTypeMenu::selectExecuteType(cocos2d::Ref *sender)
{
    
}

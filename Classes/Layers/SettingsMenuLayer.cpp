//
//  SettingsMenuLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Admin on 7/31/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "SettingsMenuLayer.h"
#include "utils.h"
#include "PMSettings.h"
#include "MainMenuLayer.h"
#include "PMTextureCache.h"

#include "SimpleAudioEngine.h"


USING_NS_CC;
USING_NS_CC_EXT;
using namespace CocosDenshion;
using namespace std;

Scene* SettingsMenuLayer::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    SettingsMenuLayer *layer = SettingsMenuLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer,0,0);
    
    // return the scene
    return scene;
}

bool SettingsMenuLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    
    
    
    Size screenSize = PMSettings::instance()->getScreenSize();
    
    setPosition( Point(0,0) );
    
    Sprite *backGround  = Sprite::create(PICT_PATH "Background/background1.png" );
    backGround->setPosition( Point(screenSize.width/2, screenSize.height/2) );
    resizeSprite(backGround, screenSize.width, screenSize.height);
    addChild(backGround, -1);
    
    Label *soundLabel = Label::create(LocalizedString("Sound"), PMSettings::instance()->getFont(), 26);
    
    soundLabel->setPosition( Point(screenSize.width / 2, screenSize.height / 2 + 150) );
    
    addChild(soundLabel , 1);
    
    soundOffButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("soundOff", NORMAL_STATE),
                                            PMTextureCache::instanse()->getIconSprite("soundOff", SELECTED_STATE),
                                            PMTextureCache::instanse()->getIconSprite("soundOff", DISABLED_STATE),
                                            CC_CALLBACK_1(SettingsMenuLayer::toggleSound, this));
    
    soundOnButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("soundOn", NORMAL_STATE),
                                           PMTextureCache::instanse()->getIconSprite("soundOn", SELECTED_STATE),
                                           PMTextureCache::instanse()->getIconSprite("soundOn", DISABLED_STATE),
                                           CC_CALLBACK_1(SettingsMenuLayer::toggleSound, this));
    
    soundOffButton->setPosition( Point::ZERO );
    soundOnButton->setPosition( Point::ZERO );
    
    soundOffButton->setVisible(!PMSettings::instance()->getSoundEnabled());
    soundOnButton->setVisible(PMSettings::instance()->getSoundEnabled());
    
    Menu *soundMenu = Menu::create(soundOffButton, soundOnButton, nullptr);
    
    soundMenu->setPosition( Point(screenSize.width / 2 + soundLabel->getContentSize().width, screenSize.height / 2 + 145) );
    addChild(soundMenu , 1);
    
    Label *backgroundVolumeLabel = Label::create(LocalizedString("BackgroundVolume"), PMSettings::instance()->getFont(), 20);
    Label *effectVolumeLabel = Label::create(LocalizedString("EffectVolume"), PMSettings::instance()->getFont(), 20);
    
    backgroundVolumeLabel->setAnchorPoint( Point::ZERO );
    effectVolumeLabel->setAnchorPoint( Point::ZERO );
    
    backgroundSlider = ControlSlider::create(PICT_PATH "System/sliderTrack.png",
                                               PICT_PATH "System/sliderProgress.png",
                                               PICT_PATH "System/sliderThumb.png");
    
    backgroundSlider->addTargetWithActionForControlEvents(this, cccontrol_selector(SettingsMenuLayer::backgroundVolumeChanged),
                                                          Control::EventType::VALUE_CHANGED);
    
    backgroundSlider->setAnchorPoint( Point::ZERO );
    backgroundSlider->setMaximumAllowedValue( 1.0 );
    backgroundSlider->setMinimumAllowedValue( 0.0 );
    backgroundSlider->setValue(PMSettings::instance()->getBackgroundVolume());
    
    effectSlider = ControlSlider::create(PICT_PATH "System/sliderTrack.png",
                                           PICT_PATH "System/sliderProgress.png",
                                           PICT_PATH "System/sliderThumb.png");
    
    effectSlider->addTargetWithActionForControlEvents(this, cccontrol_selector(SettingsMenuLayer::effectVolumeChanged),
                                                      Control::EventType::VALUE_CHANGED);
    
    effectSlider->setAnchorPoint( Point::ZERO );
    effectSlider->setMaximumAllowedValue( 1.0 );
    effectSlider->setMinimumAllowedValue( 0.0 );
    effectSlider->setValue(PMSettings::instance()->getEffectVolume());
    
    
    backgroundVolumeLabel->setPosition(Point(screenSize.width / 2 - backgroundVolumeLabel->getContentSize().width, screenSize.height / 2 + 100));
    backgroundSlider->setPosition( Point(screenSize.width / 2 + 20 , screenSize.height / 2 + 96) );
    effectVolumeLabel->setPosition(Point(screenSize.width / 2 - effectVolumeLabel->getContentSize().width, screenSize.height / 2 + 50));
    effectSlider->setPosition( Point(screenSize.width / 2 + 20, screenSize.height / 2 + 44) );
    
    addChild(backgroundVolumeLabel);
    addChild(backgroundSlider);
    addChild(effectVolumeLabel);
    addChild(effectSlider);
    
    Label *languageLabel = Label::create(LocalizedString("Language"), PMSettings::instance()->getFont(), 26);
    
    languageLabel->setPosition( Point(screenSize.width / 2, screenSize.height / 2 ) );
    addChild(languageLabel , 1);
    
    MenuItemSprite *russianLanguage = MenuItemImage::create(PICT_PATH "Languages/russian_flag.png",
                                                                 PICT_PATH "Languages/russian_flag_Selected.png", string(),
                                                                 CC_CALLBACK_1(SettingsMenuLayer::selectLanguage, this));
    
    MenuItemSprite *usaLanguage = MenuItemImage::create(PICT_PATH "Languages/usa_flag.png",
                                                                PICT_PATH "Languages/usa_flag_Selected.png", string(),
                                                                CC_CALLBACK_1(SettingsMenuLayer::selectLanguage, this));
    
    Menu *languageMenu = Menu::create(russianLanguage, usaLanguage, nullptr);
    
    russianLanguage->setTag((int)LanguageType::RUSSIAN);
    russianLanguage->setPosition(Point::ZERO);
    russianLanguage->setAnchorPoint(Point::ZERO);
    
    usaLanguage->setTag((int)(int)LanguageType::ENGLISH);
    usaLanguage->setPosition( Point(russianLanguage->getContentSize().width + 10,0) );
    usaLanguage->setAnchorPoint(Point::ZERO);
    
    languageMenu->setContentSize(Size(2*russianLanguage->getContentSize().width, russianLanguage->getContentSize().height));
    languageMenu->setPosition( Point(screenSize.width / 2 - languageMenu->getContentSize().width/2,
                                   screenSize.height / 2 - 25 - languageMenu->getContentSize().height) );
    addChild(languageMenu, 1);
    
    Menu *navigationMenu = Menu::create(nullptr);

    MenuItemFont *goToMain = MenuItemFont::create(LocalizedString("MainMenu"),
                                                  CC_CALLBACK_1(SettingsMenuLayer::mainMenu, this));
    goToMain->setPosition( Point(170, screenSize.height - 25) );
    
    navigationMenu->addChild(goToMain);
    navigationMenu->setPosition( Point::ZERO );
    addChild(navigationMenu, 1);
    
    cocos2d::extension::Scale9Sprite* EditBoxSprite = cocos2d::extension::Scale9Sprite::create("System/scalesprite.png");
    
    Label * LoginLabel = Label::create("Логин", PMSettings::instance()->getFont(), 24);
    LoginLabel->setAnchorPoint(Point(1,0.5));
    LoginLabel->setPosition(Point(screenSize.width / 2 - 10, screenSize.height / 2 - 100
                                ));
    addChild(LoginLabel,10);
    
    username = cocos2d::extension::EditBox::create(Size(200, 40), EditBoxSprite);
    username->setFontColor(Color3B(0, 0, 0));
    username->setText(UserDefault::getInstance()->getStringForKey("Username").c_str());
    username->setAnchorPoint(Point(0,0.5));
    username->setPosition(Point(screenSize.width / 2 + 10, screenSize.height / 2 - 100));
    addChild(username,10);

    return true;
}

void SettingsMenuLayer::backgroundVolumeChanged(cocos2d::Ref *sender, Control::EventType controlEvent)
{
    PMSettings::instance()->setBackgroundVolume(backgroundSlider->getValue());
    SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(backgroundSlider->getValue());
}

void SettingsMenuLayer::effectVolumeChanged(cocos2d::Ref *sender, Control::EventType controlEvent)
{
    PMSettings::instance()->setEffectVolume(effectSlider->getValue());
    SimpleAudioEngine::getInstance()->setEffectsVolume(effectSlider->getValue());
}

void SettingsMenuLayer::selectLanguage(cocos2d::Ref *sender)
{
    ResetLanguage();
    
    PMSettings::instance()->setLanguage( ((Node *)sender)->getTag() );
    
    Director::getInstance()->replaceScene(SettingsMenuLayer::scene());
}

void SettingsMenuLayer::toggleSound(cocos2d::Ref *object)
{
    PMSettings::instance()->setSoundEnabled(!PMSettings::instance()->getSoundEnabled());
    
    soundOffButton->setVisible(!PMSettings::instance()->getSoundEnabled());
    soundOnButton->setVisible(PMSettings::instance()->getSoundEnabled());
    
    if(!PMSettings::instance()->getSoundEnabled())
        SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    else
        SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}

void SettingsMenuLayer::mainMenu(cocos2d::Ref *object)
{
    UserDefault::getInstance()->setStringForKey("Username", username->getText());
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, MainMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

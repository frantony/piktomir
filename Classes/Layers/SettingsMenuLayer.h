//
//  SettingsMenuLayer.h
//  pictomir-cocos2d-x
//
//  Created by Admin on 7/31/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__SettingsMenuLayer__
#define __pictomir_cocos2d_x__SettingsMenuLayer__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"


class SettingsMenuLayer : public cocos2d::Layer
{
public:
    virtual bool init();
    
    CREATE_FUNC(SettingsMenuLayer);
    
    ~SettingsMenuLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    static cocos2d::Scene* scene();
private:
    
    cocos2d::extension::EditBox *username;
    
    cocos2d::extension::ControlSlider *backgroundSlider;
    cocos2d::extension::ControlSlider *effectSlider;
    
    cocos2d::MenuItemSprite *soundOffButton;
    cocos2d::MenuItemSprite *soundOnButton;
    
    void backgroundVolumeChanged(cocos2d::Ref *sender, cocos2d::extension::Control::EventType controlEvent);
    void effectVolumeChanged(cocos2d::Ref *sender, cocos2d::extension::Control::EventType controlEvent);
    void toggleSound(cocos2d::Ref *object);
    
    void selectLanguage(cocos2d::Ref *sender);
    
    void mainMenu(cocos2d::Ref *sender);
    
};

#endif /* defined(__pictomir_cocos2d_x__SettingsMenuLayer__) */

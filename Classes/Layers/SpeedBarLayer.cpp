//
//  SpeedBarLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "SpeedBarLayer.h"

USING_NS_CC;

bool SpeedBarLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

    Sprite *speedBack = Sprite::create(PICT_PATH "System/speedBar_Empty.png");
    speedBack->setPosition( Point::ZERO );
    speedBack->setAnchorPoint( Point::ZERO );
    
    addChild(speedBack, 0);
    
    double velocity = PMSettings::instance()->getAnimationSpeed();
    
    Texture2D *speedTexture = Director::getInstance()->getTextureCache()->addImage(PICT_PATH "System/speedBar.png");
    
    float width  = speedTexture->getContentSize().width * (PMSettings::instance()->getMaxAnimationSpeed() - velocity);
    float height = speedTexture->getContentSize().height;
    
    speed = Sprite::createWithTexture(speedTexture, Rect(0.0, 0.0, width, height) );
    speed->setPosition( Point::ZERO );
    speed->setAnchorPoint( Point::ZERO );
    
    addChild(speed, 1);

    setContentSize( speedBack->getContentSize() );

    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(SpeedBarLayer::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(SpeedBarLayer::touchEnded, this);
    listener->onTouchMoved = CC_CALLBACK_2(SpeedBarLayer::touchMoved, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

void SpeedBarLayer::fast(cocos2d::Ref *sender)
{
    double velocity = PMSettings::instance()->getAnimationSpeed();
    double maxVelocity = PMSettings::instance()->getMaxAnimationSpeed();
    double minVelocity = PMSettings::instance()->getMinAnimationSpeed();
    velocity-=0.05;
    if (velocity < minVelocity) {
        velocity = minVelocity;
    }
    PMSettings::instance()->setAnimationSpeed(velocity);
    
    double ratio = (maxVelocity - velocity) /(maxVelocity - minVelocity);
    speed->setTextureRect( Rect(0.0, 0.0, 100.0 * ratio, 10.0) );
    speed->setPosition(Point( 19 + 50.0 * ratio, PMSettings::instance()->getScreenSize().height - 9));
}

void SpeedBarLayer::slow(cocos2d::Ref *sender)
{
    double velocity = PMSettings::instance()->getAnimationSpeed();
    double maxVelocity = PMSettings::instance()->getMaxAnimationSpeed();
    double minVelocity = PMSettings::instance()->getMinAnimationSpeed();
    velocity+=0.05;
    if (velocity > maxVelocity) {
        velocity = maxVelocity;
    }
    PMSettings::instance()->setAnimationSpeed(velocity);
    
    double ratio = (maxVelocity - velocity) /(maxVelocity - minVelocity);
    speed->setTextureRect( Rect(0.0, 0.0, 100.0 * ratio, 10.0) );
    speed->setPosition(Point( 19 + 50.0 * ratio, PMSettings::instance()->getScreenSize().height - 9));
    
}


bool SpeedBarLayer::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(!enabled)
        return false;
    
    Point touchPoint = this->convertTouchToNodeSpace(touch);
    
    if(boundingBox().containsPoint(getParent()->convertTouchToNodeSpace(touch)) )
    {
        float velocity = PMSettings::instance()->getMaxAnimationSpeed() - touchPoint.x / getContentSize().width;
        
        if(touchPoint.x / getContentSize().width >= PMSettings::instance()->getMinAnimationSpeed())
        {
            float width  = touchPoint.x;
            float height = getContentSize().height;
            
            speed->setTextureRect( Rect(0, 0, width, height));
            
            PMSettings::instance()->setAnimationSpeed(velocity);
            

        }
        return true;
    }
    
    return false;
}


void SpeedBarLayer::touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    Point touchPoint = this->convertTouchToNodeSpace(touch);
 
    if(touchPoint.x < 0 || touchPoint.x > getContentSize().width)
        return;
    
    float velocity = PMSettings::instance()->getMaxAnimationSpeed() - touchPoint.x / getContentSize().width;
    
    if(touchPoint.x / getContentSize().width >= PMSettings::instance()->getMinAnimationSpeed())
    {
        float width  = touchPoint.x;
        float height = getContentSize().height;
        
        speed->setTextureRect( Rect(0, 0, width, height));
        
        PMSettings::instance()->setAnimationSpeed(velocity);
        
        
    }
    
}

void SpeedBarLayer::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    Point touchPoint = this->convertTouchToNodeSpace(touch);
 
    if(touchPoint.x < 0 || touchPoint.x > getContentSize().width)
        return;
    
    float velocity = PMSettings::instance()->getMaxAnimationSpeed() - touchPoint.x / getContentSize().width;
    
    if(touchPoint.x / getContentSize().width >= PMSettings::instance()->getMinAnimationSpeed())
    {
        float width  = touchPoint.x;
        float height = getContentSize().height;
        
        speed->setTextureRect( Rect(0, 0, width, height));
        
        PMSettings::instance()->setAnimationSpeed(velocity);
        
        
    }
    
}

void SpeedBarLayer::setEnabled(bool flag)
{
    enabled = flag;
    //speedMenu->setEnabled(flag);
}


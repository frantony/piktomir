//
//  SpeedBarLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__SpeedBarLayer__
#define __pictomir_cocos2d_x__SpeedBarLayer__

#include <iostream>
#include "PMSettings.h"

class SpeedBarLayer : public cocos2d::Layer
{
public:
    
    virtual bool init();
    
    CREATE_FUNC(SpeedBarLayer);
    
    void setEnabled(bool flag);
    
    bool isEnabled()
    {
        return enabled;
    }
    
    ~SpeedBarLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
private:
    cocos2d::Sprite *speed;
    bool enabled;
    
    void fast(cocos2d::Ref *sender);
    void slow(cocos2d::Ref *sender);
    
    
    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    
};

#endif /* defined(__pictomir_cocos2d_x__SpeedBarLayer__) */

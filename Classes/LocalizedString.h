//
//  LocalizedString.h
//  SkeletonX
//
//  Created by 小苏 on 11-12-1.
//  Copyright (c) 2011年 GeekStudio. All rights reserved.
//

#ifndef _LocalizedString_h
#define _LocalizedString_h

/*get the localized string by the key, if can't get the value then return mComment
 */
std::string LocalizedString(std::string key);

void ResetLanguage();

#endif

//
//  Map.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 16.12.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#include "AbstractMap.h"
#include "ScrollLayer.h"
#include "PlayerRobot.h"
#include "AbstractWorld.h"

USING_NS_CC;

bool AbstractMap::init()
{
    if(!Layer::init())
        return false;
    
    retain();
    
    return true;
}

void AbstractMap::setLayerOpacity(int layer, int opacity)
{
    for(int i = 0; i < layers[layer]->getMapLayer()->getChildrenCount(); ++i)
        ((Sprite *)layers[layer]->getMapLayer()->getChildren().at(i))->setOpacity(opacity);
}

void AbstractMap::setEnabled(bool flag)
{
    enabled = flag;
    scroll->setActive(flag);
}

void AbstractMap::scrollToInitialPosition()
{
//    GameParams *PMSettings::instance() = PMSettings::instance();
    
//    double centerX = PMSettings::instance()->mapSize().width/2;
//    double centerY = PMSettings::instance()->mapSize().height/2;
    
    
//    for(int i = 0; i <= DISPLAYED_LAYERS/2 && (activeLayer + i) < layers.size(); ++i)
//    {
//        float x = LAYER_POSITION_X(i);
//        float y = LAYER_POSITION_Y(x + centerX, centerX, centerY) ;
//        double scale = SCALE_FUNCTION(x);
//        
//        scroll->reorderChild(layers[activeLayer + i], activeLayer + DISPLAYED_LAYERS/2 + i);
//        
//        FiniteTimeAction* moveLayer = MoveTo::create(PMSettings::instance()->getAnimationSpeed() / 2,
//                                                         Point(x + centerX,y));
//        FiniteTimeAction* scaleLayer = ScaleTo::create(PMSettings::instance()->getAnimationSpeed() / 2,
//                                                           scale);
//        
//        FiniteTimeAction* moveEnd = CallFuncN::create( this,
//                                                          callfuncN_selector(Map::update));
//        
//        setLayerOpacity(activeLayer + i, LAYER_OPACITY(x) );
//        
//        layers[activeLayer + i]->getMapLayer()->runAction( scaleLayer );
//        layers[activeLayer + i]->runAction( Sequence::create(moveLayer, moveEnd, nullptr) );
//        
//        
//    }
//    
//    for(int i = 1; i <= DISPLAYED_LAYERS/2 && activeLayer - i >= 0; ++i)
//    {
//        
//        float x =  -(LAYER_POSITION_X(i));
//        float y = LAYER_POSITION_Y(x + centerX, centerX, centerY) ;
//        double scale = SCALE_FUNCTION(x);
//        
//        scroll->reorderChild(layers[activeLayer - i], activeLayer + DISPLAYED_LAYERS/2 - i);
//        
//        FiniteTimeAction* moveLayer = MoveTo::create(SYSTEM_ANIMATION_DELAY / 2,
//                                                         Point(x + centerX,y));
//        FiniteTimeAction* scaleLayer = ScaleTo::create(SYSTEM_ANIMATION_DELAY / 2,
//                                                           scale);
//        
//        FiniteTimeAction* moveEnd = CallFuncN::create( this,
//                                                          callfuncN_selector(Map::update));
//        
//        layers[activeLayer - i]->getMapLayer()->runAction( scaleLayer );
//        layers[activeLayer - i]->runAction( Sequence::create(moveLayer, moveEnd, nullptr) );
//        
//        setLayerOpacity(activeLayer - i, LAYER_OPACITY(x) );
//        
//    }
    
    FiniteTimeAction* moveLayer = MoveTo::create(SYSTEM_ANIMATION_DELAY / 2,
                                                     Point::ZERO);
    
    FiniteTimeAction* moveEnd = CallFunc::create(CC_CALLBACK_0(AbstractMap::update, this));
    
    scroll->getParallax()->runAction( Sequence::create(moveLayer, moveEnd, nullptr) );
}

/*void Map::visit()
{
    Rect scissorRect = Rect(0, 0, PMSettings::instance()->mapSize().width, PMSettings::instance()->mapSize().height);
     
    kmGLPushMatrix();
    glEnable(GL_SCISSOR_TEST);
    
    EGLView::sharedOpenGLView()->setScissorInPoints(scissorRect.origin.x, scissorRect.origin.y,
                                                      scissorRect.size.width, scissorRect.size.height);
    
    Node::visit();
    glDisable(GL_SCISSOR_TEST);
    kmGLPopMatrix();
    
}*/
//
//  Map4Container.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/2/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__Map4Container__
#define __pictomir_cocos2d_x__Map4Container__

#include <iostream>

#include <iostream>
#include "cocos2d.h"
#include "PMSettings.h"

class MapContainer : public cocos2d::LayerColor
{
    public :
    MapContainer() :
        cocos2d::LayerColor(),
        mapLayer(nullptr)
    {}
    
    CREATE_FUNC(MapContainer);

    virtual bool init() override
    {
        if(!LayerColor::init())
            return false;
    
        cocos2d::Point center = cocos2d::Point(mapLayer->getContentSize().width/2,mapLayer->getContentSize().height/2);
    
        setScaleY(0.5);
        setAnchorPoint( cocos2d::Point(0,0) );
        setPosition(cocos2d::Point::ZERO);
        
        mapLayer->setRotation( 45.0f );
        
        mapLayer->setAnchorPoint( cocos2d::Point(0,0) );
        mapLayer->setPosition(cocos2d::Point(0, 0));
        
        center = PointApplyAffineTransform(center,mapLayer->nodeToParentTransform());
        
        mapLayer->setPosition(cocos2d::Point(-center.x,-center.y));
        
        addChild(mapLayer);
        return true;
    }

    MapContainer(cocos2d::Layer *mapLayer) :
        cocos2d::LayerColor(),
        mapLayer(mapLayer)
    {


    }
    
    ~MapContainer()
    {
        //removeAllChildrenWithCleanup(true);
    }
    
    static MapContainer* create(cocos2d::Layer *_mapLayer)
    {
        MapContainer *container = new MapContainer(_mapLayer);
        
        container->init();
        container->autorelease();
        
        return container;
    }
    
protected:
    SYNTHESIZE(cocos2d::Layer *, mapLayer, MapLayer);
public:
 
};


#endif /* defined(__pictomir_cocos2d_x__Map4Container__) */

//
//  Map4Element.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#include "MapElement.h"
#include "PMTextureCache.h"

#ifdef MAP_EDITOR
#include "MapEditorGameLayer.h"
#endif


void MapElement::resize(float scale)
{
    grassSprite->setScaleX(scale);
    grassSprite->setScaleY(scale);
}

void MapElement::setElement(int type)
{
#ifdef MAP_EDITOR
    if (MapEditorGameLayer::getInstanse()->getWorld()) {
    if (type == MapElement::BrokenGrass) {
        if (originalType != MapElement::BrokenGrass) {
            ((Map4*)MapEditorGameLayer::getInstanse()->getWorld()->getMap())->incGrassCount();
        }
    } else if (originalType == MapElement::BrokenGrass) {
        ((Map4*)MapEditorGameLayer::getInstanse()->getWorld()->getMap())->decGrassCount();
    }
    }
#endif
    
    setType((MapElement::Type)type);
    originalType = (MapElement::Type)type;
    
    grassSprite->setSpriteFrame(PMTextureCache::instanse()->getMapGrassTileFrame(type));
}

void MapElement::repair()
{
    if(type == GreenGrass)
    {
        setType(BlueGrass);
        grassSprite->setSpriteFrame(PMTextureCache::instanse()->getMapGrassTileFrame(BlueGrass));
    }
    else if(type == BrokenGrass)
    {
        setType(RepairedGrass);
        grassSprite->setSpriteFrame(PMTextureCache::instanse()->getMapGrassTileFrame(RepairedGrass));
    }
}

void MapElement::disrepair()
{
    if(type == BlueGrass)
    {
        setType(GreenGrass);
        grassSprite->setSpriteFrame(PMTextureCache::instanse()->getMapGrassTileFrame(GreenGrass));
    }
    else if(type == RepairedGrass)
    {
        setType(BrokenGrass);
        grassSprite->setSpriteFrame(PMTextureCache::instanse()->getMapGrassTileFrame(BrokenGrass));
    }
}

void MapElement::restore()
{
    robotsHere.clear();
    setElement(originalType);
}
//
//  Map4Element.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__Map4Element__
#define __pictomir_cocos2d_x__Map4Element__

#include <iostream>
#include <set>
#include "cocos2d.h"
#include "PMSettings.h"

class AbstractRobot;

class MapElement
{
public:
    
    MapElement():
        type(GreenGrass),
        originalType(GreenGrass),
        nearElements(nullptr),
        wallZOrder(0),
        grassSprite(cocos2d::Sprite::create()),
        wallSprite(nullptr)
    {
        grassSprite->retain();
    }
    
    static MapElement* create()
    {
        return new MapElement;
    }
    
    MapElement(int dimention):
        type(GreenGrass),
        originalType(GreenGrass),
        nearElements(new MapElement *[dimention]),
        wallZOrder(0),
        grassSprite(cocos2d::Sprite::create()),
        wallSprite(nullptr)
    {
        grassSprite->retain();
    }
    
    ~MapElement()
    {
        CC_SAFE_RELEASE_NULL(grassSprite);
        //CC_SAFE_DELETE(wallSprite);
    }
    
    static MapElement* create(int dimention)
    {
        return new MapElement(dimention);
    }
    
    
    enum Type {
        None = -1,
        GreenGrass,//0
        BlueGrass,//1
        BrokenGrass,//2
        RepairedGrass//3
    };

    SYNTHESIZE(Type, type, Type);
    SYNTHESIZE(int, wallZOrder, WallZOrder);
    
    MapElement **nearElements;
    
    SYNTHESIZE(cocos2d::Sprite *, grassSprite, GrassSprite);
    SYNTHESIZE(cocos2d::Sprite *, wallSprite, WallSprite);
    
    void repair();
    void disrepair();
    void restore();
    
    bool isPainted()
    {
        return type == BlueGrass || type == RepairedGrass;
    }
    
    void setElement(int);
    void resize(float scale);
    
    void addRobot(AbstractRobot *robot)
    {
        robotsHere.insert(robot);
    }
    
    void removeRobot(AbstractRobot *robot)
    {
        robotsHere.erase(robot);
    }
    
    bool containsRobot(AbstractRobot *robot)
    {
        return robotsHere.find(robot) != robotsHere.end();
    }
    
    std::set<AbstractRobot *> &getRobots()
    {
        return robotsHere;
    }
    
private:
    Type originalType;
    std::set<AbstractRobot *>robotsHere;
};


#endif /* defined(__pictomir_cocos2d_x__Map4Element__) */

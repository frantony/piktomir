//
//  PMMapPreview.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 01.08.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "PMMapPreview.h"
#include "PMGameDatabase.h"
#include "utils.h"
#include "XmlWorldReader.h"
#include "AbstractWorld.h"
#include "ScrollLayer.h"

USING_NS_CC;

using namespace std;

bool PMMapPreview::init()
{
    if (!LayerColor::init()) {
        return false;
    }
    
    setContentSize(Size(PMMapPreviewWidth, PMMapPreviewHeight));
    
    setColor(Color3B::GRAY);
    setOpacity(144);
    
    PMMapPreview::world = 0;
    PMMapPreview::map = 0;

    
    Label * label = Label::create(LocalizedString("NewMap"), PMSettings::instance()->getFont(), 22.0);

    label->setAnchorPoint(Point(0.5, 0.5));
    label->setPosition(Point(PMMapPreviewWidth / 2, PMMapPreviewHeight / 2));
//    
//    Sprite * sprite = Sprite::create("System/Preview.png");
//    sprite->setAnchorPoint(Point(0.5, 0.5));
//    sprite->setPosition(Point(PMMapPreviewWidth / 2, PMMapPreviewHeight / 2));
//    
    //    addChild(sprite);
    worldClass = nullptr;
    addChild(label);
    
    return true;
}

bool PMMapPreview::initPastePreview()
{
    if (!LayerColor::init()) {
        return false;
    }
    
    setContentSize(Size(PMMapPreviewWidth, PMMapPreviewHeight));
    
    setColor(Color3B::GRAY);
    setOpacity(144);
    
    PMMapPreview::world = 0;
    PMMapPreview::map = 0;
    
    Label * label = Label::create(LocalizedString("PasteMap"), PMSettings::instance()->getFont(), 22.0);
    label->setAnchorPoint(Point(0.5, 0.5));
    label->setPosition(Point(PMMapPreviewWidth / 2, PMMapPreviewHeight / 2));

    worldClass = nullptr;
    addChild(label);
    
    return true;
}

bool PMMapPreview::initWithWorldMap(int world, int map)
{
    if (!LayerColor::init()) {
        return false;
    }
    
    setContentSize(Size(PMMapPreviewWidth, PMMapPreviewHeight));
    
    setColor(Color3B::GRAY);
    setOpacity(144);
    
    PMMapPreview::world = world;
    PMMapPreview::map = map;
    
    __String mapPathString;
    mapPathString.initWithFormat("%s", PMSettings::instance()->pathToMap(world, map).c_str());
    
    std::string mapPath = FileUtils::getInstance()->fullPathForFilename(mapPathString._string);
    
    XmlWorldReader xml;
    worldClass = xml.readWorldPreview(mapPath);
    
    if(worldClass != nullptr)
    {
        worldClass->getMap()->getScroll()->getParallax()->removeChild(worldClass->getMap()->getLayer(),true);
        
        MapContainer *mapLayer = worldClass->getMap()->getLayer();
        

        auto shape = DrawNode::create();
        static Point rect[4];
        rect[0] = Point(0, 0);
        rect[1] = Point(0, 200);
        rect[2] = Point(200, 200);
        rect[3] = Point(200, 0);
        
        static Color4F green(0, 1, 0, 1);
        shape->drawPolygon(rect, 4, green, 0, green);
        shape->setPosition( Point(0, 0) );
        
        auto clipper = ClippingNode::create();
        clipper->setAnchorPoint(Point(0, 0));
        clipper->setPosition( Point::ZERO );
        clipper->setStencil(shape);        
        clipper->addChild(mapLayer);
        
        
        addChild(clipper, 0);
        
        worldClass->getMap()->getLayer()->setScaleX(getContentSize().width /  mapLayer->getContentSize().width);
        worldClass->getMap()->getLayer()->setScaleY(getContentSize().width /  mapLayer->getContentSize().width / 2);
        
        
        mapLayer->setPosition( Point(PMMapPreviewWidth / 2, PMMapPreviewHeight / 2) );

        Label * label = Label::create((LocalizedString("Map")+ " " + to_string((PLATFORM_INT_FOR_TO_STRING)map)), PMSettings::instance()->getFont(), 22.0);

        label->setAnchorPoint(Point(0.5, 0));
        label->setPosition(Point(PMMapPreviewWidth / 2, 5));
        
        addChild(label , 1);
        
        int score = PMGameDatabase::instance()->getGameScore(world, map);
        
        if(score != -1)
        {
            Sprite *starSprite = PMTextureCache::instanse()->getIconSprite("star");
            starSprite->setAnchorPoint( Point::ZERO );
            starSprite->setPosition( Point(getContentSize().width  - starSprite->getContentSize().width, 0) );
            
            addChild(starSprite, 2);
            
            Label * scoreLabel = Label::create(StringUtils::format(LocalizedString("ScorePattern").c_str() , score),
                                           PMSettings::instance()->getFont(), 18.0);
            scoreLabel->setAnchorPoint( Point(0.5, 0) );
            scoreLabel->setPosition( Point(PMMapPreviewWidth / 2, PMMapPreviewHeight - scoreLabel->getContentSize().height - 10.0) );
            
            addChild(scoreLabel, 2);
        }
    }
    else
    {
        char errorString[255];
        
        sprintf(errorString, "Load map error!\n%s",xml.getError().c_str());
        
        Label * label = Label::create(errorString, PMSettings::instance()->getFont(), 15.0);
        label->setAnchorPoint(Point(0.5, 0.5));
        label->setPosition(Point(PMMapPreviewWidth / 2, PMMapPreviewHeight / 2));
        
        addChild(label);
    }
    
    
    return true;
}


PMMapPreview* PMMapPreview::create(int world, int map)
{
    PMMapPreview *pRet = new PMMapPreview();
    pRet->initWithWorldMap(world, map);
    pRet->autorelease();
    return pRet;
}

PMMapPreview* PMMapPreview::create()
{
    PMMapPreview *pRet = new PMMapPreview();
    pRet->init();
    pRet->autorelease();
    return pRet;
}

PMMapPreview* PMMapPreview::createPastePreview()
{
    PMMapPreview *pRet = new PMMapPreview();
    pRet->initPastePreview();
    pRet->autorelease();
    return pRet;
}

//
//  PMClient.h
//  pictomir
//
//  Created by Anton Dedkov on 30.01.14.
//
//

#ifndef __pictomir__PMClient__
#define __pictomir__PMClient__

#include <iostream>
#include <string>
#include <deque>
#include "protocol.h"
#include "enet/enet.h"
using std::string;
using std::deque;

class PMClient
{
public: 
    PMClient(int id);
    ~PMClient();
    
    int getId()
    {
        return id_;
    }
    
    bool haveProgram()
    {
        if(program_ == nullptr) return false;
        else return true;
    }
    bool readyForStart()
    {
        return readyForStart_;
    }
    
    deque<pt_header*> messageDeque_;
    _pt_robot_instr_list* program_ = nullptr;
    bool readyForStart_ = false;
    bool programsReceived_ = false;
    
    string name_;
    int id_;
    ENetPeer * peer_;
};

#endif /* defined(__pictomir__PMClient__) */

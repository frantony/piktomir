//
//  PMHost.cpp
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 14.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#include "PMHost.h"

PMHost::PMHost(enet_uint16 port)
{
    if (enet_initialize () != 0)
    {
        fprintf (stderr, "An error occurred while initializing ENet.\n");
    }
    
    host_ = new ENetHost;
    event_ = new ENetEvent;
    address_ = new ENetAddress;
    address_->host = ENET_HOST_ANY;
    address_->port = port;
    host_ = enet_host_create (address_ /* the address to bind the server host to */,
                              32      /* allow up to 32 clients and/or outgoing connections */,
                              2      /* allow up to 2 channels to be used, 0 and 1 */,
                              0      /* assume any amount of incoming bandwidth */,
                              0      /* assume any amount of outgoing bandwidth */);
    if (host_ == nullptr)
    {
        fprintf (stderr,
                 "An error occurred while trying to create an ENet server host.\n");
        exit (EXIT_FAILURE);
        
    }
    
    eventThread_ = new thread(&PMHost::networkEventHandler, this);
    

}

PMHost::~PMHost()
{
    
}

void PMHost::sendTo(const PMClient &recipient, const void *data, size_t size, bool doFlush)
{
    ENetPacket * packet = enet_packet_create (data, size, ENET_PACKET_FLAG_RELIABLE);
    enet_peer_send (recipient.peer_, 0, packet);
    if(doFlush)flush();
}

void PMHost::sendBroadcast(const void* data, size_t size, bool doFlush)
{
    ENetPacket * packet = enet_packet_create (data, size, ENET_PACKET_FLAG_RELIABLE);
    enet_host_broadcast (host_, 0, packet);
    if(doFlush)flush();
}

void PMHost::flush()
{
    enet_host_flush(host_);
}

vector<PMClient>* PMHost::getClients()
{
    return &clients_;
}

void PMHost::networkEventHandler()
{
    while(true)
    {
        if(enet_host_service (host_, event_, NETWORK_THREAD_SLEEP) > 0)
        {
            if (event_->type == ENET_EVENT_TYPE_CONNECT)
            {
                PMSession * session = new PMSession;
                PMClient* client =new PMClient(session->getId());

                client->peer_ = event_->peer;
                
                char name[32];
                enet_address_get_host(&event_->peer->address, name, 32);
                client->name_ = name;
                
                clients_.push_back(*client);
                
                _client_info * packet = new _client_info;
                packet->priority = HIGH_PRIORITY;
                packet->type = CLIENT_CONNECTED;
                packet->size = sizeof(_client_info) - sizeof(pt_header);
                packet->id   = client->getId();
                
                messageDeque_.push_back((pt_header*)packet);
                
            }
            
            if (event_->type == ENET_EVENT_TYPE_DISCONNECT)
            {
                for(std::vector<PMClient>::iterator i = clients_.begin(); i != clients_.end(); ++i)
                {
                    if(i->peer_ == event_->peer)
                    {
                        _client_info * packet = new _client_info;
                        packet->priority = HIGH_PRIORITY;
                        packet->type = CLIENT_DISCONNECTED;
                        packet->size = sizeof(_client_info) - sizeof(pt_header);
                        packet->id   = i->getId();
                        
                        messageDeque_.push_back((pt_header*)packet);
                        break;
                    }
                }
                enet_peer_reset(event_->peer);
            }
            
            if (event_->type == ENET_EVENT_TYPE_RECEIVE)
            {
                char * packet = new char[event_->packet->dataLength];
                memcpy(packet, event_->packet->data, event_->packet->dataLength);
                
                for(vector<PMClient>::iterator i = clients_.begin(); i != clients_.end(); i++)
                {
                    if(i->peer_ == event_->peer)
                    {
                        if(((pt_header*)packet)->type == CONNECTION_INFO)
                        {
                            i->name_ = ((_connection_info*)packet)->client_name_;
                        }
                        i->messageDeque_.push_back((pt_header*)packet);
                        break;
                    }
                }
            }
        }
        
    }
}

//
//  PMHostClient.cpp
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 18.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#include "PMHostClient.h"

PMHostClient::PMHostClient(ENetAddress address)
{
    if (enet_initialize () != 0)
    {
        fprintf (stderr, "An error occurred while initializing ENet.\n");
    }
    address_ = new ENetAddress;
    memcpy(address_, &address, sizeof(ENetAddress));
    event_ = new ENetEvent;
    host_ = enet_host_create (nullptr, 1, 2, 0, 0);
    if (host_ == nullptr)
    {
        fprintf (stderr, "An error occurred while trying to create an ENet client host.\n");
    }
    
    networkThread_ = new thread(&PMHostClient::networkEventHandler, this);
}

PMHostClient::~PMHostClient()
{
    
}

bool PMHostClient::isConnected()
{
    return connected_;
}

void PMHostClient::send(const void* data, size_t dataSize)
{
    ENetPacket * packet = nullptr;
    packet = enet_packet_create(data, dataSize, ENET_PACKET_FLAG_RELIABLE);
    enet_peer_send(peer_, 0, packet);
    enet_host_flush(host_);
}

void PMHostClient::networkEventHandler()
{
    while(true)
    {
        if(!connected_) connect();
        
        if(enet_host_service (host_, event_, NETWORK_THREAD_SLEEP) > 0)
        {
            if (event_->type == ENET_EVENT_TYPE_CONNECT)
            {
                connected_ = true;
                sendClientInfo();
            }
            
            if (event_->type == ENET_EVENT_TYPE_DISCONNECT)
            {
                connected_ = false;
            }
            
            if (event_->type == ENET_EVENT_TYPE_RECEIVE)
            {
                char* data = new char[event_->packet->dataLength];
                memcpy(data, event_->packet->data, event_->packet->dataLength);
                if(((pt_header*)data)->priority == HIGH_PRIORITY)
                {
                    messageDeque_.push_front((pt_header*)data);
                }
                else
                {
                    messageDeque_.push_back((pt_header*)data);
                }
            }
        }
    }
}

void PMHostClient::connect()
{
    connected_ = true;
    if(peer_ != nullptr) enet_peer_reset(peer_);
    peer_ = enet_host_connect (host_, address_, 2, 0);
    
    if (peer_ == nullptr)
    {
        fprintf (stderr, "No available peers for initiating an ENet connection.\n");
    }
}

void PMHostClient::sendClientInfo()
{
    _connection_info info;
    strcpy(info.client_name_, cocos2d::UserDefault::getInstance()->getStringForKey("Username").c_str());
    
    info.type = CONNECTION_INFO;
    info.priority = HIGH_PRIORITY;
    info.size = sizeof(_connection_info) - sizeof(pt_header);
    
    send(&info, sizeof(_connection_info));
}
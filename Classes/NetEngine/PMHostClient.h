//
//  PMHostClient.h
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 18.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PMHostClient__
#define __pictomir_cocos2d_x__PMHostClient__

#include <iostream>
#include "PMHost.h"

class PMHostClient
{
public:
    PMHostClient(ENetAddress address);
    ~PMHostClient();
    
    bool isConnected();
    
    deque<pt_header*> messageDeque_;
    
    void send(const void* data, size_t dataSize);
    
private:
    
    void networkEventHandler();
    void connect();
    
    void sendClientInfo();
    
    thread * networkThread_;
    
    ENetHost * host_ = nullptr;
    ENetEvent * event_ = nullptr;
    ENetPeer * peer_ = nullptr;
    ENetAddress * address_ = nullptr;
    
    bool connected_ = false;
    
};

#endif /* defined(__pictomir_cocos2d_x__PMHostClient__) */

//
//  PMManager.cpp
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 25.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "PMManager.h"
#include "protocol.h"

#include "GameLayer.h"

PMManager *PMManager::_self = 0;

PMManager::PMManager()
{
    this->cocos2d::Node::init();
    
    enable();
    
    numberOfSolutions_ = 0;
    
    this->retain();
    resume();
}

PMManager::~PMManager()
{
    MessageBox("Control lost.", "Piktomir Manager");
    enet_deinitialize();
}

void PMManager::messageHandler(float dt)
{
    if(host_ != nullptr && host_->messageDeque_.size() > 0)
    {
        pt_header * packet = host_->messageDeque_.front();
        
        switch (packet->type)
        {
            case INIT_SINGLEPLAYER_GAME:
                initSingleGame((_init_singleplayer_game_sr_cl*)packet);
                break;
            case INIT_MULTIPLAYER_GAME:
            {
                /*PMMultiplayerHost * mpHost = */new PMMultiplayerHost((*(_init_multiplayer_game_sr_cl*)packet));
                
                break;
            }
            case CONNECT_MULTIPLAYER_GAME:
            {
                multiplayer_host_ = new PMHostClient(((_connect_multiplayer_game_sr_cl*)packet)->address);
                robot_ = ((_connect_multiplayer_game_sr_cl*)packet)->robot; 
                break;
            }
            default:
                break;
        }
        host_->messageDeque_.pop_front();
    }
    if(multiplayer_host_ != nullptr && multiplayer_host_->messageDeque_.size() > 0)
    {
        pt_header * packet = multiplayer_host_->messageDeque_.front();
        
        switch (packet->type)
        {
            case MULTIPLAYER_CONFIRMATION:
                
                numberOfRobots_ = ((_multiplayer_confirmation*)packet)->numberOfRobots;
                solutions = new _pt_robot_instr_list[numberOfRobots_];
                
                startMultiplayerGame((_multiplayer_confirmation*)packet);
                
                break;
                
            case MULTIPLAYER_PROGRAM:
                
                memcpy(&solutions[numberOfSolutions_], (_pt_robot_instr_list*)packet, sizeof(_pt_robot_instr_list));
                PMSettings::instance()->getGameLayer()->getWorld()->robotAt(((_pt_robot_instr_list*)packet)->robotIndex)->setMethodListFromNet(((_pt_robot_instr_list*)packet));
                CCLOG("programm");
                
                numberOfSolutions_ ++;
                
                if(numberOfSolutions_ == numberOfRobots_)
                    sendProgramsReceived();
                //if(numberOfSolutions_ > numberOfRobots_) numberOfSolutions_ -= numberOfRobots_;
                
                break;
                
            case MULTIPLAYER_RUN:
                multiplayerRun();
                CCLOG("Run multiplayer");
                
                
                break;
                
            default:
                break;
        }
        
        
        multiplayer_host_->messageDeque_.pop_front();
    }

}

void PMManager::disable()
{
    Node::unschedule(schedule_selector(PMManager::messageHandler));
    delete host_;
}

void PMManager::enable()
{
    ENetAddress teacher_address;
    teacher_address.host = ENET_HOST_BROADCAST;
    teacher_address.port = PM_SERVICE_PORT;
    host_ = new PMHostClient(teacher_address);
    schedule(schedule_selector(PMManager::messageHandler), 0.1f);
}

void PMManager::initSingleGame(_init_singleplayer_game_sr_cl *packet)
{
    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, GameLayer::scene(false,packet->world,packet->level + 1));
    Director::getInstance()->replaceScene(trans);
}

void PMManager::startMultiplayerGame(_multiplayer_confirmation* packet)
{
    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, GameLayer::scene(false, packet->world,packet->level + 1, robot_));
    Director::getInstance()->replaceScene(trans);
}

void PMManager::sendSolution(_pt_robot_instr_list *packet)
{
    if(multiplayer_host_ != nullptr)
    {
        packet->size = sizeof(_pt_robot_instr_list) - sizeof(pt_header);
        packet->type = MULTIPLAYER_PROGRAM;
        multiplayer_host_->send((void*)packet, sizeof(_pt_robot_instr_list));
    }
}

void PMManager::sendReadyToStart()
{
    pt_header* packet = new pt_header;
    packet->priority = HIGH_PRIORITY;
    packet->size = 0;
    packet->type = MULTIPLAYER_READY_TO_PLAY;
    
    multiplayer_host_->send(packet, sizeof(pt_header));
}

void PMManager::sendProgramsReceived()
{
    pt_header* packet = new pt_header;
    packet->priority = HIGH_PRIORITY;
    packet->size = 0;
    packet->type = MULTIPLAYER_PROGRAMS_RECEIVED;
    
    multiplayer_host_->send(packet, sizeof(pt_header));
}

void PMManager::multiplayerRun()
{
    for(int i = 0 ; i < numberOfSolutions_; ++i)
    {
        CCLOG("RobotIndex: %d", solutions[i].robotIndex);
    }
    PMSettings::instance()->getGameLayer()->startGame();
}
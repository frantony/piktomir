//
//  PMManager.h
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 25.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PMManager__
#define __pictomir_cocos2d_x__PMManager__

#include <iostream>
#include <cocos2d.h>
#include "Singleton.h"
#include <deque>
#include <thread>
#include "PMHost.h"
#include "PMHostClient.h"
#include "PMMultiplayerHost.h"
using namespace cocos2d;

class PMManager :public Node
{
public:
    static PMManager *_self;
    
    static PMManager * instance()
    {
        if(!_self)
            _self = new PMManager();
        
        return _self;
    }
    
    PMManager();
    ~PMManager();
    
    void disable();
    void enable();
    
    void sendSolution(_pt_robot_instr_list *packet);
    void sendReadyToStart();
    
private:
    
    void messageHandler(float dt);
    
    unsigned short numberOfRobots_;
    unsigned short numberOfSolutions_;
    
    short robot_;
    
    PMHostClient * host_ = nullptr;
    PMHostClient * multiplayer_host_ = nullptr;
    
    _pt_robot_instr_list* solutions = nullptr;
    
    bool multiplayer_enabled_ = false;
    
    void initSingleGame(_init_singleplayer_game_sr_cl* packet);
    void startMultiplayerGame(_multiplayer_confirmation* packet);
    void multiplayerRun();
    void sendProgramsReceived();
};

#endif /* defined(__pictomir_cocos2d_x__PMManager__) */

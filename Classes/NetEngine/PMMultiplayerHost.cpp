//
//  PMMultiplayerHost.cpp
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 18.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#include "PMMultiplayerHost.h"

PMMultiplayerHost::PMMultiplayerHost(_init_multiplayer_game_sr_cl initPacket):PMHost(PM_MULTIPLAYER_PORT)
{
    clientsNeeded_ = initPacket.clientNeeded;
    world_ = initPacket.world;
    level_ = initPacket.level;
    
    messageThread_ = new thread(&PMMultiplayerHost::messageHandler, this);
    
}

PMMultiplayerHost::~PMMultiplayerHost()
{
    
}

void PMMultiplayerHost::messageHandler()
{
    while(true)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(MESSAGE_THREAD_SLEEP));
        
        if(!messageDeque_.empty())
        {
            pt_header * packet = messageDeque_.front();
            
            if(!confirmation_sent_)
            {
                switch (packet->type)
                {
                    case CLIENT_CONNECTED:
                        
                        clientsConnected_++;
                        if(clientsConnected_ == clientsNeeded_)
                        {
                            sendMultiplayerConfirmation();
                            confirmation_sent_ = true;
                        }
                        
                        break;
                        
                    case CLIENT_DISCONNECTED:
                        
                        clientsConnected_--;
                        
                        break;
                        
                    default:
                        break;
                }
                
            }
            
            messageDeque_.pop_front();
        }
        
        for(auto &i: clients_)
        {
            if(!i.messageDeque_.empty())
            {
                pt_header* packet = i.messageDeque_.front();
                i.messageDeque_.pop_front();
                
                switch (packet->type)
                {
                    case MULTIPLAYER_PROGRAM:
                    {
                        _pt_robot_instr_list * program = new _pt_robot_instr_list;
                        memcpy(program, packet, sizeof(_pt_robot_instr_list));
                        
                        i.program_ = program;
                        
                        CCLOG("Host: Program");
                        
                        break;
                    }
                        
                    case MULTIPLAYER_READY_TO_PLAY:
                    {
                        i.readyForStart_ = true;
                        CCLOG("Host: ready to play");
                        
                        break;
                    }
                    
                    case MULTIPLAYER_PROGRAMS_RECEIVED:
                    {
                        i.programsReceived_ = true;
                        CCLOG("Host: Client all prog");
                        
                        break;
                    }
                        
                    default:
                        break;
                }
            }
        }
        
        if(!program_sent && clients_.size() > 0)
        {
            bool ok = true;
            for(auto &i: clients_)
            {
                if(i.program_ == nullptr) ok = false;
                if(i.readyForStart_ == false) ok = false;
            }
            if(ok)
            {
                for(vector<PMClient>::iterator i = clients_.begin(); i != clients_.end(); i++)
                {
                    i->program_->type = MULTIPLAYER_PROGRAM;
                    sendBroadcast((void*)i->program_, sizeof(_pt_robot_instr_list), true);
                }
                program_sent = true;
            }
        }
        if(!run_sent_ && program_sent)
        {
            bool ok = true;
            for(auto &i: clients_)
            {
                if(i.programsReceived_ == false) ok = false;
            }
            if(ok)
            {
                sendRunMultiplayer();
                run_sent_ = true;
            }
        }
    }
}

void PMMultiplayerHost::sendMultiplayerConfirmation()
{
    _multiplayer_confirmation* packet = new _multiplayer_confirmation;
    packet->type = MULTIPLAYER_CONFIRMATION;
    packet->priority = HIGH_PRIORITY;
    packet->size = sizeof(_multiplayer_confirmation) - sizeof(pt_header);
    packet->numberOfRobots = clientsNeeded_;
    packet->level = level_;
    packet->world = world_;
    
    sendBroadcast((void*)packet, sizeof(_multiplayer_confirmation));

}

void PMMultiplayerHost::sendRunMultiplayer()
{
    pt_header* packet = new pt_header;
    packet->type = MULTIPLAYER_RUN;
    packet->priority = HIGH_PRIORITY;
    packet->size = 0;
    
    sendBroadcast((void*)packet, sizeof(pt_header));
}
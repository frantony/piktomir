//
//  PMMultiplayerHost.h
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 18.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PMMultiplayerHost__
#define __pictomir_cocos2d_x__PMMultiplayerHost__

#include <iostream>
#include "PMHost.h"

class PMMultiplayerHost: public PMHost
{
public:
    PMMultiplayerHost(_init_multiplayer_game_sr_cl initPacket);
    ~PMMultiplayerHost();
    
    void messageHandler();
    
private:

    short world_;
    short level_;
    
    void sendMultiplayerConfirmation();
    void sendRunMultiplayer();
    
    thread * messageThread_;
    
    short clientsConnected_ = 0;
    short clientsNeeded_;
    
    bool run_sent_ = false;
    bool program_sent = false;
    bool confirmation_sent_ = false;
};


#endif /* defined(__pictomir_cocos2d_x__PMMultiplayerHost__) */

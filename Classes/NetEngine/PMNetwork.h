//
//  PMNetwork.h
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 14.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#ifndef pictomir_cocos2d_x_PMNetwork_h
#define pictomir_cocos2d_x_PMNetwork_h

#include "PMHost.h"

#define PM_SERVICE_PORT 18841
#define PM_MULTIPLAYER_PORT 18842
#define NETWORK_THREAD_SLEEP 50
#define MESSAGE_THREAD_SLEEP 50

#endif

//
//  PMSession.cpp
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 18.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#include "PMSession.h"

unsigned int PMSession::allocated_ = 0;

PMSession::PMSession()
{
    id_ = allocated_;
    allocated_++;
}

PMSession::~PMSession()
{
    
}

unsigned int PMSession::getId()
{
    return id_;
}
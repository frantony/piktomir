//
//  PMSession.h
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 18.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PMSession__
#define __pictomir_cocos2d_x__PMSession__

#include <iostream>

class PMSession
{
public:
    PMSession();
    ~PMSession();
    
    unsigned int getId();
    
private:
    unsigned int id_;
    
    static unsigned int allocated_;
};

#endif /* defined(__pictomir_cocos2d_x__PMSession__) */

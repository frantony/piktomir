//
//  protocol.h
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 02.10.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef pictomir_cocos2d_x_protocol_h
#define pictomir_cocos2d_x_protocol_h

#include "RobotMethod.h"
#include "enet/enet.h"

enum priority
{
    HIGH_PRIORITY,
    LOW_PRIORITY
};

enum type
{
    INIT_SINGLEPLAYER_GAME,
    INIT_MULTIPLAYER_GAME,
    CONNECT_MULTIPLAYER_GAME,
    CLIENT_CONNECTED,
    CONNECTION_INFO,
    CLIENT_DISCONNECTED,
    CLIENTS_READY,
    MULTIPLAYER_PROGRAM,
    MULTIPLAYER_RUN,
    MULTIPLAYER_READY_TO_PLAY,
    MULTIPLAYER_CONFIRMATION,
    MULTIPLAYER_PROGRAMS_RECEIVED
};

struct pt_header
{
    char priority;
    char type;
    
    short size;
};

struct _init_singleplayer_game_sr_cl: public pt_header
{
    short world;
    short level;
};

struct _init_multiplayer_game_sr_cl: public pt_header
{
    short world;
    short level;
    short clientNeeded;
};

struct _connect_multiplayer_game_sr_cl: public pt_header
{
    short world;
    short level;
    short robot;
    
    ENetAddress address;
};

struct _client_info: public pt_header
{
    int id;
};

struct _connection_info: public pt_header
{
    char client_name_[32];
};

struct _multiplayer_confirmation: public pt_header
{
    short world;
    short level;
    unsigned short numberOfRobots;
};

struct _pt_robot_instr_list: public pt_header
{
    unsigned short robotIndex;
    
    struct _program
    {
        short ID;
        
        char useRepeater;
        char repeater;
        char useCondition;
        char condition;
        
        unsigned int methodNum;
        
        int methods[64];
        unsigned int execType[64];
    };
    
    unsigned short programNum;
    _program programs[5];
}; // sizeof(pt_robot_instr_list) == 1024

#endif

//
//  PMGameDatabase.cpp
//  Piktomir
//
//  Created by Бесшапошников Никита on 04.04.14.
//
//

#include "PMGameDatabase.h"

USING_NS_CC;
using namespace std;

int PMGameDatabase::getGameScore(int worldPart, int level)
{
    string value = localStorageGetItem(StringUtils::format("%d-%d", worldPart, level));
    
    
    if(value != "")
        return atoi(value.c_str());
    else
        return -1;
}

void PMGameDatabase::setGameScore(int worldPart, int level, int score)
{
    localStorageSetItem(StringUtils::format("%d-%d", worldPart, level), to_string(score));
}
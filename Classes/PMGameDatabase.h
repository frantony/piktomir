//
//  PMGameDatabase.h
//  Piktomir
//
//  Created by Бесшапошников Никита on 04.04.14.
//
//

#ifndef __Piktomir__PMGameDatabase__
#define __Piktomir__PMGameDatabase__

#include <iostream>
#include "cocos2d.h"
#include "PMSettings.h"
#include "Singleton.h"
#include "LocalStorage.h"

class PMGameDatabase : public Singleton<PMGameDatabase>
{
public:
    PMGameDatabase()
    {
        localStorageInit(PMSettings::instance()->getDatabaseFilePath());
    }
    
    ~PMGameDatabase()
    {
        localStorageFree();
    }

    int getGameScore(int worldPart, int level);
    void setGameScore(int worldPart, int level, int score);
};

#endif /* defined(__Piktomir__PMGameDatabase__) */

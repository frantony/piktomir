//
//  GameParams.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#include "PMSettings.h"

USING_NS_CC;

using namespace std;
using namespace tinyxml2;

void PMSettings::loadWorlds()
{
    mapsInfo.clear();
    netMapsInfo.clear();
    
    
	ssize_t size;

	char *buf = (char *)FileUtils::getInstance()->getFileData(pathToWorlds(true), "r", &size);

	tinyxml2::XMLDocument doc;
    doc.Parse(buf,size);
    
    delete buf;
  
	if(doc.Error())
	{
		MessageBox(doc.GetErrorStr1(), "1");
		return;
	}
    
    XMLElement *root = doc.FirstChildElement("worlds");
    XMLElement *curNode = root->FirstChildElement();

    for (; curNode; curNode = curNode->NextSiblingElement())
    {
        mapsInfo.push_back(_local_world_info { curNode->UnsignedAttribute("count") });
    }
    
    
    
    /////////////////________________NETGAME_____________________
    
    

    buf = (char *)FileUtils::getInstance()->getFileData(pathToWorlds(false), "r", &size);
    
    doc.Parse(buf,size);
    
    delete buf;
    
	if(doc.Error())
	{
		MessageBox(doc.GetErrorStr1(), "1");
		return;
	}
    
    root = doc.FirstChildElement("worlds"); /////////// worlds.xml
    int cnt = 0;
    
    for (XMLElement *curNode = root->FirstChildElement(); curNode; curNode = curNode->NextSiblingElement())
    {
        _net_world_info netMapInfo;
        netMapInfo.levelNum = curNode->UnsignedAttribute("count"); ////количество карт
        
        buf = (char *)FileUtils::getInstance()->getFileData(pathToWorld(cnt, false) + "/world.xml", "r", &size); ///роботы
        
		tinyxml2::XMLDocument worldDoc;
        worldDoc.Parse(buf,size);
        
        delete buf;
        
        root = worldDoc.FirstChildElement("world");
        
        for (XMLElement *node = root->FirstChildElement(); node; node = node->NextSiblingElement())
        {
            netMapInfo.levelInfo.push_back(_net_world_info::_level_info { node->UnsignedAttribute("count") } );
        }
        
        netMapsInfo.push_back(netMapInfo);
        cnt++;
    }
}

//
//  PMSettings.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__GameParams__
#define __pictomir_cocos2d_x__GameParams__

//#include <iostream>
#include "cocos2d.h"
#include <string>
#include <stdio.h>
#include "tinyxml2.h"

#include "Singleton.h"

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
    #include <direct.h>
#endif
#include "LocalizedString.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	#define PICT_PATH ""
#endif // CC_TARGET_PLATFORM == CC_PLATFORM_WIN32

#if (CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	#define PICT_PATH ""
#endif // CC_TARGET_PLATFORM == CC_PLATFORM_MAC&&IOS

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define PICT_PATH "Picts/"
#endif // CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#if (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
#define PICT_PATH "Picts/"
#endif // CC_TARGET_PLATFORM == CC_PLATFORM_LINUX


#define SYNTHESIZE_GAMEPARAM(varType, varName, funName) \
protected: varType varName; \
public: varType get##funName(void) const { return varName; } \
public: void set##funName(varType var) { \
    varName = var; \
    set##varType##ValueForKey(#varName, varName); \
    }

#define SYNTHESIZE(varType, varName, funName)\
protected: varType varName;\
public: varType get##funName(void) const { return varName; }\
public: void set##funName(varType var){ varName = var; }

#define SYNTHESIZE_D(varType, varName, funName, defaultValue)\
protected: varType varName = defaultValue;\
public: varType get##funName(void) const { return varName; }\
public: void set##funName(varType var){ varName = var; }


#define NodeContent(Node) (char*)xmlNodeGetContent(Node->children)
#define NodeProp(Node,Prop) (char*)xmlGetProp(Node, (xmlChar*)Prop)
#define getMethodLayers PMSettings::instance()->getWorld()->layer->getProgramLayer()->layers

#if (CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define writablePath (cocos2d::FileUtils::getInstance()->getWritablePath()+"Piktomir/")
#else
#define writablePath cocos2d::FileUtils::getInstance()->getWritablePath()
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define PLATFORM_INT_FOR_TO_STRING long long
#define platform_mkdir(path) _mkdir(path)
#else
#define PLATFORM_INT_FOR_TO_STRING int
#define platform_mkdir(path) mkdir(path, S_IRWXU)
#endif

const float SYSTEM_ANIMATION_DELAY = 0.4f;

const int REPEATER_BUTTON_NUM = 8;

const int MaxRepeater = 1000;

const int DISPLAYED_LAYERS = 4;
const float LAYER_POSITION_X_DELTA = 200.0f;
const float LAYER_POSITION_TANGENS = M_PI/4.0f;

const float EPS = 0.00000001f;

const int EMPTY_REPEATER =  0;
const int REPEAT_FOREVER = -1;
const int REPEAT_DISABLE = -2;

#define GET_I_BY_X(x) ((x)/LAYER_POSITION_X_DELTA)*((x)/LAYER_POSITION_X_DELTA)
#define LAYER_OPACITY(x) (fabs(x) < EPS) ? 255 : atan(1.0f/1.2f)/atan(GET_I_BY_X(x)/1.2) * 200.0f
#define LAYER_POSITION_X(i) LAYER_POSITION_X_DELTA * sqrt((double)i)
#define LAYER_POSITION_Y(x,centerX,centerY) (x - centerX) * tan(LAYER_POSITION_TANGENS) + centerY
#define SCALE_FUNCTION(x) 1.0f - atan(GET_I_BY_X(x)/1.2f)/atan(1.0f/1.2f) * 0.15f

class AbstractWorld;

#define SERIALIZE_SETTING(type, name, txtName, category) \
private: const std::string name##Key = txtName; \
public: type get##name(void) \
{ \
    cocos2d::ValueMap dict = settingsDictionary.at(category).asValueMap(); \
    return to##type(dict.at(name##Key).asString()); \
} \
public: void set##name(type value) \
{ \
    cocos2d::ValueMap &dict = settingsDictionary.at(category).asValueMap(); \
    dict[name##Key] = cocos2d::Value(std::to_string(value)); \
    save(); \
} 

class GameLayer;

struct _local_world_info
{
    unsigned int levelNum;
};

struct _net_world_info
{
    struct _level_info
    {
        unsigned int robotNum;
    };
    
    unsigned int levelNum;
    std::vector<_level_info> levelInfo;
};

class PMSettings : public Singleton<PMSettings>
{
private:
    const std::string FileName = "Settings.plist";
    
    const std::string MapSettings = "MapSettings";
    const std::string AnimationSettings = "AnimationSettings";
    const std::string GameSettings = "GameSettings";
    const std::string SoundSettings = "SoundSettings";
    const std::string LanguageSettings = "LanguageSettings";
    
    SERIALIZE_SETTING(int, MapVersion, "MapVersion", MapSettings)
    SYNTHESIZE_D(bool, localGame, LocalGame, true)
    SYNTHESIZE(std::string, mapPath, MapPath)
    SYNTHESIZE(std::string, netMapPath, NetMapPath)
    
    SYNTHESIZE(std::string, copyMap, CopyMap)
    
    SYNTHESIZE(std::string, databaseFilePath, DatabaseFilePath)
    
    SERIALIZE_SETTING(double, AnimationSpeed, "AnimationSpeed", AnimationSettings)
    SYNTHESIZE_D(double, minAnimationSpeed, MinAnimationSpeed, 0.2)
    SYNTHESIZE_D(double, maxAnimationSpeed, MaxAnimationSpeed, 1.0)
    
    SERIALIZE_SETTING(int, WorldPart, "WorldPart", GameSettings)
    SERIALIZE_SETTING(int, Level, "Level", GameSettings)
    SERIALIZE_SETTING(bool, GameStarted, "GameStarted", GameSettings)
    
    SERIALIZE_SETTING(int, BackgroundVolume, "BackgroundVolume",SoundSettings)
    SERIALIZE_SETTING(int, EffectVolume, "EffectVolume", SoundSettings)
    SERIALIZE_SETTING(bool, SoundEnabled, "SoundEnabled", SoundSettings)
    
    SERIALIZE_SETTING(int, Language, "Language",LanguageSettings)
    
    SYNTHESIZE_D(cocos2d::Size, screenSize, ScreenSize, cocos2d::Size::ZERO)
    SYNTHESIZE_D(float, programLayerWidth, ProgramLayerWidth, 350.0)
    
    SYNTHESIZE(std::vector<_local_world_info>, mapsInfo, MapsInfo)
    SYNTHESIZE(std::vector<_net_world_info>, netMapsInfo, NetMapsInfo)
    
    SYNTHESIZE_D(std::string, font, Font, "fonts/tahoma.ttf")
    SYNTHESIZE_D(GameLayer *, gameLayer, GameLayer, nullptr)
public:
    
    PMSettings()
    {
        if(fileExists())
        {
            settingsDictionary = cocos2d::FileUtils::getInstance()->getValueMapFromFile(std::string(writablePath) + FileName);
        }
        else
        {
            cocos2d::ValueMap mapDictionary;
            mapDictionary.insert(std::pair<std::string, cocos2d::Value>(MapVersionKey, cocos2d::Value("0")));
            settingsDictionary.insert(std::pair<std::string, cocos2d::Value>(MapSettings, cocos2d::Value(mapDictionary)));
            
            cocos2d::ValueMap animationDictionary;
            animationDictionary.insert(std::pair<std::string, cocos2d::Value>(AnimationSpeedKey, cocos2d::Value("0.5")));
            settingsDictionary.insert(std::pair<std::string, cocos2d::Value>(AnimationSettings, cocos2d::Value(animationDictionary)));
            
            cocos2d::ValueMap gameDictionary;
            gameDictionary.insert(std::pair<std::string, cocos2d::Value>(WorldPartKey, cocos2d::Value("0")));
            gameDictionary.insert(std::pair<std::string, cocos2d::Value>(LevelKey, cocos2d::Value("1")));
            gameDictionary.insert(std::pair<std::string, cocos2d::Value>(GameStartedKey, cocos2d::Value("0")));
            settingsDictionary.insert(std::pair<std::string, cocos2d::Value>(GameSettings, cocos2d::Value(gameDictionary)));
            
            cocos2d::ValueMap soundDictionary;
            soundDictionary.insert(std::pair<std::string, cocos2d::Value>(BackgroundVolumeKey, cocos2d::Value("1.0")));
            soundDictionary.insert(std::pair<std::string, cocos2d::Value>(EffectVolumeKey, cocos2d::Value("1.0")));
            soundDictionary.insert(std::pair<std::string, cocos2d::Value>(SoundEnabledKey, cocos2d::Value("1")));
            settingsDictionary.insert(std::pair<std::string, cocos2d::Value>(SoundSettings, cocos2d::Value(soundDictionary)));
            
            cocos2d::ValueMap languageDictionary;
            languageDictionary.insert(std::pair<std::string, cocos2d::Value>(LanguageKey,
                                                                             cocos2d::Value(std::to_string((int)cocos2d::Application::getInstance()->getCurrentLanguage()))));
            settingsDictionary.insert(std::pair<std::string, cocos2d::Value>(LanguageSettings, cocos2d::Value(languageDictionary)));
            
            save();
        }
        
        databaseFilePath = writablePath + "GameDatabase";
    }

private:
    
    cocos2d::ValueMap settingsDictionary;
    
    bool fileExists()
    {
        return cocos2d::FileUtils::getInstance()->isFileExist(writablePath + FileName);
    }
    
    void save()
    {
        cocos2d::FileUtils::getInstance()->writeToFile(settingsDictionary, (std::string(writablePath) + FileName).c_str());
        
    }
    
    int toint(const std::string &val)
    {
        return atoi(val.c_str());
    }
    
    double todouble(const std::string &val)
    {
        return atof(val.c_str());
    }
    
    bool tobool(const std::string &val)
    {
        return (bool)atoi(val.c_str());
    }
    
public:
    cocos2d::Size mapSize()
    {
        return cocos2d::Size(screenSize.width - programLayerWidth, screenSize.height);
    }
    
    void incLevel()
    {
        setLevel(getLevel() + 1);
    }
    
    void incWorldPart()
    {
        setWorldPart(getWorldPart() + 1);
    }
    
    void decLevel()
    {
        setLevel(getLevel() - 1);
    }
    
    void decWorldPart()
    {
        setWorldPart(getWorldPart() - 1);
    }
    
    void loadWorlds();
    
    std::string pathToMap (int world, int map, bool local) {
        if (local) {
            return writablePath+"Maps/Local/World"+std::to_string(world) + "/Lvl"+std::to_string(map)+".map";
        } else {
            return writablePath+"Maps/Net/World"+std::to_string(world) + "/Lvl"+std::to_string(map)+".map";
        }
    }
    
    std::string pathToMap (int world, int map) {
        return pathToMap (world, map, getLocalGame());
    }
    
    std::string pathToLocalMap (bool local) {
        return pathToMap (getWorldPart(), getLevel(), local);
    }
    
    std::string pathToLocalMap () {
        return pathToLocalMap(getLocalGame());
    }
    
    std::string pathToWorlds (bool local) {
        if (local) {
            return writablePath+"Maps/Local/Worlds.xml";
        } else {
            return writablePath+"Maps/Net/Worlds.xml";
        }
    }
    
    std::string pathToWorlds () {
        return pathToWorlds(getLocalGame());
    }
    
    std::string pathToWorld (int world, bool local) {
        if (local) {
            return writablePath+"Maps/Local/World"+std::to_string(world);
        } else {
            return writablePath+"Maps/Net/World"+std::to_string(world);
        }
    }
    
    std::string pathToWorld (int world) {
        return pathToWorld(world, getLocalGame());
    }
    
    std::string pathToWorld () {
        if (getLocalGame()) {
            return writablePath+"Maps/Local/World"+std::to_string(getWorldPart());
        } else {
            return writablePath+"Maps/Net/World"+std::to_string(getWorldPart());
        }
    }
    
    void deleteWorld(int world, bool local) {
        if (local) {
            auto settings = PMSettings::instance();
            std::vector<_local_world_info> var = settings->getMapsInfo();
            var.erase(var.begin()+world);
            settings->setMapsInfo(var);
        } else {
            auto settings = PMSettings::instance();
            std::vector<_net_world_info> var = settings->getNetMapsInfo();
            var.erase(var.begin()+world);
            settings->setNetMapsInfo(var);
        }
    }
    
    void deleteWorld(int world) {
        deleteWorld(world, getLocalGame());
    }
    
    void deleteWorld() {
        deleteWorld(getWorldPart(), getLocalGame());
    }
    
    void addWorld(bool local) {
        if (local) {
            PMSettings* settings = PMSettings::instance().get();
            std::vector<_local_world_info> mapsInfo = settings->getMapsInfo();
            mapsInfo.push_back(_local_world_info{ 0 });
            settings->setMapsInfo(mapsInfo);
        } else {
            PMSettings* settings = PMSettings::instance().get();
            std::vector<_net_world_info> mapsInfo = settings->getNetMapsInfo();
            mapsInfo.push_back(_net_world_info{ 0, std::vector<_net_world_info::_level_info>(0) });
            settings->setNetMapsInfo(mapsInfo);
        }
    }
    
    void addWorld() {
        addWorld(getLocalGame());
    }
    
    void deleteMap(int world, int map, bool local) {
        if (local) {
            PMSettings* settings = PMSettings::instance().get();
            std::vector<_local_world_info> mapsInfo = settings->getMapsInfo();
            mapsInfo.at(world).levelNum--;
            settings->setMapsInfo(mapsInfo);
        } else {
            PMSettings* settings = PMSettings::instance().get();
            std::vector<_net_world_info> mapsInfo = settings->getNetMapsInfo();
            mapsInfo.at(world).levelNum--;
            mapsInfo.at(world).levelInfo.erase(mapsInfo.at(world).levelInfo.begin()+(map-1));
            settings->setNetMapsInfo(mapsInfo);
        }
        
        system(("rm "+pathToMap(world, map,local)).c_str());
        
        for (int i = map+1; i <= getMapsCount(world, local)+1; ++i) {
            std:: string oldMapName = PMSettings::instance()->pathToMap(world, i,local);
            std:: string newMapName = PMSettings::instance()->pathToMap(world, i-1,local);
            rename(oldMapName.c_str(), newMapName.c_str());
        }
    }
    
    void deleteMap(int world, int map) {
        deleteMap(world, map, getLocalGame());
    }
    
    void deleteMap(int map) {
        deleteMap(getWorldPart(), map, getLocalGame());
    }
    
    void deleteMap() {
        deleteMap(getWorldPart(), getLevel(), getLocalGame());
    }
    
    void addMap(int world, bool local) {
        if (local) {
            PMSettings* settings = PMSettings::instance().get();
            std::vector<_local_world_info> mapsInfo = settings->getMapsInfo();
            mapsInfo.at(world).levelNum++;
            settings->setMapsInfo(mapsInfo);
        } else {
            PMSettings* settings = PMSettings::instance().get();
            std::vector<_net_world_info> mapsInfo = settings->getNetMapsInfo();
            mapsInfo.at(world).levelNum++;
            mapsInfo.at(world).levelInfo.push_back(_net_world_info::_level_info{0});
            settings->setNetMapsInfo(mapsInfo);
        }
    }
    
    void addMap(int world) {
        addMap(world,getLocalGame());
    }
    
    void addMap() {
        addMap(getWorldPart());
    }
    
    int getWorldsCount(bool local) {
        if (local) {
            return getMapsInfo().size();
        } else {
            return getNetMapsInfo().size();
        }
    }
    
    int getWorldsCount() {
        return getWorldsCount(getLocalGame());
    }
    
    int getMapsCount(int world, bool local) {
        if (local) {
            return getMapsInfo()[world].levelNum;
        } else {
            return getNetMapsInfo()[world].levelNum;
        }
    }
    
    int getMapsCount(int world) {
        return getMapsCount(world, getLocalGame());
    }
    
    int getMapsCount() {
        return getMapsCount(getWorldPart(), getLocalGame());
    }
};


#endif /* defined(__pictomir_cocos2d_x__GameParams__) */

//
//  PMTextureCache.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/9/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "PMTextureCache.h"
#include "Robot4.h"

USING_NS_CC;
using namespace std;

PMTextureCache *PMTextureCache::_instanse = nullptr;

PMTextureCache *PMTextureCache::instanse()
{
    if(_instanse == nullptr)
    {
        _instanse = new PMTextureCache;
        _instanse->init();
    }
    
    return _instanse;
}

void PMTextureCache::deleteInstanse()
{
    CC_SAFE_DELETE(_instanse);
}

PMTextureCache::~PMTextureCache()
{
    Director::getInstance()->getTextureCache()->removeAllTextures();
}

void PMTextureCache::initMapTexture(WorldType type,int tileset)
{
    worldType = type;
    
    switch(worldType)
    {
        case pmWorld4 :
        {
            SpriteFrameCache *cache = SpriteFrameCache::getInstance();
            cache->addSpriteFramesWithFile(StringUtils::format(PICT_PATH "MapsPics/world4-tileset%d.plist",tileset));
            
            mapTexture_Disabled = cocos2d::StringUtils::format(PICT_PATH "MapsPics/world4_tileset%d_Disabled.png",tileset);
            mapEditTexture = cocos2d::StringUtils::format(PICT_PATH "MapsPics/world4_tileset%d_Edit.png",tileset);
        }    break;
        default: break;
    }
}

void PMTextureCache::initRobotTextures(AbstractWorld *world)
{
    if(!robotsLoaded)
    {
        robotsLoaded = true;
    }
    
    if(world->getBaseRobot() != nullptr)
    {
        switch(world->getBaseRobot()->type())
        {
            case pmRobot4:
            {
                SpriteFrameCache *cache = SpriteFrameCache::getInstance();
                cache->addSpriteFramesWithFile(PICT_PATH "Robot Methods/robot4-methods.plist");
                cache->addSpriteFramesWithFile(PICT_PATH "Conditions/robot4-conditions.plist");
                
            }   break;
            case pmLamp4:
                break;
            default: break;
        }
    }
}

Sprite *PMTextureCache::getMapGrassTile(int tile, std::string state)
{
    return Sprite::createWithSpriteFrame(getMapGrassTileFrame(tile, state));
}

SpriteFrame *PMTextureCache::getMapGrassTileFrame(int tile, std::string state)
{
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();

    return cache->getSpriteFrameByName(StringUtils::format("mapElement%d-%s.png", tile, state.c_str()));
}

Sprite *PMTextureCache::getMapWallTile(int wall, std::string state)
{
    return Sprite::createWithSpriteFrame(getMapWallTileFrame(wall, state));
}

SpriteFrame *PMTextureCache::getMapWallTileFrame(int wall, std::string state)
{
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    SpriteFrame *frame = cache->getSpriteFrameByName(StringUtils::format("wall%d-%s.png", wall, state.c_str()));
    
    return frame;
}

Sprite *PMTextureCache::getMapEditGrassTile(int tile)
{
    switch(worldType)
    {
        case pmWorld4: return Sprite::create(mapEditTexture.c_str(), Rect((tile+1) * 64.0, 0.0, 64.0, 64.0));
        default: return nullptr;
    }
}

Sprite *PMTextureCache::getMapEditWallTile(int wall)
{
    switch(worldType)
    {
        case pmWorld4: return Sprite::create(mapEditTexture.c_str(), Rect(5*64.0 + wall * 78.0, 0.0, 78.0, 78.0));
        default: return nullptr;
    }
}

Sprite *PMTextureCache::getMapGrassDisabledTile(int tile)
{
    switch(worldType)
    {
        case pmWorld4: return Sprite::create(mapTexture_Disabled.c_str(), Rect((tile+1) * 64.0, 0.0, 64.0, 64.0));
        default: return nullptr;
    }
}

Sprite *PMTextureCache::getMapWallDisabledTile(int wall)
{
    switch(worldType)
    {
        case pmWorld4: return Sprite::create(mapTexture_Disabled.c_str(), Rect(5*64.0 + wall * 78.0, 0.0, 78.0, 78.0));
        default: return nullptr;
    }
}


Sprite *PMTextureCache::getIconSprite(std::string name, std::string state)
{
    return Sprite::createWithSpriteFrame(getIconSpriteFrame(name, state));
}

SpriteFrame *PMTextureCache::getIconSpriteFrame(std::string name, std::string state)
{
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    
    return cache->getSpriteFrameByName(StringUtils::format("%sIcon-%s.png", name.c_str(), state.c_str()));
}

Sprite *PMTextureCache::getRobotMethodSprite(int method, _method::ExecuteType execType, std::string state)
{
    return Sprite::createWithSpriteFrame(getRobotMethodSpriteFrame(method, execType, state));
}

SpriteFrame *PMTextureCache::getRobotMethodSpriteFrame(int method, _method::ExecuteType execType, std::string state)
{
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    SpriteFrame *frame;
    
    if(state != DISABLED_STATE)
    {
        frame = cache->getSpriteFrameByName(StringUtils::format("%d-%s-%d.png", method, state.c_str(), execType));
    }
    else
    {
        frame = cache->getSpriteFrameByName(StringUtils::format("%d-%s.png", method, state.c_str()));
    }
    
    return frame;
}

Sprite *PMTextureCache::getRobotConditionSprite(int condition, std::string state)
{
    return Sprite::createWithSpriteFrame(getRobotConditionSpriteFrame(condition, state));
}

SpriteFrame *PMTextureCache::getRobotConditionSpriteFrame(int condition, std::string state)
{
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    return cache->getSpriteFrameByName(StringUtils::format("cond%d-%s.png", condition, state.c_str()));
}

cocos2d::Sprite *PMTextureCache::getRepeaterSprite(int repeater, std::string state)
{
    return Sprite::createWithSpriteFrame(getRepeaterSpriteFrame(repeater, state));
}

cocos2d::SpriteFrame *PMTextureCache::getRepeaterSpriteFrame(int repeater, std::string state)
{
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    return cache->getSpriteFrameByName(StringUtils::format("rep%d-%s.png", repeater, state.c_str()));
}

Sprite *PMTextureCache::getProgramSprite(int method, _method::ExecuteType execType, std::string state)
{
    return Sprite::createWithSpriteFrame(getProgramSpriteFrame(method, execType, state));
}

SpriteFrame *PMTextureCache::getProgramSpriteFrame(int cmd, _method::ExecuteType execType, std::string state)
{
    string cmdName;
    
    switch(cmd)
    {
        case CMDA:cmdName = "A"; break;
        case CMDB:cmdName = "B"; break;
        case CMDC:cmdName = "C"; break;
    }
    
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    SpriteFrame *frame;
    
    if(state != DISABLED_STATE)
    {
        frame = cache->getSpriteFrameByName(StringUtils::format("%s-%s-%d.png", cmdName.c_str(), state.c_str(), execType));
    }
    else
    {
        frame = cache->getSpriteFrameByName(StringUtils::format("%s-%s.png", cmdName.c_str(), state.c_str()));
    }
    return frame;
}

void PMTextureCache::init()
{
    robotsLoaded = false;

    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile(PICT_PATH "Robot Methods/common-methods.plist");
    
    cache->addSpriteFramesWithFile(PICT_PATH "Conditions/common-conditions.plist");
    
    cache->addSpriteFramesWithFile(PICT_PATH "Repeaters/repeaters.plist");
    cache->addSpriteFramesWithFile(PICT_PATH "System/iconset.plist");
    
//    TEXTURECACHE_CREATE_TEXTURE(methodBackgroundOnTrueTexture,"System/actionBackground");
//    TEXTURECACHE_CREATE_TEXTURE(methodBackgroundOnFalseTexture,"System/actionBackgroundOnFalse");
//    TEXTURECACHE_CREATE_TEXTURE(methodBackgroundAlwaysTexture,"System/actionBackgroundAlways");
//    methodBackgroundOnTrueTexture_Broken = PICT_PATH "System/actionBackground_Broken.png";
//    methodBackgroundOnFalseTexture_Broken = PICT_PATH "System/actionBackgroundOnFalse_Broken.png";
//    methodBackgroundAlwaysTexture_Broken = PICT_PATH "System/actionBackgroundAlways_Broken.png";
//    
//    lockTexture = PICT_PATH "System/lock.png";
//    unlockTexture = PICT_PATH "System/Unlock.png";
//    
//    TEXTURECACHE_CREATE_TEXTURE(editIconTexture,"System/EditIcon");
//    TEXTURECACHE_CREATE_TEXTURE(startIconTexture,"System/StartIcon");
//    TEXTURECACHE_CREATE_TEXTURE(pauseIconTexture,"System/PauseIcon");
//    TEXTURECACHE_CREATE_TEXTURE(stopIconTexture,"System/StopIcon");
//    TEXTURECACHE_CREATE_TEXTURE(makeStepIconTexture,"System/MakeStepIcon");
//    TEXTURECACHE_CREATE_TEXTURE(nextLevelIconTexture,"System/NextLevelIcon");
//    TEXTURECACHE_CREATE_TEXTURE(prevLevelIconTexture,"System/PrevLevelIcon");
//    TEXTURECACHE_CREATE_TEXTURE(showIconTexture,"System/ShowIcon");
//    TEXTURECACHE_CREATE_TEXTURE(hideIconTexture,"System/HideIcon");
//    TEXTURECACHE_CREATE_TEXTURE(pasteIconTexture,"System/PasteIcon");
//    TEXTURECACHE_CREATE_TEXTURE(clearIconTexture,"System/ClearIcon");
//    
//    TEXTURECACHE_CREATE_TEXTURE(soundOffIconTexture,"System/SoundOffIcon");
//    TEXTURECACHE_CREATE_TEXTURE(soundOnIconTexture,"System/SoundOnIcon");
//    
//    TEXTURECACHE_CREATE_TEXTURE(infoIconTexture,"System/InfoIcon");

}

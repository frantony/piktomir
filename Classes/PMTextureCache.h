//
//  PMTextureCache.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/9/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PMTextureCache__
#define __pictomir_cocos2d_x__PMTextureCache__

#include <iostream>
#include "cocos2d.h"
#include "AbstractWorld.h"
#include "AbstractRobot.h"


const std::string NORMAL_STATE = "normal";
const std::string SELECTED_STATE = "selected";
const std::string DISABLED_STATE = "disabled";
const std::string FAILED_STATE = "failed";

#define TEXTURECACHE_SYNTHESIZE_VECTOR(varName, funName)\
protected:  std::vector<std::string> varName;\
protected:  std::vector<std::string> varName##_Selected;\
protected:  std::vector<std::string> varName##_Disabled;\
public: cocos2d::Texture2D * get##funName##Texture(int var)\
{ return  cocos2d::Director::getInstance()->getTextureCache()->addImage(varName[var].c_str()); }\
public: cocos2d::Texture2D * get##funName##Texture_Selected(int var) \
{ return  cocos2d::Director::getInstance()->getTextureCache()->addImage(varName##_Selected[var].c_str()); }\
public: cocos2d::Texture2D * get##funName##Texture_Disabled(int var) \
{ return  cocos2d::Director::getInstance()->getTextureCache()->addImage(varName##_Disabled[var].c_str()); }\
public: cocos2d::Sprite * get##funName##Sprite(int var) { return  cocos2d::Sprite::create(varName[var].c_str()); }\
public: cocos2d::Sprite * get##funName##Sprite_Selected(int var) { return  cocos2d::Sprite::create(varName##_Selected[var].c_str()); }\
public: cocos2d::Sprite * get##funName##Sprite_Disabled(int var) { return  cocos2d::Sprite::create(varName##_Disabled[var].c_str()); }

#define TEXTURECACHE_SET_TEXTURE(varName, index, iconName)\
varName[index] = (string(PICT_PATH)+ string(iconName)+".png").c_str();\
varName##_Selected[index] = (string(PICT_PATH)+ string(iconName)+"_Selected.png").c_str();\
varName##_Disabled[index] = (string(PICT_PATH)+ string(iconName)+"_Disabled.png").c_str();



#define TEXTURECACHE_SYNTHESIZE(varName, funName)\
protected:  std::string varName;\
protected:  std::string varName##_Selected;\
protected:  std::string varName##_Disabled;\
public: cocos2d::Texture2D * get##funName##Texture(void) \
{ return  cocos2d::Director::getInstance()->getTextureCache()->addImage(varName.c_str()); }\
public: cocos2d::Texture2D * get##funName##Texture_Selected(void) \
{ return  cocos2d::Director::getInstance()->getTextureCache()->addImage(varName##_Selected.c_str()); }\
public: cocos2d::Texture2D * get##funName##Texture_Disabled(void) \
{ return  cocos2d::Director::getInstance()->getTextureCache()->addImage(varName##_Disabled.c_str()); } \
public: cocos2d::Sprite * get##funName##Sprite(void) { return  cocos2d::Sprite::create(varName.c_str()); }\
public: cocos2d::Sprite * get##funName##Sprite_Selected(void) { return  cocos2d::Sprite::create(varName##_Selected.c_str()); }\
public: cocos2d::Sprite * get##funName##Sprite_Disabled(void) { return  cocos2d::Sprite::create(varName##_Disabled.c_str()); }

#define TEXTURECACHE_CREATE_TEXTURE(varName, iconName)\
varName = PICT_PATH iconName".png";\
varName##_Selected = PICT_PATH iconName"_Selected.png";\
varName##_Disabled = PICT_PATH iconName"_Disabled.png";


class PMTextureCache
{
private:
    std::string mapTexture_Disabled;
    std::string mapEditTexture;

//    std::string lockTexture;
//    std::string unlockTexture;
    
    WorldType worldType;
    
    bool robotsLoaded;
//    TEXTURECACHE_SYNTHESIZE(startIconTexture, StartIcon)
//    TEXTURECACHE_SYNTHESIZE(editIconTexture, EditIcon)
//    TEXTURECACHE_SYNTHESIZE(pauseIconTexture, PauseIcon)
//    TEXTURECACHE_SYNTHESIZE(makeStepIconTexture, MakeStepIcon)
//    TEXTURECACHE_SYNTHESIZE(stopIconTexture, StopIcon)
//    TEXTURECACHE_SYNTHESIZE(nextLevelIconTexture, NextLevelIcon)
//    TEXTURECACHE_SYNTHESIZE(prevLevelIconTexture, PrevLevelIcon)
//    TEXTURECACHE_SYNTHESIZE(showIconTexture, ShowIcon)
//    TEXTURECACHE_SYNTHESIZE(hideIconTexture, HideIcon)
//    TEXTURECACHE_SYNTHESIZE(pasteIconTexture, PasteIcon)
//    TEXTURECACHE_SYNTHESIZE(clearIconTexture, ClearIcon)
//    
//    TEXTURECACHE_SYNTHESIZE(soundOffIconTexture, SoundOffIcon)
//    TEXTURECACHE_SYNTHESIZE(soundOnIconTexture, SoundOnIcon)
//    
//    TEXTURECACHE_SYNTHESIZE(infoIconTexture, InfoIcon)
    
public:
    
    ~PMTextureCache();
    
    void initMapTexture(WorldType type,int tileset);
    void initRobotTextures(AbstractWorld *world);
    void init();
    
    cocos2d::Sprite *getIconSprite(std::string name, std::string state = NORMAL_STATE);
    cocos2d::SpriteFrame *getIconSpriteFrame(std::string name, std::string state = NORMAL_STATE);
    
    cocos2d::Sprite *getRobotMethodSprite(int method, _method::ExecuteType execType, std::string state);
    cocos2d::SpriteFrame *getRobotMethodSpriteFrame(int method, _method::ExecuteType execType = _method::pmExecuteOnTrue, std::string state = NORMAL_STATE);
    
    cocos2d::Sprite *getRobotConditionSprite(int condition, std::string state);
    cocos2d::SpriteFrame *getRobotConditionSpriteFrame(int condition, std::string state = NORMAL_STATE);
    
    cocos2d::Sprite *getRepeaterSprite(int repeater, std::string state);
    cocos2d::SpriteFrame *getRepeaterSpriteFrame(int repeater, std::string state = NORMAL_STATE);
    
    cocos2d::SpriteFrame *getProgramSpriteFrame(int cmd, _method::ExecuteType execType , std::string state);
    cocos2d::Sprite *getProgramSprite(int cmd, _method::ExecuteType execType , std::string state);
    
    cocos2d::Sprite *getMapGrassTile(int tile, std::string state = NORMAL_STATE);
    cocos2d::SpriteFrame *getMapGrassTileFrame(int tile, std::string state = NORMAL_STATE);
    cocos2d::Sprite *getMapWallTile(int wall, std::string state = NORMAL_STATE);
    cocos2d::SpriteFrame *getMapWallTileFrame(int wall, std::string state = NORMAL_STATE);
    
    cocos2d::Sprite *getMapEditGrassTile(int tile);
    cocos2d::Sprite *getMapEditWallTile(int wall);
    cocos2d::Sprite *getMapGrassDisabledTile(int tile);
    cocos2d::Sprite *getMapWallDisabledTile(int wall);
    
//    cocos2d::Sprite *getLockSprite()
//    {
//        return cocos2d::Sprite::create(lockTexture.c_str());
//    }
//    
//    cocos2d::Sprite *getUnlockSprite()
//    {
//        return cocos2d::Sprite::create(unlockTexture.c_str());
//    }
//    
    
    static PMTextureCache *_instanse;
    static PMTextureCache *instanse();
    static void deleteInstanse();
};

#endif /* defined(__pictomir_cocos2d_x__PMTextureCache__) */

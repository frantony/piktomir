//
//  AbstractRobot.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef pictomir_cocos2d_x_AbstractRobot_h
#define pictomir_cocos2d_x_AbstractRobot_h

#define SYNTHESIZE(varType, varName, funName)\
protected: varType varName;\
public: varType get##funName(void) const { return varName; }\
public: void set##funName(varType var){ varName = var; }

#include "cocos2d.h"
#include "RobotMethod.h"
#include "protocol.h"
#include <map>
#include <vector>
#include <stack>
#include <functional>
#include "TaskList.h"

class AbstractWorld;
class ProgramLayer;
class Task;

const int EMPTY_METHOD = 0;
const int CMDMAIN = -2;
const int CMDA = -3;
const int CMDB = -4;
const int CMDC = -5;



const int EMPTY_CONDITION = 0;

enum RobotType
{
    pmEnvRobot = -3,
    pmPlayerRobot = -2,
    pmAbstractRobot = -1,
    pmRobot4 = 0,
    pmLamp4 = 1,
    pmTrasportRobot4,
    pmRobotCount = 3
};

class AbstractRobot
{
    SYNTHESIZE(AbstractWorld *, parentWorld, ParentWorld);
    
    SYNTHESIZE(int, stepCount, StepCount);
    
    SYNTHESIZE(TaskList *, taskList, TaskList);
public:
    enum RobotState
    {
        NoState,
        EndedWork,
        PlayingAnimation,
        Win,
        Broken
    };
    
    enum RobotControl
    {
        NoControl,
        Programm,
        Network,
        Auto
    };
    
private:
    
    unsigned char state;
    
public:
    AbstractRobot():
        parentWorld(nullptr),
        state(0),
        taskList(nullptr)
    {}
    
    AbstractRobot(AbstractWorld *parentWorld):
        parentWorld(parentWorld),
        state(0),
        taskList(nullptr)
    {}
    
    std::map<int,RobotMethod> methodList;
    
    virtual ~AbstractRobot()
    {
        CC_SAFE_DELETE(taskList);
    }
    
    static AbstractRobot* create()
    {
        return new AbstractRobot();
    }
    
    static AbstractRobot* create(AbstractWorld *parentWorld)
    {
        return new AbstractRobot(parentWorld);
    }
    
    void setStateFlag(RobotState flag)
    {
        state |= 1 << flag;
    }
    
    void clearStateFlag(RobotState flag)
    {
        state &= ~(1 << flag);
    }
    
    bool isEndedWork()
    {
        return state & (1 << EndedWork);
    }
    
    bool isPlayingAnimation()
    {
        return state & (1 << PlayingAnimation);
    }
    
    bool isBroken()
    {
        return state & (1 << Broken);
    }
    
    bool isWin()
    {
        return state & (1 << Win);
    }
    
    void clearAllStateFlags()
    {
        state = 0;
    }
    
    void execute(int methodID) const
    {
        //if(methodList.find(methodName).)
        methodList.at(methodID).execute();
    }
    
    RobotMethod getMethod(int methodID) const
    {
        //if(methodList.find(methodName).)
        return methodList.at(methodID);
    }
    
    void addMethod(int key, RobotMethod method)
    {
        methodList.insert(std::pair<int,RobotMethod>(key,method));
    }
    
    std::vector<int> getMethodList();
    
    void cleanTempMethods();
    void cleanAllMethods();
    
    void checkTask();
    
    //pt_robot_instr_list *getNetMethodList(unsigned int index);
    void setMethodListFromNet(_pt_robot_instr_list *netList);
    
    virtual bool checkCondition(int condition)
    {
        return true;
    }

    virtual void destroy() {}
    virtual void win() {}
    
    //for safe delete support
    virtual void release()
    {
        
    }
    
    virtual void interact(AbstractRobot *robot, const std::function<void(AbstractRobot *)> &callback)
    {
        
    }
    
    virtual bool interactable()
    {
        return false;
    }
    
    virtual bool canMoveOn()
    {
        return false;
    }
    
    virtual void resetRobot()
    {
        
    }
    
    virtual RobotType type ()
    {
        return pmAbstractRobot;
    }
    
    virtual int getNativeMethodCount()
    {
        return 0;
    }
    
    virtual int getConditionsCount()
    {
        return 0;
    }
    
    virtual RobotType parentType()
    {
        return pmAbstractRobot;
    }
    
};


#endif

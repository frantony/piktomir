//
//  EnvRobot.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/11/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef pictomir_cocos2d_x_EnvRobot_h
#define pictomir_cocos2d_x_EnvRobot_h

#include "cocos2d.h"
#include "AbstractRobot.h"

class PlayerRobot;

class EnvRobot : public AbstractRobot,public cocos2d::Sprite
{
public:
    EnvRobot(AbstractWorld *parentWorld):
        AbstractRobot(parentWorld)
    {}
    
    virtual RobotType parentType() final
    {
        return pmEnvRobot;
    }
    
    virtual void initTexture() = 0;

    cocos2d::Vector<cocos2d::Sprite *> &getParts()
    {
        return parts;
    }
protected:
    cocos2d::Vector<cocos2d::Sprite *> parts;
};

#endif

//
//  RobotManager.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 29.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#include "RobotManager.h"
#include "GameLayer.h"
#include "PMSettings.h"
#include "VariableList.h"


using namespace std;
USING_NS_CC;


void RobotManager::loop()
{
    if(state == Paused)
        return;
    
    if(robot->getParentWorld()->robotsWin() && !controlled)
    {
        robot->win();
        
        //Log("stack size: %lu", robot->instructionStack.size());
        
        if(gameLayer != nullptr)
            gameLayer->robotWin();
        
        return;
    }
    else if(robot->isBroken())
    {
        robot->destroy();
        
        if(gameLayer != nullptr)
            gameLayer->robotLoose();
        
        return;
    }
    else if(robot->isPlayingAnimation())
    {
        return;
    }
    
    if(programEnded && !robot->isEndedWork() && !controlled)
    {
        robot->setStateFlag(AbstractRobot::EndedWork);
        return;
    }
    
    if(robot->getParentWorld()->robotsEndedWork() && !controlled)
    {
        if(gameLayer != nullptr)
            gameLayer->robotFailure();
        
        return;
    }
    
    if(programEnded)
        return;
    
    if(state == ControlledByStack)
    {
        if(forcedList.empty())
        {
            state = Paused;
            return;
        }
        
        _instruction forcedInstr = forcedList[0];
        
        if(forcedInstr.command == INSTR_EXECUTE)
        {
            RobotMethod method = robot->getMethod(forcedInstr.param);
            
            if(method.getMethodType() == RobotMethod::NativeMethod)
            {
                if(robot->getParentWorld()->robotsPlayingAnimation(robot))
                {
                    return;
                }
                
                method.execute();
                forcedList.erase(forcedList.begin());
            }

        }
        
        return;
    }
    
    
    _instruction instr = program[programPosition];
    
    switch(instr.command)
    {
        case INSTR_EXECUTE:
        {
            RobotMethod method = robot->getMethod(instr.param);
            
            if(method.getMethodType() == RobotMethod::NativeMethod)
            {                
//                if(robot->getParentWorld()->robotsPlayingAnimation(robot))
//                {
//                    return;
//                }
                
                method.execute();
                
                ++programPosition;
                
                if(state == Controlled)
                    state = Paused;
                
                if(state == ControlledByStack && programEnded && !robot->isBroken())
                    state = Paused;
            }
            else
            {
                stack.push(programPosition + 1);
                programPosition = methodRefs[instr.param];
            }
            //continue;
            //highlight button
            if(instr.otherParams[0] != -1 && instr.otherParams[1] != -1)
                if(gameLayer != nullptr )
                    gameLayer->highLightButon(instr.otherParams[0], instr.otherParams[1]);
            
            
        } break;
        case INSTR_START_LOOP:
        {
            int pos = programPosition + 1;
            
            if(instr.otherParams[0])
            {
                ++pos;
                

            }
            
            if(program[pos].command == INSTR_END_LOOP)
                programPosition = pos + 1;
            else
            {
                ++programPosition;
                stack.push(0);
            }
            
        } break;
        case INSTR_END_LOOP:
        {
            int iterator = stack.top() + 1;
            
            if(iterator == MaxRepeater)
            {
                if(gameLayer != nullptr)
                    gameLayer->robotFailure();
                
                return;
            }
            
            if(iterator < instr.param)
            {
                if(instr.otherParams[1] && !robot->getMethod(instr.otherParams[0]).hasNotDefaultMethods() &&
                   !robot->checkCondition(program[methodRefs[instr.otherParams[0]] + 1].param))
                {
                    stack.pop();
                    ++programPosition;
                }
                else
                {
                    stack.pop();
                    stack.push(iterator);
                    //with skipping start loop
                    programPosition = methodRefs[instr.otherParams[0]] + 1;
                }
            }
            else
            {
                stack.pop();
                ++programPosition;
            }
            
        } break;
        case INSTR_CHECK_CONDITION:
        {
            VariableList::instanse()->setValue(instr.otherParams[0], robot->checkCondition(instr.param));
            ++programPosition;
            
        } break;
        case INSTR_TEST:
        {
            switch ((_method::ExecuteType)instr.otherParams[0]) {
                case _method::pmExecuteAlways:
                    break;
                case _method::pmExecuteOnTrue:
                    if(!VariableList::instanse()->getValue(instr.param))
                        ++programPosition;
                    break;
                case _method::pmExecuteOnFalse:
                    if(VariableList::instanse()->getValue(instr.param))
                        ++programPosition;
                    break;
            }
            
            ++programPosition;
            
        } break;
        case INSTR_RETURN:
        {
            if(stack.empty())
            {
                programEnded = true;
                return;
            }
            
            programPosition = stack.top();
            stack.pop();
            
        } break;
    }
    
}


void RobotManager::linkProgram()
{
    program.clear();
    
    RobotMethod mainMethod = robot->getMethod(CMDMAIN);
    
    program.insert(program.end(),mainMethod.instructionList.begin(),mainMethod.instructionList.end());
    
    int programIndex = robot->getMethod(CMDMAIN).instructionList.size();
    
    for(auto i = robot->methodList.begin(); i != robot->methodList.end(); ++i)
    {
        if(i->second.getMethodType() != RobotMethod::NativeMethod && i->first != CMDMAIN)
        {
            methodRefs.insert(pair<int,int>(i->first,programIndex));
            program.insert(program.end(),i->second.instructionList.begin(),i->second.instructionList.end());
            programIndex += i->second.instructionList.size();
        }
    }
}

void RobotManager::forceAddMethod(int id, int n)
{
    for(int i = 0; i < n; ++i)
    {
        _instruction instr = {INSTR_EXECUTE, (short)id, {-1, -1}};
        forcedList.push_back(instr);
    }
}
//
//  RobotManager.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 29.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__Robot4Manager__
#define __pictomir_cocos2d_x__Robot4Manager__

#include <iostream>
#include <vector>
#include <map>
#include <stack>
#include "AbstractRobot.h"
#include "PMSettings.h"

class GameLayer;

class RobotManager
{
public:
    
    enum State
    {
        Paused,
        Controlled,
        ControlledByStack,
        Working
    };
protected:    
    SYNTHESIZE(GameLayer *, gameLayer, GameLayer);
    SYNTHESIZE(State, state, State);
    SYNTHESIZE(bool, controlled, Controlled);
private:
    
    SYNTHESIZE(AbstractRobot *,robot, Robot);
    
    std::vector<_instruction> program;
    std::map<int, int> methodRefs;
    std::stack<int> stack;
    
    std::vector<_instruction> forcedList;
    
    int programPosition = 0;
    bool programEnded = false;
    
public:
    
    RobotManager():
        gameLayer(nullptr),
        state(Paused),
        controlled(false),
        robot(nullptr)
    {}
        
    static RobotManager* create()
    {
        return new RobotManager;
    }
    
    RobotManager(AbstractRobot *robot):
        gameLayer(nullptr),
        state(Paused),
        controlled(false),
        robot(robot)
    {
    }
    
    static RobotManager* create(AbstractRobot *robot)
    {
        return new RobotManager(robot);
    }
    
    void forceAddMethod(int id, int n = 1);
    
    void linkProgram();
    
    void loop();
    
    void clear()
    {
        programPosition = 0;
        programEnded = false;
        program.clear();
        methodRefs.clear();
        stack = std::stack<int> ();
    }
protected:
};


#endif /* defined(__pictomir_cocos2d_x__RobotManager__) */

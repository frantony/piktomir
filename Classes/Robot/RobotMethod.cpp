//
//  RobotMethod.c
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//
#include <stdio.h>
#include "RobotMethod.h"
#include "AbstractRobot.h"
#include "PMSettings.h"

USING_NS_CC;

using namespace std;

void RobotMethod::execute() const
{
    if(methodType == NativeMethod)
        (parent->*nativeMethod) ();
}

vector<_instruction> RobotMethod::makeMethod(int method, std::vector<_method> methods,int layerIndex,int condition, int repeater, bool reservedTag)
{
    vector<_instruction> ret;
    _instruction instr;
    
    value_tag tag;
    
    if(repeater != -2 && repeater != 0)
    {
        instr.command = INSTR_START_LOOP;
        
        instr.otherParams[0] = (condition != -1 && condition != EMPTY_CONDITION) ? 1 : 0;
        instr.otherParams[1] = method;
        ret.push_back(instr);
    }

    if(condition != -1 && condition != EMPTY_CONDITION)
    {
        instr.command = INSTR_CHECK_CONDITION;
        instr.param = condition;
        tag = instr.otherParams[0] = VariableList::instanse()->addVariable(false);
        instr.otherParams[1] = 0;
        ret.push_back(instr);
    }

    
    for(int i= 0; i < methods.size(); ++i)
    {
        if(methods[i].methodID != EMPTY_METHOD)
        {
            if(condition != -1 && condition != EMPTY_CONDITION)
            {
                instr.command = INSTR_TEST;
                instr.param = tag;
                instr.otherParams[0] = methods[i].executeType;
                instr.otherParams[1] = 0;
                ret.push_back(instr);
            }
            
            instr.command = INSTR_EXECUTE;
            instr.param = methods[i].methodID;
            instr.otherParams[0] = layerIndex;
            instr.otherParams[1] = i;
            ret.push_back(instr);
        }
    }
    
    if(repeater != -2 && repeater != 0)
    {
        instr.command = INSTR_END_LOOP;
        
        if(repeater != REPEAT_FOREVER)
            instr.param = repeater;
        else
            instr.param = MaxRepeater;
        
        instr.otherParams[0] = method;
        instr.otherParams[1] = (condition != -1 && condition != EMPTY_CONDITION) ? 1 : 0;
        ret.push_back(instr);
    }

    instr.command = INSTR_RETURN;
    instr.param = 0;
    instr.otherParams[0] = 0;
    instr.otherParams[1] = 0;
    ret.push_back(instr);
    
    return ret;
}

bool RobotMethod::hasNotDefaultMethods()
{
    bool res = false;
    
    for(int i = 0; i < instructionList.size(); ++i)
    {
        if(instructionList[i].command == INSTR_TEST && instructionList[i].otherParams[0] != _method::pmExecuteOnTrue)
        {
            res = true;
            break;
        }
    }
    
    return res;
}

//
//  SelectButton.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 09.12.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#include "SelectButton.h"
#include "PMSettings.h"
#include "PMTextureCache.h"

USING_NS_CC;

Sprite *SelectButton::cloneSprite()
{
    Sprite *newSpr = Sprite::createWithSpriteFrame(((Sprite *)getNormalImage())->getSpriteFrame());
    newSpr->setPosition(getPosition());
    
    return newSpr;
}

void SelectButton::startAnimation()
{
    startPosition = getPosition();
    ActionInterval* jump = JumpBy::create(2.0f * SYSTEM_ANIMATION_DELAY, Point(0,0), getContentSize().height / 2.5, 1);
    //FiniteTimeAction* moveLayerDown = MoveTo::create(SYSTEM_ANIMATION_DELAY, Point(getPositionX(),getPositionY()) );
    
    runnigAction = runAction( RepeatForever::create(jump) );
}

void SelectButton::jumpOnce()
{
    startPosition = getPosition();
    ActionInterval* jump = CCJumpBy::create(2.0f * SYSTEM_ANIMATION_DELAY, Point(0,0), getContentSize().height / 2.5, 1);
    
    runnigAction = runAction( jump );
}

void SelectButton::stopAnimation()
{
    if(runnigAction != nullptr)
    {
        this->stopAction(runnigAction);
        runnigAction = nullptr;
    }
    
    FiniteTimeAction *initial = MoveTo::create(SYSTEM_ANIMATION_DELAY / 2.0f, startPosition);
    
    runAction(initial);
}

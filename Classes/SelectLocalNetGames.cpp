//
//  SelectLocalNetGames.cpp
//  Piktomir
//
//  Created by Danila Eremin on 03.04.14.
//
//

#include "SelectLocalNetGames.h"
#include "utils.h"
#include "PMSettings.h"
#include "MainMenuLayer.h"
#include "SelectMenuLayer.h"
#include "MapEditorGameLayer.h"

USING_NS_CC;
using namespace std;

Scene* SelectLocalNetGames::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    SelectLocalNetGames *layer = SelectLocalNetGames::create();
    
    // add layer as a child to scene
    scene->addChild(layer,0,0);
    
    // return the scene
    return scene;
}

bool SelectLocalNetGames::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size screenSize = PMSettings::instance()->getScreenSize();
    
    setPosition( Point(0,0) );
    
    Sprite *backGround  = Sprite::create(PICT_PATH "Background/background1.png" );
    backGround->setPosition( Point(screenSize.width/2, screenSize.height/2) );
    resizeSprite(backGround, screenSize.width, screenSize.height);
    addChild(backGround, -1);
    
    Menu *navigationMenu = Menu::create(nullptr);
    Menu *worldSelectMenu = Menu::create();
    

    MenuItemFont* plusButton = MenuItemFont::create(LocalizedString("EditLocalGame"), CC_CALLBACK_1(SelectLocalNetGames::setLocalGame, this));
    plusButton->setPosition( Point(screenSize.width / 2, screenSize.height / 2 - 50) );
    worldSelectMenu->addChild(plusButton,1);
    
    plusButton = MenuItemFont::create(LocalizedString("EditNetGame"), CC_CALLBACK_1(SelectLocalNetGames::setNetGame, this));
    plusButton->setPosition( Point(screenSize.width / 2, screenSize.height / 2 + 50) );
    worldSelectMenu->setPosition( Point::ZERO );
    worldSelectMenu->addChild(plusButton,1);
    
    addChild(worldSelectMenu);
    
    MenuItemFont *goToMain = MenuItemFont::create(LocalizedString("MainMenu"),
                                                  CC_CALLBACK_1(SelectLocalNetGames::mainMenu, this));
    goToMain->setPosition( Point(170, screenSize.height - 25) );

    MenuItemFont *uploadToServer = MenuItemFont::create(LocalizedString("UploadToServer"), CC_CALLBACK_1(SelectLocalNetGames::uploadMapsToServer, this));
    uploadToServer->setPosition( Point(170, screenSize.height - 75) );
    uploadToServer->setColor(Color3B(20, 200, 20));
    navigationMenu->addChild(uploadToServer);

    
    navigationMenu->addChild(goToMain);
    navigationMenu->setPosition( Point::ZERO );
    addChild(navigationMenu, 1);
    
    
    return true;
}

void SelectLocalNetGames::uploadMapsToServer (cocos2d::Ref *sender)
{
    MapEditorGameLayer::getInstanse()->uploadMapsToServer();
}

void SelectLocalNetGames::mainMenu(cocos2d::Ref *object)
{
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, MainMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void SelectLocalNetGames::setNetGame(cocos2d::Ref *object)
{
    PMSettings::instance()->setLocalGame(false);
    PMSettings::instance()->loadWorlds();
    TransitionSlideInL* trans = TransitionSlideInR::create( 1.5f * SYSTEM_ANIMATION_DELAY, SelectMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void SelectLocalNetGames::setLocalGame(cocos2d::Ref *object)
{
    PMSettings::instance()->setLocalGame(true);
    PMSettings::instance()->loadWorlds();
    TransitionSlideInL* trans = TransitionSlideInR::create( 1.5f * SYSTEM_ANIMATION_DELAY, SelectMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

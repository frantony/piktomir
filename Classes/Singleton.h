//
//  Singleton.h
//  pictomir
//
//  Created by Anton Dedkov on 31.01.14.
//
//

#ifndef pictomir_Singleton_h
#define pictomir_Singleton_h

#include <memory>

template <class T>
class Singleton
{

public:
    typedef std::shared_ptr<T> self_type;
private:
    static self_type _self;
protected:
    inline Singleton(){}
    virtual ~Singleton(){ deleteInstanse(); }
public:
    static inline self_type instance()
    {
        if (!_self)
            _self.reset(new T);
        return _self;
    }
    
    static inline void deleteInstanse()
    {
        _self.reset();
    }
};

template <class T>
typename Singleton<T>::self_type Singleton<T>::_self;

#endif

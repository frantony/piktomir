//
//  Furnace.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 3/14/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "World4.h"
#include "PMSettings.h"
#include "GameLayer.h"
#include "Lamp4.h"
#include "PMTextureCache.h"

USING_NS_CC;

using namespace std;


bool Lamp4::init()
{
    if( !Sprite::init() )
        return false;
    
    retain();
    
    stepCount = 0;
    
    return true;
}

void Lamp4::initTexture()
{
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile(PICT_PATH "Robot/lamp4.plist");
    
    SpriteFrame *frame = cache->getSpriteFrameByName(StringUtils::format("lamp4Button%d-%d.png" , curDirection, 0));
    
    setDisplayFrame(frame);

    Sprite *lampSprite = Sprite::createWithSpriteFrame(cache->getSpriteFrameByName(StringUtils::format("lamp4-%d.png" , turnState)));

    parts.pushBack(lampSprite);
}

void Lamp4::resetRobot()
{
    turnState = Off;
    setPosition(startPosition, startLayer, startDirection);
}

void Lamp4::setPosition(Point point, int layer, int direction)
{
    curDirection = direction;
    curLayer = layer;
    curPosition = point;
    
    Map4 * map= ((World4 *)parentWorld)->getMap4();
    
    map->element(curLayer,curPosition.y, curPosition.x)->addRobot(this);
    
    Sprite::setPosition( map->Coordinates(curPosition.x, curPosition.y, -15, 35) );
    parts.at(0)->setPosition(map->Coordinates(lampPosition.x, lampPosition.y, -15, 35));
}

void Lamp4::interact(AbstractRobot* robot, const std::function<void(AbstractRobot *)> &callback)
{
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    
    turnState = (TurnState)(1-turnState);
    parts.at(0)->setSpriteFrame(cache->getSpriteFrameByName(StringUtils::format("lamp4-%d.png" , turnState)));
    callback(this);
}

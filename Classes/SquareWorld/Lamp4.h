//
//  Furnace.h
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 3/14/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__Furnace__
#define __pictomir_cocos2d_x__Furnace__

#include "EnvRobot2D.h"

class Lamp4 : public EnvRobot2D
{
public:
    enum TurnState
    {
        Off = 0,
        On = 1
    };
    
    Lamp4():
        EnvRobot2D(nullptr),
        turnState(Off)
    {}
    
    static Lamp4* create()
    {
        return new Lamp4();
    }
    
    Lamp4(AbstractWorld *parent):
        EnvRobot2D(parent),
        turnState(Off)
    {}
    
    virtual ~Lamp4()
    {
        //release();
    }
    
    virtual void release() override
    {
        Sprite::release();
    }
    
    static Lamp4* create(AbstractWorld *parent)
    {
        Lamp4* lamp = new Lamp4(parent);
        
        lamp->init();
        lamp->autorelease();
        
        return lamp;
    }
    
    SYNTHESIZE(TurnState, turnState, TurnState);
    SYNTHESIZE(cocos2d::Point, lampPosition, LampPosition);
    
    virtual bool init();
    //virtual void initWithXMLNode(xmlNodePtr robotNode);
    
    void setPosition(cocos2d::Point point, int layer, int direction);
    
    virtual void interact (AbstractRobot* robot, const std::function<void(AbstractRobot *)> &callback = std::function<void(AbstractRobot *)>()) override;
    
    virtual bool interactable() override
    {
        return true;
    }
    
    virtual RobotType type () override
    {
        return pmLamp4;
    }
    
    virtual int getNativeMethodCount()
    {
        return 0;
    }
    
    virtual int getConditionsCount()
    {
        return 0;
    }
    
    virtual void resetRobot() override;
    
    virtual void initTexture() override;
    
    virtual bool canMoveOn() override
    {
        return false;
    }

    
};

#endif /* defined(__pictomir_cocos2d_x__Furnace__) */

//
//  Map4.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__Map4__
#define __pictomir_cocos2d_x__Map4__

#include <iostream>
#include "AbstractMap.h"
#include <vector>
#include "utils.h"
#include <string.h>
#include "ProgramLayer.h"
#include "Task.h"
#include "Hints.h"
#include "ControlLayer4.h"
#include "PMTextureCache.h"

#ifdef MAP_EDITOR
#include "mapEditorsMenu.h"
#endif

class World4;

enum Map4Tiles
{
    MAP4_ROBOT_START = 1,
    MAP4_GREEN_GRASS = 2,
    MAP4_BLUE_GRASS = 3,
    MAP4_UNPAINT_GRASS = 4,
    MAP4_PAINT_GRASS = 5,

    MAP4_ST2_WALL = 7,
    MAP4_ST3_WALL = 6,
    MAP4_ST23_WALL = 8
};

class Map4 : public AbstractMap
{
    friend class XmlWorldReader;
    friend class XMLWorldSaver;
    
public:
    
    enum Direction
    {
        Down = 0,
        Right,
        Up,
        Left
    };
    
    Map4() :
        AbstractMap(),
        mapElementSize(cocos2d::Size(64,64)),
        paintedGrassCount(0),
        width(0),
        height(0),
        tileSet(0),
        grassCount(0)
    {}
    
    
    Map4(World4* parent,int width, int height, int layerNum, int tileset):
        AbstractMap((AbstractWorld *)parent),
        mapElementSize(cocos2d::Size(64,64)),
        paintedGrassCount(0),
        width(width),
        height(height),
        tileSet(tileset),
        grassCount(0)
    {
        mapElements.resize(layerNum);
    }
    
    static Map4 *create();
    static Map4 *create(World4* parent,int width, int height, int layerNum, int tileset);
    
    ~Map4()
    {

    }
    
    cocos2d::Size mapElementSize;
    
    SYNTHESIZE(int, paintedGrassCount, PaintedGrassCount);
    SYNTHESIZE(int, width, Width);
    SYNTHESIZE(int, height, Height);
    SYNTHESIZE(int, tileSet, TileSet);
    SYNTHESIZE(int, grassCount, GrassCount);
    SYNTHESIZE(ControlLayer4 *,controlLayer,ControlLayer);
    
    bool init();
    
    virtual void drawMap(bool previewDraw);
    virtual void update();
    virtual void clean();
    virtual void centerMap();
    
    virtual float getRealWidth() const {
        return width * mapElementSize.width;
    }
    
    virtual float getRealHeight() const {
        return height * mapElementSize.height;
    }
    
    World4 *world()
    {
        return (World4 *)_world;
    }
    
    void incPaintedGrass()
    {
        ++paintedGrassCount;
    }
    
    cocos2d::Point Coordinates(int x, int y, double dx, double dy);
    
    void incGrassCount()
    {
        ++grassCount;
    }
    
    void decGrassCount()
    {
        --grassCount;
    }
    
    virtual void setEnabled(bool flag);
    
    virtual MapType type()
    {
        return pmMap4;
    }

    Sprite* resizeSprite;
    
    void resize (int width_, int height_);
    void changeLayers (int layers);
    void deleteMapElement (MapElement* mapElement,int layer);
    void drawMapElement (MapElement* mapElement,int layer);
    
    void addUpWall(Point, int);
    void addLeftWall(Point, int);
    void deleteUpWall(Point, int);
    void deleteLeftWall(Point, int);
    void switchUpWall(Point, int);
    void switchLeftWall(Point, int);
    
    cocos2d::Point xyAtTouch (cocos2d::Point point, int layer);
    cocos2d::Point coordAtTouch (cocos2d::Point point, int layer);
    void changeElementTypeAtPoint (cocos2d::Point, int layer);
    cocos2d::Point dxdyAtPoint(cocos2d::Point point);
    cocos2d::Point coordAtPoint(cocos2d::Point point);
    
    int resizeAtTouch(cocos2d::Point point);
    
    void drawNewRobot (PlayerRobot2D * robot);
    
private:    
    int getWallZOrder(int x, int y, int layer);
};

#endif /* defined(__pictomir_cocos2d_x__Map4__) */

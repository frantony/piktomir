//
//  Robot4.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__Robot4__
#define __pictomir_cocos2d_x__Robot4__

#include "PlayerRobot2D.h"

#include "PaintTask.h"
#include "PositionTask.h"

int sign (int a) ;

class Robot4 : public PlayerRobot2D
{
private:
    cocos2d::ParticleFireworks *fireWorks;
public:
    
    ProgramLayer* program;
    
    virtual bool init();
    virtual void initTexture();
    
    Robot4(AbstractWorld *parent):
        PlayerRobot2D(parent)
    {}
    
    virtual ~Robot4()
    {
        //release();
    }
    
    virtual void release() override
    {
        Sprite::release();
    }
    
    static Robot4* create(AbstractWorld *parent)
    {
        Robot4 *robot = new Robot4(parent);
        
        robot->init();
        robot->autorelease();
        
        return robot;
    }
    
    enum Condition
    {
        GrassIsGreen = 1,
        GrassIsBlue = 2,
        NoWall = 3,
        Wall = 4
    };
    
    enum NativeMethod 
    {
        Move = 3,
        TurnLeft = 1,
        TurnRight = 2,
        Paint = 4,
        MoveReverse = 5,
        UnPaint = 6
    };

    virtual bool checkCondition(int condition);
    bool isNearWall();
    
    virtual void resetRobot() override;
    void setPosition(cocos2d::Point point, int layer, int state);
    bool isGrassAt(cocos2d::Point point, int layer);
    
//real moving
    bool canMoveTo(int x, int y, int direction);
    void moveTo(int x, int y, bool reverse);
    
    void endMove();
    void endPaint();
    void endUnPaint();
    void endTurn();
    void endInteract(AbstractRobot *robot);
    
    virtual void destroy();
    virtual void win();
    
    virtual RobotType type ()
    {
        return pmRobot4;
    }
    
    void move();
    void moveReverse();
    void turnLeft();
    void turnRight();
    void paint();
    void unPaint();
    
    virtual int getNativeMethodCount()
    {
        return 5;
    }
    
    virtual int getConditionsCount()
    {
        return 4;
    }
    
    void incStepCount();
};

#endif /* defined(__pictomir_cocos2d_x__Robot4__) */

//
//  Task.h
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 2/22/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__Task__
#define __pictomir_cocos2d_x__Task__

#include <cocos2d.h>
#include "PMSettings.h"
#include <memory>

class AbstractRobot;

#define CREATE_TASK_DATA(type,...) std::make_shared<type>(__VA_ARGS__)

enum TaskType
{
    pmPaintTask,
    pmPositionTask
};

struct _task_update_data
{
    _task_update_data(TaskType taskType):
        taskType(taskType)
    {}
    
    TaskType taskType;
};

class Task
{
private:
    AbstractRobot *robot;
public:
    Task (AbstractRobot *robot):
        robot(robot)
    {}
    
    virtual bool complete() = 0;
    virtual void clear() = 0;
    virtual void update(const std::shared_ptr<_task_update_data> &data) = 0;
    
    virtual ~Task()
    {
        
    }
};

#endif /* defined(__pictomir_cocos2d_x__Task__) */

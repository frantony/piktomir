//
//  TeacherActionLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Admin on 05/02/14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#include "TeacherActionLayer.h"

USING_NS_CC;
using namespace std;

bool TeacherActionLayer::init()
{
    if ( !LayerColor::initWithColor(Color4B(100,100,100,200), TAL_STARTGAME_SECTIONSIZE + TAL_GROUPACTIONS_SECTIONSIZE, TAL_HEIGHT) )
    {
        return false;
    }

    Label *headerLabel = Label::create("Действия", "Arial", 18);
    headerLabel->setColor(Color3B(255, 0, 0));
    
    headerLabel->setAnchorPoint( Point::ZERO );
    headerLabel->setPosition( Point(10,getContentSize().height - headerLabel->getContentSize().height) );
    
    addChild(headerLabel);
    
    headerLabelHeight = headerLabel->getContentSize().height;
    
    MenuItemFont *startGameButton = MenuItemFont::create("Начать!", CC_CALLBACK_1(TeacherActionLayer::startGames, this));
    startGameButton->setAnchorPoint( Point::ZERO );
    startGameButton->setPosition( Point::ZERO );
    
    Menu *startMenu = Menu::create(startGameButton,nullptr);
    startMenu->setContentSize(Size(startGameButton->getContentSize().width,startGameButton->getContentSize().height));
    
    startMenu->setPosition(Point(TAL_STARTGAME_SECTIONSIZE/2 - startMenu->getContentSize().width/2,
                           getContentSize().height/2 - startMenu->getContentSize().height/2));
    
    addChild(startMenu);

    float buttonY  = 0;
    
    MenuItemFont *deleteFromGroupButton = MenuItemFont::create("Удалить из группы",
                                                               CC_CALLBACK_1(TeacherActionLayer::deleteFromGroup, this));
    deleteFromGroupButton->setFontNameObj("Arial");
    deleteFromGroupButton->setFontSizeObj(18);
    
    deleteFromGroupButton->setAnchorPoint( Point::ZERO );
    deleteFromGroupButton->setPosition( Point(0,buttonY) );
    
    buttonY += deleteFromGroupButton->getContentSize().height;
    
    MenuItemFont *addToGroupButton = MenuItemFont::create("Добавить в группу",
                                                          CC_CALLBACK_1(TeacherActionLayer::addToGroup, this));
    addToGroupButton->setFontNameObj("Arial");
    addToGroupButton->setFontSizeObj(18);
    
    addToGroupButton->setAnchorPoint( Point::ZERO );
    addToGroupButton->setPosition( Point(0,buttonY) );
    
    buttonY += addToGroupButton->getContentSize().height;
    
    MenuItemFont *deleteGroupButton = MenuItemFont::create("Удалить группу(ы)",
                                                           CC_CALLBACK_1(TeacherActionLayer::deleteGroup, this));
    deleteGroupButton->setFontNameObj("Arial");
    deleteGroupButton->setFontSizeObj(18);
    
    deleteGroupButton->setAnchorPoint( Point::ZERO );
    deleteGroupButton->setPosition( Point(0,buttonY) );
    
    buttonY += deleteGroupButton->getContentSize().height;
    
    MenuItemFont *createGroupButton = MenuItemFont::create("Объединить в группу",
                                                           CC_CALLBACK_1(TeacherActionLayer::createGroup, this));
    createGroupButton->setFontNameObj("Arial");
    createGroupButton->setFontSizeObj(18);
    
    createGroupButton->setAnchorPoint( Point::ZERO );
    createGroupButton->setPosition( Point(0,buttonY) );
    
    buttonY += createGroupButton->getContentSize().height;
    

    
    Menu *groupMenu = Menu::create(createGroupButton, deleteGroupButton, addToGroupButton, deleteFromGroupButton, nullptr);
    groupMenu->setContentSize(Size(startGameButton->getContentSize().width,buttonY));
    
    groupMenu->setPosition(Point(TAL_STARTGAME_SECTIONSIZE + TAL_STARTGAME_SECTIONSIZE/2 - groupMenu->getContentSize().width/2,
                                getContentSize().height/2 - groupMenu->getContentSize().height/2));
    
    addChild(groupMenu);
    

    return true;
}

bool TeacherActionLayer::initWithControlLayers(ClientListLayer *freeClientListLayer_,ClientListLayer *playingClientListLayer_,TeacherMapSelectLayer *mapSelectLayer_)
{
    if(!init())
        return false;
    
    //PMManager::instance()->disable();
    
    freeClientListLayer = freeClientListLayer_;
    playingClientListLayer = playingClientListLayer_;
    mapSelectLayer = mapSelectLayer_;
    
    PMServerHost::instance()->registerRefreshUIFunc(this, (CLIENT_CONN_FUNCTION)&TeacherActionLayer::addClient, (CLIENT_CONN_FUNCTION)&TeacherActionLayer::removeClient);
    
    return true;
}

void TeacherActionLayer::startGames(cocos2d::Ref *sender)
{
    Map<int,ClientLayer *> clients = freeClientListLayer->getClientList();
    Vector<ClientGroupLayer *> groups  = freeClientListLayer->getGroups();
    
    std::vector<PMClient> sendList;
    
    for(auto &i : clients)
    {
        if(i.second->getChecked())
        {
            
            for(auto client = PMServerHost::instance()->getClients()->begin(); client != PMServerHost::instance()->getClients()->end(); client++)
            {
                if(i.first == client->getId())
                {
                    sendList.push_back(*client);
                }
            }
            
            PMServerHost::instance()->sendInitSingleplayerGame(sendList, mapSelectLayer->getSinglePlayerWorldPart(), mapSelectLayer->getSinglePlayerLevel());
        }
        PMServerHost::instance()->flush();
    }
    
    int robot_count = 0;
    
    for(auto &k : groups)
    {
        if(k->getChecked())
        {
            ENetAddress multiplayerServerAddress;
            clients = k->getClients();
            for(Map<int,ClientLayer *>::const_iterator i = clients.begin(); i != clients.end(); i++)
            {
                if(i == clients.begin())
                {
                    for(auto client = PMServerHost::instance()->getClients()->begin(); client != PMServerHost::instance()->getClients()->end(); client++)
                    {
                        if(i->first == client->getId())
                        {
                            multiplayerServerAddress = client->peer_->address;
                            
                            int worldPart = mapSelectLayer->getMultiPlayerWorldPart();
                            int level = mapSelectLayer->getMultiPlayerLevel();
                            
                            int clientsNedeed = PMSettings::instance()->getNetMapsInfo().at(worldPart).levelInfo.at(level).robotNum;
                            
                            PMServerHost::instance()->sendInitMultiplayerGame(*client, clientsNedeed, worldPart, level);
                        }
                    }
                }
                
                
                for(auto client = PMServerHost::instance()->getClients()->begin(); client != PMServerHost::instance()->getClients()->end(); client++)
                {
                    if(i->first == client->getId())
                    {
                        PMServerHost::instance()->sendConnectMultiplayer(*client, multiplayerServerAddress, robot_count);
                        CCLOG("Robot number: %d", robot_count);
                        robot_count++;
                    }
                }
            }
        }
        PMServerHost::instance()->flush();
    }
    
    setPlayingClients();
}

void TeacherActionLayer::createGroup(cocos2d::Ref *sender)
{
    freeClientListLayer->createGroup();
}

void TeacherActionLayer::deleteGroup(cocos2d::Ref *sender)
{
    freeClientListLayer->deleteGroups();
}

void TeacherActionLayer::addToGroup(cocos2d::Ref *sender)
{
    freeClientListLayer->addToGroup();
}

void TeacherActionLayer::deleteFromGroup(cocos2d::Ref *sender)
{
    freeClientListLayer->deleteFromGroup();
}

void TeacherActionLayer::addClient(PMClient *client)
{
    freeClientListLayer->addClient(client);
}

void TeacherActionLayer::removeClient(PMClient *client)
{
    freeClientListLayer->removeClient(client);
    playingClientListLayer->removeClient(client);
}

void TeacherActionLayer::setPlayingClients()
{
    Map<int,ClientLayer *>& clients = freeClientListLayer->getClientList();
    Vector<ClientGroupLayer *>& groups  = freeClientListLayer->getGroups();
    
    for(auto i = clients.begin(); i != clients.end();)
    {
        if(i->second->getChecked())
        {
            i->second->forceSetChecked(false);
            i->second->setGame(mapSelectLayer->getSinglePlayerWorldPart(), mapSelectLayer->getSinglePlayerLevel());
            playingClientListLayer->addClient(i->first, i->second);
            i = clients.erase(i);
        }
        else
        {
            ++i;
        }
    }
    
    for(auto j = groups.begin(); j != groups.end();)
    {
        if((*j)->getChecked())
        {
            (*j)->forceSetChecked(false);
            (*j)->setGame(mapSelectLayer->getMultiPlayerWorldPart(), mapSelectLayer->getMultiPlayerLevel());
            playingClientListLayer->addGroup(*j);
            
            j = groups.erase(j);
        }
        else
        {
            ++j;
        }
    }
    
    freeClientListLayer->drawClients();
    playingClientListLayer->drawClients();
}

void TeacherActionLayer::draw(cocos2d::Renderer* renderer, const kmMat4 &transform, bool transformUpdated)
{
    lineDrawCommand.init(_globalZOrder);
    lineDrawCommand.func = CC_CALLBACK_0(TeacherActionLayer::onDraw, this, transform, transformUpdated);
    Director::getInstance()->getRenderer()->addCommand(&lineDrawCommand);
    
    LayerColor::draw(renderer, transform, transformUpdated);
}

void TeacherActionLayer::onDraw(const kmMat4 &transform, bool transformUpdated)
{
    LayerColor::onDraw(transform, transformUpdated);
    
    kmMat4 oldMat;
    kmGLGetMatrix(KM_GL_MODELVIEW, &oldMat);
    kmGLLoadMatrix(&_modelViewTransform);
    
    DrawPrimitives::setDrawColor4F(255,255,255,255);
    glLineWidth(1.5f);
    
    float startX = TAL_STARTGAME_SECTIONSIZE;
    float endX = startX;
    float startY = 0;
    float endY = getContentSize().height - headerLabelHeight;
    
    DrawPrimitives::drawLine(Point(startX,startY ), Point(endX,endY));
    
    kmGLLoadMatrix(&oldMat);
}
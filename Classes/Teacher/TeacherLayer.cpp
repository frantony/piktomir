//
//  TeacherLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 07.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "TeacherLayer.h"
#include "PMSettings.h"
#include "MainMenuLayer.h"
#include "utils.h"
USING_NS_CC;

Scene* TeacherLayer::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    TeacherLayer *layer = TeacherLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer,0,0);
    
    // return the scene
    return scene;
}


bool TeacherLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

    setPosition( Point(0,0) );
    
    Sprite *backGround  = Sprite::create(PICT_PATH "Background/background1.png");
    backGround->setPosition( Point(PMSettings::instance()->getScreenSize().width/2, PMSettings::instance()->getScreenSize().height/2) );
    resizeSprite(backGround, PMSettings::instance()->getScreenSize().width, PMSettings::instance()->getScreenSize().height);
    addChild(backGround, -1);
    
    Label *freeLabel = Label::create("Свободные ученики", "Arial", 18);
    freeLabel->setColor( Color3B(255,0,0) );
    freeLabel->setAnchorPoint( Point::ZERO );
    freeLabel->setPosition( Point(CLIENTLIST_BOUND, PMSettings::instance()->getScreenSize().height - CLIENTLIST_BOUND - freeLabel->getContentSize().height) );
    
    Label *playingLabel = Label::create("Играющие ученики", "Arial", 18);
    playingLabel->setColor( Color3B(255,0,0) );
    playingLabel->setAnchorPoint( Point::ZERO );
    playingLabel->setPosition( Point(CLIENTLIST_BOUND, PMSettings::instance()->getScreenSize().height/2 - freeLabel->getContentSize().height) );
    
    addChild(freeLabel,1);
    addChild(playingLabel,1);
    
    LayerColor *colorLayer = LayerColor::create(Color4B(100,100,100,125),
                                                    400 - 2 * CLIENTLIST_BOUND, PMSettings::instance()->getScreenSize().height/2 - 3 * CLIENTLIST_BOUND);
    addChild(colorLayer,1);
    colorLayer->setPosition( Point(CLIENTLIST_BOUND, PMSettings::instance()->getScreenSize().height/2 + CLIENTLIST_BOUND) );
    
    LayerColor *playingColorLayer = LayerColor::create(Color4B(100,100,100,125),
                                                           400 - 2 * CLIENTLIST_BOUND, PMSettings::instance()->getScreenSize().height/2 - 2 * CLIENTLIST_BOUND);
    addChild(playingColorLayer,1);
    playingColorLayer->setPosition( Point(CLIENTLIST_BOUND, CLIENTLIST_BOUND) );
    
    ClientListLayer * freeClientListLayer = ClientListLayer::create(PMSettings::instance()->getScreenSize().height/2 - 3 * CLIENTLIST_BOUND);
    colorLayer->addChild(freeClientListLayer);
    
    ClientListLayer * playingClientListLayer = ClientListLayer::create(PMSettings::instance()->getScreenSize().height/2 - 2 * CLIENTLIST_BOUND);
    playingColorLayer->addChild(playingClientListLayer);
    
    TeacherMapSelectLayer* teacherMapLayer = TeacherMapSelectLayer::create();
    
    teacherMapLayer->setAnchorPoint(Point::ZERO);
    
    TeacherActionLayer *actionLayer = TeacherActionLayer::create(freeClientListLayer,playingClientListLayer,teacherMapLayer);
    actionLayer->setPosition(Point(420,
                                 PMSettings::instance()->getScreenSize().height - 200));
    
    teacherMapLayer->setPosition(Point(420,PMSettings::instance()->getScreenSize().height - actionLayer->getContentSize().height - 210));
    
    addChild(teacherMapLayer);
    addChild(actionLayer);

    MenuItemFont *exit = MenuItemFont::create(LocalizedString("BackToMainMenu"),
                                              CC_CALLBACK_1(TeacherLayer::mainMenu,this));
    exit->setPosition( Point::ZERO );
    exit->setAnchorPoint( Point::ZERO );
    
    Menu *exitMenu = Menu::create(exit, nullptr);
    exitMenu->setContentSize(exit->getContentSize());
    exitMenu->setPosition( Point(getContentSize().width - exit->getContentSize().width - 20.0,
                                   exit->getContentSize().height + 20.0) );
    
    
    addChild(exitMenu, 1);
    
    //PMManager::instance()->disable();
    
    return true;
}

void TeacherLayer::mainMenu(cocos2d::Ref *pObj)
{
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, MainMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
    //PMServerHost::destroy();
    //PMManager::instance()->enable();
}
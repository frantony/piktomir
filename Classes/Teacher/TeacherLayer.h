//
//  TeacherLayer.h
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 07.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__TeacherLayer__
#define __pictomir_cocos2d_x__TeacherLayer__

#include <iostream>
#include "cocos2d.h"
#include "PMServerHost.h"

#include "TeacherActionLayer.h"


class TeacherLayer:public cocos2d::Layer
{
public:
    virtual bool init();
    
    CREATE_FUNC(TeacherLayer);
    
    static cocos2d::Scene* scene();
    
    ~TeacherLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
private:
    
    void mainMenu(cocos2d::Ref *pObj);
};

#endif /* defined(__pictomir_cocos2d_x__TeacherLayer__) */

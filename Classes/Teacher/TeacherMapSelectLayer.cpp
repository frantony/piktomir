 //
//  TeacherMapSelectLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Admin on 30/01/14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#include "TeacherMapSelectLayer.h"

USING_NS_CC;
using namespace std;

bool TeacherMapSelectLayer::init()
{
    if(!LayerColor::initWithColor( Color4B(100,100,100,200) ))
    {
        return false;
    }
    
    vector<string> worldVector;
    vector<string> levelVector;
    
    singlePlayerWorldPart = 0;
    singlePlayerLevel = 0;
    multiPlayerWorldPart = 0;
    multiPlayerLevel = 0;
    
    setContentSize(Size(2 * MAPSELECT_SECTION_SIZE + MAPSELECT_SECTION_SEPARATOR, MAPSELECT_SECTION_SIZE));
    
    Label *headerLabel = Label::create("Выбор карты", "Arial", 18);
    
    headerLabel->setColor(Color3B(255, 0, 0));
    
    headerLabel->setAnchorPoint( Point::ZERO );
    headerLabel->setPosition( Point(10,getContentSize().height - headerLabel->getContentSize().height) );
    
    addChild(headerLabel);
    
    headerLabelHeight = headerLabel->getContentSize().height;
    
    for(int i = 0; i < PMSettings::instance()->getWorldsCount(); ++i)
        worldVector.push_back(StringUtils::format(LocalizedString("WorldPattern").c_str(), i+1));
    
    for(int i = 0; i < PMSettings::instance()->getMapsCount(0); ++i)
        levelVector.push_back(string("Карта") + to_string((PLATFORM_INT_FOR_TO_STRING)i+1));
    
    singlePlayerWorldBox = PMComboBox::create(worldVector, this, menu_selector(TeacherMapSelectLayer::singlePlayerWorldBoxOnShow),
                                              menu_selector(TeacherMapSelectLayer::singlePlayerWorldBoxOnEnd),
                                              menu_selector(TeacherMapSelectLayer::singlePlayerWorldBoxOnClose),
                                              18, "Arial");
    
    singlePlayerLevelBox = PMComboBox::create(levelVector, this, menu_selector(TeacherMapSelectLayer::singlePlayerLevelBoxOnShow),
                                              menu_selector(TeacherMapSelectLayer::singlePlayerLevelBoxOnEnd),
                                              menu_selector(TeacherMapSelectLayer::singlePlayerLevelBoxOnClose),
                                              18, "Arial");
    
    Label *singleLabel = Label::create("Одиночная", "Arial", 18);
    

    singlePlayerLevelBox->setAnchorPoint(Point(0.5,0.5));
    singlePlayerLevelBox->setPosition(Point(MAPSELECT_SECTION_SIZE/2 - singlePlayerLevelBox->getContentSize().width/2,
                              MAPSELECT_SECTION_SIZE/2 - singlePlayerLevelBox->getContentSize().height/2 - headerLabelHeight - 5));
    
    singlePlayerWorldBox->setAnchorPoint(Point(0.5,0.5));
    singlePlayerWorldBox->setPosition( Point(MAPSELECT_SECTION_SIZE/2 - singlePlayerWorldBox->getContentSize().width/2,
                               MAPSELECT_SECTION_SIZE/2 + singlePlayerWorldBox->getContentSize().height/2 - headerLabelHeight + 5) );
    
    singleLabel->setAnchorPoint(Point::ZERO);
    singleLabel->setPosition( Point(10, getContentSize().height - singleLabel->getContentSize().height - headerLabelHeight - 10) );
    
    addChild(singlePlayerWorldBox,1);
    addChild(singlePlayerLevelBox,0);
    addChild(singleLabel);
    
    worldVector.clear();
    levelVector.clear();
    
    for(int i = 0; i < PMSettings::instance()->getNetMapsInfo().size(); ++i)
        worldVector.push_back(StringUtils::format(LocalizedString("WorldPattern").c_str(), i+1));
    
    for(int i = 0; i < PMSettings::instance()->getNetMapsInfo().at(0).levelNum; ++i)
        levelVector.push_back(string("Карта") + to_string((PLATFORM_INT_FOR_TO_STRING)i+1));
    
    multiPlayerWorldBox = PMComboBox::create(worldVector, this, menu_selector(TeacherMapSelectLayer::multiPlayerWorldBoxOnShow),
                                              menu_selector(TeacherMapSelectLayer::multiPlayerWorldBoxOnEnd),
                                              menu_selector(TeacherMapSelectLayer::multiPlayerWorldBoxOnClose),
                                              18, "Arial");
    
    multiPlayerLevelBox = PMComboBox::create(levelVector, this, menu_selector(TeacherMapSelectLayer::multiPlayerLevelBoxOnShow),
                                              menu_selector(TeacherMapSelectLayer::multiPlayerLevelBoxOnEnd),
                                              menu_selector(TeacherMapSelectLayer::multiPlayerLevelBoxOnClose),
                                              18, "Arial");
    
    Label *multiLabel = Label::create("Multiplayer", "Arial", 18);
    
    float shiftX = MAPSELECT_SECTION_SIZE + MAPSELECT_SECTION_SEPARATOR;
    
    multiPlayerLevelBox->setAnchorPoint(Point(0.5,0.5));
    multiPlayerLevelBox->setPosition(Point(shiftX + MAPSELECT_SECTION_SIZE/2 - multiPlayerLevelBox->getContentSize().width/2,
                                          MAPSELECT_SECTION_SIZE/2 - multiPlayerLevelBox->getContentSize().height/2 - headerLabelHeight - 5));
    
    multiPlayerWorldBox->setAnchorPoint(Point(0.5,0.5));
    multiPlayerWorldBox->setPosition( Point(shiftX + MAPSELECT_SECTION_SIZE/2 - multiPlayerWorldBox->getContentSize().width/2,
                                           MAPSELECT_SECTION_SIZE/2 + multiPlayerWorldBox->getContentSize().height/2 - headerLabelHeight + 5) );
    
    multiLabel->setAnchorPoint(Point::ZERO);
    multiLabel->setPosition( Point(shiftX + 10, getContentSize().height - singleLabel->getContentSize().height - headerLabelHeight - 10) );
    
    addChild(multiPlayerWorldBox,1);
    addChild(multiPlayerLevelBox,0);
    addChild(multiLabel);
    
    return true;
}

void TeacherMapSelectLayer::singlePlayerWorldBoxOnShow(cocos2d::Ref *obj)
{
    singlePlayerLevelBox->setEnabled(false);
}

void TeacherMapSelectLayer::singlePlayerWorldBoxOnEnd(cocos2d::Ref *obj)
{
     vector<string> levelVector;
    
    removeChild(singlePlayerLevelBox);
    
    singlePlayerWorldPart = singlePlayerWorldBox->getSelectedItemIndex();
    
    for(int i = 0; i < PMSettings::instance()->getMapsCount(singlePlayerWorldBox->getSelectedItemIndex()); ++i)
        levelVector.push_back(string("Карта") + to_string((PLATFORM_INT_FOR_TO_STRING)i+1));
    
    singlePlayerLevelBox = PMComboBox::create(levelVector, this, menu_selector(TeacherMapSelectLayer::singlePlayerLevelBoxOnShow),
                                              menu_selector(TeacherMapSelectLayer::singlePlayerLevelBoxOnEnd),
                                              menu_selector(TeacherMapSelectLayer::singlePlayerLevelBoxOnClose),
                                              18, "Arial");
    
    singlePlayerLevelBox->setAnchorPoint(Point(0.5,0.5));
    singlePlayerLevelBox->setPosition(Point(MAPSELECT_SECTION_SIZE/2 - singlePlayerLevelBox->getContentSize().width/2,
                              MAPSELECT_SECTION_SIZE/2 - singlePlayerLevelBox->getContentSize().height/2 - headerLabelHeight - 5));
    
    addChild(singlePlayerLevelBox,0);
}

void TeacherMapSelectLayer::singlePlayerWorldBoxOnClose(cocos2d::Ref *obj)
{
    singlePlayerLevelBox->setEnabled(true);
}

void TeacherMapSelectLayer::singlePlayerLevelBoxOnShow(cocos2d::Ref *obj)
{
    singlePlayerWorldBox->setEnabled(false);
}

void TeacherMapSelectLayer::singlePlayerLevelBoxOnEnd(cocos2d::Ref *obj)
{
    singlePlayerLevel = singlePlayerLevelBox->getSelectedItemIndex();
    singlePlayerWorldBox->setEnabled(true);
}

void TeacherMapSelectLayer::singlePlayerLevelBoxOnClose(cocos2d::Ref *obj)
{
    singlePlayerWorldBox->setEnabled(true);
}

void TeacherMapSelectLayer::multiPlayerWorldBoxOnShow(cocos2d::Ref *obj)
{
    multiPlayerLevelBox->setEnabled(false);
}

void TeacherMapSelectLayer::multiPlayerWorldBoxOnEnd(cocos2d::Ref *obj)
{
    vector<string> levelVector;
    
    removeChild(multiPlayerLevelBox);
    
    multiPlayerWorldPart = multiPlayerWorldBox->getSelectedItemIndex();
    
    for(int i = 0; i < PMSettings::instance()->getNetMapsInfo().at(multiPlayerWorldBox->getSelectedItemIndex()).levelNum; ++i)
        levelVector.push_back(string("Карта") + to_string((PLATFORM_INT_FOR_TO_STRING)i+1));
    
    float shiftX = MAPSELECT_SECTION_SIZE + MAPSELECT_SECTION_SEPARATOR;
    
    multiPlayerLevelBox = PMComboBox::create(levelVector, this, menu_selector(TeacherMapSelectLayer::multiPlayerLevelBoxOnShow),
                                              menu_selector(TeacherMapSelectLayer::multiPlayerLevelBoxOnEnd),
                                              menu_selector(TeacherMapSelectLayer::multiPlayerLevelBoxOnClose),
                                              18, "Arial");
    
    multiPlayerLevelBox->setAnchorPoint(Point(0.5,0.5));
    multiPlayerLevelBox->setPosition(Point(shiftX + MAPSELECT_SECTION_SIZE/2 - multiPlayerLevelBox->getContentSize().width/2,
                                          MAPSELECT_SECTION_SIZE/2 - multiPlayerLevelBox->getContentSize().height/2 - headerLabelHeight - 5));
    
    addChild(multiPlayerLevelBox,0);
}

void TeacherMapSelectLayer::multiPlayerWorldBoxOnClose(cocos2d::Ref *obj)
{
    multiPlayerLevelBox->setEnabled(true);
}

void TeacherMapSelectLayer::multiPlayerLevelBoxOnShow(cocos2d::Ref *obj)
{
    multiPlayerWorldBox->setEnabled(false);
}

void TeacherMapSelectLayer::multiPlayerLevelBoxOnEnd(cocos2d::Ref *obj)
{
    multiPlayerLevel = multiPlayerLevelBox->getSelectedItemIndex();
    multiPlayerWorldBox->setEnabled(true);
}

void TeacherMapSelectLayer::multiPlayerLevelBoxOnClose(cocos2d::Ref *obj)
{
    multiPlayerWorldBox->setEnabled(true);
}

void TeacherMapSelectLayer::draw(cocos2d::Renderer* renderer, const kmMat4 &transform, bool transformUpdated)
{    
    DrawPrimitives::init();
    DrawPrimitives::setDrawColor4F(255,255,255,255);
    glLineWidth(1.5f);
    
    LayerColor::draw(renderer, transform, transformUpdated);
    
    float startX = getContentSize().width/2;
    float endX = startX;
    float startY = 0;
    float endY = getContentSize().height - headerLabelHeight - 10;
    
    DrawPrimitives::drawLine(Point(startX,startY ), Point(endX,endY));
}

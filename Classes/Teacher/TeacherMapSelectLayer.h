//
//  TeacherMapSelectLayer.h
//  pictomir-cocos2d-x
//
//  Created by Admin on 30/01/14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__TeacherMapSelectLayer__
#define __pictomir_cocos2d_x__TeacherMapSelectLayer__

#include <iostream>

#include "cocos2d.h"
#include "cocos-ext.h"
#include "GameLayer.h"

#include "PMCombobox.h"

const float MAPSELECT_SECTION_SIZE = 150.0;
const float MAPSELECT_SECTION_SEPARATOR = 10.0;
class TeacherMapSelectLayer :public cocos2d::LayerColor
{
public:
    SYNTHESIZE(int, singlePlayerWorldPart, SinglePlayerWorldPart);
    SYNTHESIZE(int, singlePlayerLevel, SinglePlayerLevel);
    SYNTHESIZE(int, multiPlayerWorldPart, MultiPlayerWorldPart);
    SYNTHESIZE(int, multiPlayerLevel, MultiPlayerLevel);
    
    virtual bool init();
    
    CREATE_FUNC(TeacherMapSelectLayer);
    
    ~TeacherMapSelectLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
private:
    
    float headerLabelHeight;
    
    PMComboBox *singlePlayerWorldBox;
    PMComboBox *singlePlayerLevelBox;
    PMComboBox *multiPlayerWorldBox;
    PMComboBox *multiPlayerLevelBox;
    
    void singlePlayerWorldBoxOnShow(cocos2d::Ref *obj);
    void singlePlayerWorldBoxOnEnd(cocos2d::Ref *obj);
    void singlePlayerWorldBoxOnClose(cocos2d::Ref *obj);
    void singlePlayerLevelBoxOnShow(cocos2d::Ref *obj);
    void singlePlayerLevelBoxOnEnd(cocos2d::Ref *obj);
    void singlePlayerLevelBoxOnClose(cocos2d::Ref *obj);
    
    void multiPlayerWorldBoxOnShow(cocos2d::Ref *obj);
    void multiPlayerWorldBoxOnEnd(cocos2d::Ref *obj);
    void multiPlayerWorldBoxOnClose(cocos2d::Ref *obj);
    void multiPlayerLevelBoxOnShow(cocos2d::Ref *obj);
    void multiPlayerLevelBoxOnEnd(cocos2d::Ref *obj);
    void multiPlayerLevelBoxOnClose(cocos2d::Ref *obj);
    
    void draw(cocos2d::Renderer* renderer, const kmMat4 &transform, bool transformUpdated);
};

#endif /* defined(__pictomir_cocos2d_x__TeacherMapSelectLayer__) */

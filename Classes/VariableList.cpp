//
//  VariableList.cpp
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 09.10.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "VariableList.h"

VariableList *VariableList::_instanse = nullptr;

VariableList *VariableList::instanse()
{
    if(_instanse == nullptr)
        _instanse = new VariableList();
    
    return _instanse;
}

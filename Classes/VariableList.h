//
//  VariableList.h
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 09.10.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__VariableList__
#define __pictomir_cocos2d_x__VariableList__

#include <iostream>
#include <vector>

typedef size_t value_tag;

class VariableList
{
private:
    
    std::vector<int> variables;
    unsigned int reservedTagsNum;
    
public:
    
	VariableList() :
		variables(std::vector<int>()),
		reservedTagsNum(0)
	{}

    value_tag addVariable(bool reserved)
    {
        variables.push_back(0);
        
        if(reserved)
            ++reservedTagsNum;
        
        return variables.size() - 1;
    }
    
    void setValue(value_tag tag, int value)
    {
        variables[tag] = value;
    }
    
    int getValue(value_tag tag)
    {
        return variables[tag];
    }
    
    void clearAllList()
    {
        reservedTagsNum = 0;
        variables.clear();
    }
    
    void clearNotReservedList()
    {
        int size = variables.size();
        
        for(int i = reservedTagsNum; i < size; ++i)
            variables.pop_back();
    }
    
    static VariableList *_instanse;
    
    static VariableList *instanse();
    
};


#endif /* defined(__pictomir_cocos2d_x__VariableList__) */

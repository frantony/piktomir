 //
//  XmlWorldReader.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "XmlWorldReader.h"
#include "World4.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "PMTextureCache.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;
using namespace tinyxml2;

AbstractWorld *XmlWorldReader::readWorldPreview(std::string mapPath)
{
    previewLoad = true;
    
    return readWorld(nullptr, mapPath, -1);
}

AbstractWorld *XmlWorldReader::readWorld(GameLayer *layer,std::string mapPath, int _baseRobot)
{
    gameLayer = layer;
    baseRobot = _baseRobot;
    
	ssize_t size;

	char *buf = (char *)FileUtils::getInstance()->getFileData(mapPath.c_str(),"r", &size);

	tinyxml2::XMLDocument doc;
    doc.Parse(buf,size);
    
    
    if(doc.Error())
    {
        error = doc.GetErrorStr1();
        CCLOG("error:%s\n",doc.GetErrorStr2());
        
        delete buf;
        return nullptr;
    }
    
    XMLElement *mapNode = doc.FirstChildElement("map");
    
    worldType = (WorldType) mapNode->UnsignedAttribute("worldtype");
    
    curNode = mapNode;
    
    createWorldByType();
    
    if(world == nullptr)
    {
        error = "Can't create world!";
        delete buf;
        return nullptr;
    }
    
    if(baseRobot != -1)
        world->setBaseRobot(baseRobot);
    
    bool state;
    
    for (curNode = mapNode->FirstChildElement(); curNode; curNode = curNode->NextSiblingElement())
    {
        if(!strcmp(curNode->Name(), "robots"))
        {
            state = readRobots();
        }
        else if(!strcmp(curNode->Name(), "layers"))
        {
            state = readMapNode();
        }
        else if(!strcmp(curNode->Name(), "hints") && layer != nullptr && !previewLoad)
        {
            state = readHintsNode();
        }
        
        if(!state)
            break;
    }// end for
    
    if(!state)
    {
        delete world;
        return nullptr;
    }
    
    world->getMap()->drawMap(previewLoad);
    delete buf;
    return world;
}

bool XmlWorldReader::readRobots()
{
    int robotCount = 0;
    XMLElement *parentNode = curNode;
    
    for (curNode = curNode->FirstChildElement(); curNode; curNode = curNode->NextSiblingElement())
    {
        if(strcmp((char *)curNode->Name(), "robot"))
        {
            error = "Not registered node :" + std::string((char *)curNode->Name());
            return false;
        }
        
        RobotType robotType = (RobotType)curNode->UnsignedAttribute("type");
        
        if(!createRobot(robotType))
        {
            error = "Can't create robot!";
            return false;
        }
        
        if (curNode->Attribute("baserobot") && baseRobot == -1)
        {
            world->setBaseRobot(robotCount);
            baseRobot = robotCount;
        }
        
        PMTextureCache::instanse()->initRobotTextures(world);
        
        if(!readRobotNode(robotType))
            return false;
        
        if(world->robotAt(robotCount)->parentType() == pmPlayerRobot )
            ((PlayerRobot *)world->robotAt(robotCount))->initTexture();
        
        if(world->robotAt(robotCount)->parentType() == pmEnvRobot )
            ((EnvRobot *)world->robotAt(robotCount))->initTexture();

        ++robotCount;
    }//end for
    
    
    if(robotCount == 1 && baseRobot == -1)
    {
        world->setBaseRobot(0);
    }
    
    else if(world->getBaseRobot() == nullptr)
    {
        error = "No base Robot";
        return false;
    }

    
    curNode = parentNode;
    return true;
}

bool XmlWorldReader::readRobotNode(RobotType robotType)
{
    switch (robotType)
    {
        case pmRobot4:
            return readRobot4Node();
            
        case pmLamp4:
            return readLamp4Node();
            break;
            
        case pmTrasportRobot4:
            return readTransportRobot4Node();
            break;
            
        default:
            return false;
    }

}

bool XmlWorldReader::readMapNode()
{
    switch(worldType)
    {
        case pmWorld4:
            return readMap4Node();
            
        default:
            return false;
    }

}

bool XmlWorldReader::readRobot4Node()
{
    Robot4 *robot = (Robot4 *) world->robotAt( world->robotCount() - 1 );
    
    
    int x = curNode->IntAttribute("x");
    int y = curNode->IntAttribute("y");
    int layer = curNode->IntAttribute("layer");
    int state = curNode->IntAttribute("state");
    
    robot->setPosition( Point(x, y), layer, state);
    
    robot->setStartPosition( Point(x,y) );
    robot->setStartDirection(state);
    robot->setStartLayer(layer);
    
    XMLElement *parentNode = curNode;
    
    if(!previewLoad)
    {
        for (curNode = curNode->FirstChildElement(); curNode; curNode = curNode->NextSiblingElement())
        {
            if(!strcmp((char *)curNode->Name(), "functions"))
            {
                if(!readFunctionsNode((AbstractRobot *)robot == world->getBaseRobot()))
                    return false;
            }
            else if(!strcmp((char *)curNode->Name(), "tasks"))
            {
                if(!readTaskNode())
                    return false;
            }
        }
    }
    curNode = parentNode;
    return true;
}

bool XmlWorldReader::readLamp4Node()
{
    Lamp4 *robot = (Lamp4 *) world->robotAt( world->robotCount() - 1 );
    
    int x = curNode->IntAttribute("x");
    int y = curNode->IntAttribute("y");
    int layer = curNode->IntAttribute("layer");
    int state = curNode->IntAttribute("state");
    
    robot->setPosition( Point(x, y), layer, state);
    
    robot->setStartPosition( Point(x,y) );
    robot->setStartDirection(state);
    robot->setStartLayer(layer);
    
    XMLElement *lampNode = curNode->FirstChildElement();
    
    int lampX = lampNode->IntAttribute("x");
    int lampY = lampNode->IntAttribute("x");
    
    robot->setLampPosition( Point(lampX, lampY) );
    
    return true;
}

bool XmlWorldReader::readTransportRobot4Node()
{
    TransportRobot4 *robot = (TransportRobot4 *) world->robotAt( world->robotCount() - 1 );
    
    
    int x = curNode->IntAttribute("x");
    int y = curNode->IntAttribute("y");
    int layer = curNode->IntAttribute("layer");
    int state = curNode->IntAttribute("state");
    
    robot->setPosition( Point(x, y), layer, state);
    
    robot->setStartPosition( Point(x,y) );
    robot->setStartDirection(state);
    robot->setStartLayer(layer);
    
    return true;
}

bool getbit(int src, int index)
{
    return src &= (1 << index);
}

bool XmlWorldReader::readMap4Node()
{
    int layer = 0;
    
    World4 *world4 = (World4 *)world;
    
    XMLElement *parentNode = curNode;
    
    for (curNode = curNode->FirstChildElement(); curNode; curNode = curNode->NextSiblingElement())
    {
        if(strcmp(curNode->Name(), "layer"))
        {
            error = "Not registered node :" + std::string(curNode->Name());
            return false;
        }
        
        int width = world4->getMap4()->getWidth();
        int height = world4->getMap4()->getHeight();
        
        char * MapStr = new char[width*height*3];
        sscanf((char *)(curNode->GetText()), " %s", MapStr);
        
        std::strcpy(MapStr, base64_decode(MapStr).c_str());
        
        std::vector<std::vector<std::vector<MapElement *> > > &layers = world4->getMap4()->mapElements;
        
        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                int tmp = y * width + x;
                int walls = (unsigned char)MapStr[tmp*2]-1;
                int type = MapStr[tmp*2+1]-2;
                
                layers[layer][y][x]->setElement(type);
                
                if(layers[layer][y][x]->getGrassSprite() != nullptr)
                {
                    layers[layer][y][x]->getGrassSprite()->setPosition(world4->getMap4()->Coordinates(x,y,0,0));
                }
                
                if (type == MapElement::BrokenGrass)
                {
                    world4->getMap4()->incGrassCount();
                }
                
                if(y < height-1 && getbit(walls, 7))
                    layers[layer][y][x]->nearElements[Map4::Down] = layers[layer][y+1][x];
                else
                    layers[layer][y][x]->nearElements[Map4::Down] = nullptr;
                
                if(x < width-1 && getbit(walls, 6))
                    layers[layer][y][x]->nearElements[Map4::Right] = layers[layer][y][x+1];
                else
                    layers[layer][y][x]->nearElements[Map4::Right] = nullptr;
                
                if(y > 0 && getbit(walls, 5))
                    layers[layer][y][x]->nearElements[Map4::Up] = layers[layer][y-1][x];
                else
                    layers[layer][y][x]->nearElements[Map4::Up] = nullptr;
                
                if(x > 0 && getbit(walls, 4))
                    layers[layer][y][x]->nearElements[Map4::Left] = layers[layer][y][x-1];
                else
                    layers[layer][y][x]->nearElements[Map4::Left] = nullptr;
                
                Sprite* sprite = nullptr;
                
                if ( x==0 || y == 0 )
                {
                    sprite = nullptr;
                }
                else if(layers[layer][y][x]->nearElements[Map4::Left] == nullptr)
                {
                    if (layers[layer][y][x]->nearElements[Map4::Up] == nullptr)
                    {
                        sprite = PMTextureCache::instanse()->getMapWallTile(2);
                        sprite->setPosition(world4->getMap4()->Coordinates(x,y,-7,7));
                        //Лево верх 64x34
                    }
                    else
                    {
                        sprite = PMTextureCache::instanse()->getMapWallTile(0);
                        sprite->setPosition(world4->getMap4()->Coordinates(x,y,-35,7));
                        //Лево 43х34
                    }
                }
                else if (layers[layer][y][x]->nearElements[Map4::Up] == nullptr)
                {
                    sprite = PMTextureCache::instanse()->getMapWallTile(1);
                    sprite->setPosition(world4->getMap4()->Coordinates(x,y,-7,35));
                    //Верх
                }
                else
                {
                    sprite = nullptr;
                }
                
                if (sprite!=nullptr)
                {
                    
                }
                
                layers[layer][y][x]->setWallSprite(sprite);
            } //end for x
        } // end for y
        
        ++layer;

    }// end for
    
   curNode = parentNode;
    
    return true;
}

bool XmlWorldReader::readFunctionsNode(bool isBaseRobot)
{
    if(gameLayer == nullptr)
        return true;
    
    ProgramLayer *programLayer = nullptr;
 
#ifndef MAP_EDITOR
    
    if(isBaseRobot)
    {
        programLayer = ProgramLayer::create(gameLayer);
        gameLayer->addChild(programLayer, 15, 1);
        gameLayer->setProgramLayer(programLayer);
    }
    
#else
    programLayer = ProgramLayer::create(gameLayer);
    gameLayer->addChild(programLayer, 15, 1);
    Robot4 *robot = (Robot4 *) world->robotAt( world->robotCount() - 1 );
    robot->program = programLayer;
    
    if(isBaseRobot) {
        programLayer->setVisible(true);
        gameLayer->setProgramLayer(programLayer);
    } else {
        programLayer->setVisible(false);
    }
#endif
    
    
    int cnt = 0;
    
    XMLElement *parentNode = curNode;
    
    for (curNode = curNode->FirstChildElement(); curNode; curNode = curNode->NextSiblingElement())
    {
        if (!strcmp(curNode->Name(), "function"))
        {
            MethodLayer *methodLayer = nullptr;

#ifndef MAP_EDITOR
            if(isBaseRobot)
            {
                methodLayer = MethodLayer::create(programLayer);
                methodLayer->index = cnt;
                programLayer->layers.push_back(methodLayer);
            }
#else
            methodLayer = MethodLayer::create(programLayer);
            methodLayer->index = cnt;
            programLayer->layers.push_back(methodLayer);
#endif
            
            if(!readMethodNode(methodLayer))
                return false;
            
            cnt++;
        }
        else
        {
            error = "Not registered node :" + std::string(curNode->Name());
            return false;
        }
    }

#ifndef MAP_EDITOR
    if(isBaseRobot)
    {
        programLayer->drawLayers(world);
    }
#else
    programLayer->drawLayers(world);
#endif
    
    curNode = parentNode;
    return true;
}

bool XmlWorldReader::readMethodNode(MethodLayer *methodLayer)
{
    Robot4 *robot = (Robot4 *) world->robotAt( world->robotCount() - 1 );
    
    if(curNode->FirstChildElement("content")->GetText() == nullptr)
    {
        error = "No content for functions!";
        return false;
    }
    
    string functionContent = curNode->FirstChildElement("content")->GetText();

    std::vector<_method> methods;
    
    string name = curNode->Attribute("name");
    int repeater = curNode->FirstChildElement("repeater")->IntAttribute("value");
    int condition = curNode->FirstChildElement("condition")->IntAttribute("value");
    bool useRepeater = curNode->FirstChildElement("repeater")->IntAttribute("use");
    bool useCondition = curNode->FirstChildElement("condition")->IntAttribute("use");
    int width = curNode->FirstChildElement("sizes")->FirstChildElement("size")->IntAttribute("width");
    int height = curNode->FirstChildElement("sizes")->FirstChildElement("size")->IntAttribute("height");
   
    bool hidden = curNode->IntAttribute("hide");
    string givenText;
    string doText;
    
    bool resizable = curNode->FirstChildElement("sizes")->IntAttribute("lockheitgh") != 0;
    int maxHeight = curNode->FirstChildElement("sizes")->FirstChildElement("maxsize")->IntAttribute("height");
    
    if(hidden)
    {
        givenText = curNode->FirstChildElement("hiddentext")->FirstChildElement("giventext")->GetText();
        doText = curNode->FirstChildElement("hiddentext")->FirstChildElement("dotext")->GetText();
    }
    
    if(repeater == 0)
        repeater = 1;
    
    int methodID;
    
    if(name == "main"){
        methodID = CMDMAIN;
    } else if(name == "Command 1") {
        methodID = CMDA;
    } else if(name == "Command 2") {
        methodID = CMDB;
    } else if(name == "Command 3") {
        methodID = CMDC;
    }

    
    for (int i = 0; i < 3 * width * height; i+=3)
    {
        _method::BackgroundType backType = (_method::BackgroundType)(functionContent[i + 1] - '0');
        _method::ExecuteType execType = (_method::ExecuteType)(functionContent[i + 2] - '0');

        switch(functionContent[i])
        {
            case 'A' : methods.push_back(_method(CMDA, backType, execType)); break;
            case 'B' : methods.push_back(_method(CMDB, backType, execType)); break;
            case 'C' : methods.push_back(_method(CMDC, backType, execType));break;
            default : methods.push_back(_method((_method::ExecuteType)(functionContent[i] - '0'), backType, execType));break;
        }

    }
    
    if(methodLayer == nullptr)
    {
        //value_tag useConditionTag;
        
        //vector<_instruction> execVector = RobotMethod::makeExecuteMethod(methodID, useRepeater, repeater, useCondition, condition,useConditionTag, true);
        vector<_instruction> instrVector = RobotMethod::makeMethod(methodID,methods, -1, condition != -1,repeater,true);
        
        robot->addMethod(methodID,  RobotMethod(robot, instrVector, RobotMethod::ProgrammedMethod) );
        //robot->addMethod(methodID_Exec,  RobotMethod(robot, execVector, RobotMethod::ProgrammedMethod) );
    }
    else
    {
        methodLayer->methodID = methodID;
        
        methodLayer->name = name;
        methodLayer->repeater = repeater;
        methodLayer->condition = condition;
        methodLayer->useRepeater = useRepeater;
        methodLayer->useCondition = useCondition;
        methodLayer->width = width;
        methodLayer->height = height;
        
        methodLayer->methods = methods;
        
        methodLayer->hidden = hidden;
        
        if(hidden)
        {
            methodLayer->givenText = givenText;
            methodLayer->doText = doText;
        }
        
        methodLayer->resizable = resizable;
        methodLayer->maxHeight = maxHeight;
        methodLayer->minHeight = height;
        
        methodLayer->drawFunctions();
    }
    
    return true;
}

bool XmlWorldReader::readTaskNode()
{
    Robot4 *robot = (Robot4 *) world->robotAt( world->robotCount() - 1 );
    
    XMLElement *parentNode = curNode;
    
    robot->setTaskList(new TaskList(pmCompleteAllTasks));
    
    for (curNode = curNode->FirstChildElement(); curNode; curNode=curNode->NextSiblingElement())
    {
        
        if(!strcmp(curNode->Name(), "paint"))
        {
            int needToPaint = curNode->IntAttribute("count");
            
            if(needToPaint == 0)
                needToPaint = ((Map4 *)world->getMap())->getGrassCount();
            
            PaintTask *task = PaintTask::create(robot, needToPaint);
            robot->getTaskList()->addTask(pmPaintTask, task);
        }
        else if(!strcmp(curNode->Name(), "interaction"))
        {
            
        }
        else if(!strcmp(curNode->Name(), "positions"))
        {
            int x = curNode->IntAttribute("x");
            int y = curNode->IntAttribute("y");
            int layer = curNode->IntAttribute("layer");
            
            PositionTask *task = PositionTask::create(robot, layer, Point(x,y));
            robot->getTaskList()->addTask(pmPositionTask, task);
        }
        
    }//end for
    
    curNode = parentNode;
    return true;
}

bool XmlWorldReader::readHintsNode()
{
    XMLElement *parentNode = curNode;
    for (curNode = curNode->FirstChildElement(); curNode; curNode = curNode->NextSiblingElement())
    {
        if (!strcmp(curNode->Name(), "hint"))
        {
            Hint *hint = Hint::create();
            
            std::string target;
            
            if(curNode->Attribute("target") != nullptr)
                target = curNode->Attribute("target");
            else
                target = "global";
            
            if(target == "global")
            {
                HintPart part = {};
                part.type = GlobalHint;
                
                if(curNode->GetText() != nullptr)
                    part.hintText = curNode->GetText();
                
                hint->hintParts.push_back(part);
                
                gameLayer->setHint(hint);
            }
            else
            {
                MethodLayer *layer;
                
                for (int i = 0; i < gameLayer->getProgramLayer()->layers.size();++i )
                {
                    if(gameLayer->getProgramLayer()->layers[i]->name == target)
                        layer = gameLayer->getProgramLayer()->layers[i];
                }

                XMLElement *parentSubNode = curNode;
                for (curNode = curNode->FirstChildElement(); curNode; curNode = curNode->NextSiblingElement())
                {
                    HintPart part = {};
                    
                    std::string _type = curNode->Attribute("type");
                    
                    if (_type == "text")
                    {
                        part.type = TextHint;
                        part.hintText = curNode->GetText();
                        
                    }
                    else if(_type == "function")
                    {
                        part.type = MethodHint;
                        part.hintText = "";
                        
                        
                        string content = curNode->GetText();
                        
                        for (int i = 0; i < layer->methods.size(); ++i)
                        {
                            switch(content[i])
                            {
                                case '-' : part.funcHint.push_back(_method(EMPTY_METHOD, _method::pmNormal, _method::pmExecuteOnTrue)); break;
                                case 'A' : part.funcHint.push_back(_method(CMDA, _method::pmNormal, _method::pmExecuteOnTrue)); break;
                                case 'B' : part.funcHint.push_back(_method(CMDB, _method::pmNormal, _method::pmExecuteOnTrue)); break;
                                case 'C' : part.funcHint.push_back(_method(CMDC, _method::pmNormal, _method::pmExecuteOnTrue));break;
                                default : part.funcHint.push_back(_method(content[i] - '0', _method::pmNormal, _method::pmExecuteOnTrue));break;
                            }

                        }
                    }
                    
                    hint->hintParts.push_back(part);
                    layer->setHint(hint);
                    layer->drawHint();
                }//end for
                curNode = parentSubNode;
                
            }// end else
            
        }//end if
    }// end for

    curNode = parentNode;
    return true;
}

bool XmlWorldReader::createRobot(RobotType robotType)
{
    AbstractRobot *robot;
    
    switch (robotType)
    {
        case pmRobot4:
            robot = Robot4::create(world);
            break;
            
        case pmLamp4:
            robot = Lamp4::create(world);
            break;
        
        case pmTrasportRobot4:
            robot = TransportRobot4::create(world);
            break;
            
        default:
            robot = nullptr;
            return false;
            break;
    }
    
    world->addRobot(robot);
    
    return true;
}

void XmlWorldReader::createWorldByType()
{
    switch(worldType)
    {
        case pmWorld4:
            world  = World4::create(
                        curNode->IntAttribute("width"),
                        curNode->IntAttribute("height"),
                        curNode->IntAttribute("layers"),
                        curNode->IntAttribute("tileset")
                    );
            break;
        default: return;
    }
    
    PMTextureCache::instanse()->initMapTexture(world->type(),curNode->IntAttribute("tileset"));
}



//
//  XmlWorldSaver.h
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 22.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__XmlWorldSaver__
#define __pictomir_cocos2d_x__XmlWorldSaver__

#include <iostream>
#include "AbstractWorld.h"
#include "GameLayer.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "tinyxml2.h"

#include "PaintTask.h"
#include "PositionTask.h"

using namespace std;
using namespace tinyxml2;

class XMLWorldSaver {
	tinyxml2::XMLDocument * doc;
    
    AbstractWorld* world_;
    GameLayer* layer_;
    std::string path_;
    
    void saveWorld4(XMLElement * map);
    XMLElement * saveMap4Layer (int layer);
    
    XMLElement * saveProgram ();
    XMLElement * saveRobotProgram (int robot);
    XMLElement * saveMethod (int layer);
    XMLElement * saveRobotMethod (int robot, int method);
    XMLElement * saveTask (int robot);
    
    XMLElement * saveHint(int methodlayer);
    
    XMLElement * saveRobot (int robot);
    
public:
    XMLWorldSaver(AbstractWorld * world, GameLayer * gamelayer, std::string path);
};

#endif /* defined(__pictomir_cocos2d_x__XmlWorldSaver__) */

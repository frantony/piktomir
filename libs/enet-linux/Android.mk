LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := enet_static

LOCAL_MODULE_FILENAME := libenet

LOCAL_SRC_FILES := $(wildcard $(LOCAL_PATH)/*.c)
 
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include

#LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static

include $(BUILD_STATIC_LIBRARY)


#NDK_TOOLCHAIN_VERSION=clang
APP_STL := c++_static
APP_CPPFLAGS := -frtti -DCC_ENABLE_CHIPMUNK_INTEGRATION=1 -DCOCOS2D_DEBUG=1 -std=c++11 -fsigned-char
APP_LDFLAGS := -lstdc++ -latomic
